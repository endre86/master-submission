<?xml version="1.0" encoding="UTF-8"?>
<xmi:XMI xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:core="http://no.hib.dpf.core" xmlns:diagram="http://no.hib.dpf.diagram">
  <diagram:DSignature signature="/1">
    <dPredicates predicate="/1/@predicates.0">
      <dGraph graph="/1/@predicates.0/@shape">
        <dNodes node="/1/@predicates.0/@shape/@nodes.0" dOutgoings="/0/@dPredicates.0/@dGraph/@dArrows.0 /0/@dPredicates.0/@dGraph/@dArrows.1" location="51 116" size="100 30"/>
        <dNodes node="/1/@predicates.0/@shape/@nodes.1" dIncomings="/0/@dPredicates.0/@dGraph/@dArrows.0" location="322 41" size="100 30"/>
        <dNodes node="/1/@predicates.0/@shape/@nodes.2" dIncomings="/0/@dPredicates.0/@dGraph/@dArrows.1" location="347 178" size="100 30"/>
        <dArrows arrow="/1/@predicates.0/@shape/@arrows.0" dSource="/0/@dPredicates.0/@dGraph/@dNodes.0" dTarget="/0/@dPredicates.0/@dGraph/@dNodes.1">
          <nameOffset offset="10 -3" len="16"/>
        </dArrows>
        <dArrows arrow="/1/@predicates.0/@shape/@arrows.1" dSource="/0/@dPredicates.0/@dGraph/@dNodes.0" dTarget="/0/@dPredicates.0/@dGraph/@dNodes.2">
          <nameOffset offset="10 -3" len="16"/>
        </dArrows>
      </dGraph>
      <visualization type="ArrowToArrow" source="/1/@predicates.0/@shape/@arrows.0" target="/1/@predicates.0/@shape/@arrows.1"/>
    </dPredicates>
    <dPredicates predicate="/1/@predicates.1">
      <dGraph graph="/1/@predicates.1/@shape">
        <dNodes node="/1/@predicates.1/@shape/@nodes.0" dOutgoings="/0/@dPredicates.1/@dGraph/@dArrows.0" location="100 5" size="100 30"/>
        <dNodes node="/1/@predicates.1/@shape/@nodes.1" dOutgoings="/0/@dPredicates.1/@dGraph/@dArrows.1" location="100 105" size="100 30"/>
        <dNodes node="/1/@predicates.1/@shape/@nodes.2" dIncomings="/0/@dPredicates.1/@dGraph/@dArrows.0 /0/@dPredicates.1/@dGraph/@dArrows.1" location="369 63" size="100 30"/>
        <dArrows arrow="/1/@predicates.1/@shape/@arrows.0" dSource="/0/@dPredicates.1/@dGraph/@dNodes.0" dTarget="/0/@dPredicates.1/@dGraph/@dNodes.2">
          <nameOffset offset="10 -3" len="16"/>
        </dArrows>
        <dArrows arrow="/1/@predicates.1/@shape/@arrows.1" dSource="/0/@dPredicates.1/@dGraph/@dNodes.1" dTarget="/0/@dPredicates.1/@dGraph/@dNodes.2">
          <nameOffset offset="10 -3" len="16"/>
        </dArrows>
      </dGraph>
      <visualization type="ArrowToArrow" source="/1/@predicates.1/@shape/@arrows.0" target="/1/@predicates.1/@shape/@arrows.1"/>
    </dPredicates>
    <dPredicates predicate="/1/@predicates.2">
      <dGraph graph="/1/@predicates.2/@shape">
        <dNodes node="/1/@predicates.2/@shape/@nodes.0" dOutgoings="/0/@dPredicates.2/@dGraph/@dArrows.0 /0/@dPredicates.2/@dGraph/@dArrows.1" location="55 89" size="100 30"/>
        <dNodes node="/1/@predicates.2/@shape/@nodes.1" dIncomings="/0/@dPredicates.2/@dGraph/@dArrows.1" location="342 149" size="100 30"/>
        <dNodes node="/1/@predicates.2/@shape/@nodes.2" dIncomings="/0/@dPredicates.2/@dGraph/@dArrows.0" location="352 28" size="100 30"/>
        <dArrows arrow="/1/@predicates.2/@shape/@arrows.0" dSource="/0/@dPredicates.2/@dGraph/@dNodes.0" dTarget="/0/@dPredicates.2/@dGraph/@dNodes.2">
          <nameOffset offset="10 -3" len="16"/>
        </dArrows>
        <dArrows arrow="/1/@predicates.2/@shape/@arrows.1" dSource="/0/@dPredicates.2/@dGraph/@dNodes.0" dTarget="/0/@dPredicates.2/@dGraph/@dNodes.1">
          <nameOffset offset="10 -3" len="16"/>
        </dArrows>
      </dGraph>
      <visualization type="ArrowToArrow" source="/1/@predicates.2/@shape/@arrows.0" target="/1/@predicates.2/@shape/@arrows.1"/>
    </dPredicates>
    <dPredicates predicate="/1/@predicates.3">
      <dGraph graph="/1/@predicates.3/@shape">
        <dNodes node="/1/@predicates.3/@shape/@nodes.0" dOutgoings="/0/@dPredicates.3/@dGraph/@dArrows.0" location="74 21" size="100 30"/>
        <dNodes node="/1/@predicates.3/@shape/@nodes.1" dOutgoings="/0/@dPredicates.3/@dGraph/@dArrows.1" location="98 122" size="100 30"/>
        <dNodes node="/1/@predicates.3/@shape/@nodes.2" dIncomings="/0/@dPredicates.3/@dGraph/@dArrows.0 /0/@dPredicates.3/@dGraph/@dArrows.1" location="404 70" size="100 30"/>
        <dArrows arrow="/1/@predicates.3/@shape/@arrows.0" dSource="/0/@dPredicates.3/@dGraph/@dNodes.0" dTarget="/0/@dPredicates.3/@dGraph/@dNodes.2">
          <nameOffset offset="10 -3" len="16"/>
        </dArrows>
        <dArrows arrow="/1/@predicates.3/@shape/@arrows.1" dSource="/0/@dPredicates.3/@dGraph/@dNodes.1" dTarget="/0/@dPredicates.3/@dGraph/@dNodes.2">
          <nameOffset offset="10 -3" len="16"/>
        </dArrows>
      </dGraph>
      <visualization type="ArrowToArrow" source="/1/@predicates.3/@shape/@arrows.0" target="/1/@predicates.3/@shape/@arrows.1"/>
    </dPredicates>
    <dPredicates predicate="/1/@predicates.4">
      <dGraph graph="/1/@predicates.4/@shape">
        <dNodes node="/1/@predicates.4/@shape/@nodes.0" location="54 27" size="100 30"/>
      </dGraph>
      <visualization type="OnNode"/>
    </dPredicates>
    <dPredicates predicate="/1/@predicates.5">
      <dGraph graph="/1/@predicates.5/@shape">
        <dNodes node="/1/@predicates.5/@shape/@nodes.0" location="96 69" size="100 30"/>
      </dGraph>
      <visualization type="OnNode"/>
    </dPredicates>
  </diagram:DSignature>
  <core:Signature>
    <predicates symbol="and_split">
      <shape id="de982ee1-1dd9-4f97-9e1e-56ca1cca7c1d">
        <nodes id="b0709e0a-688d-493b-b08e-7e6ca831924e" name="Node0" outgoings="/1/@predicates.0/@shape/@arrows.0 /1/@predicates.0/@shape/@arrows.1"/>
        <nodes id="d0d594d0-04a4-486e-ad31-e8a3a6c94557" name="Node1" incomings="/1/@predicates.0/@shape/@arrows.0"/>
        <nodes id="2b48a360-aa3e-46b5-96a3-875af88c3fb8" name="Node2" incomings="/1/@predicates.0/@shape/@arrows.1"/>
        <arrows id="47e1f3b9-5a12-4d39-adfc-8fc7c5978e80" name="Arrow0" source="/1/@predicates.0/@shape/@nodes.0" target="/1/@predicates.0/@shape/@nodes.1"/>
        <arrows id="7ecf86df-2975-46f9-9bb5-0da9ff4be171" name="Arrow1" source="/1/@predicates.0/@shape/@nodes.0" target="/1/@predicates.0/@shape/@nodes.2"/>
      </shape>
      <validator type="JAVA" validator="import java.util.List;&#xA;import java.util.Map;&#xA;import no.hib.dpf.core.Arrow;&#xA;import no.hib.dpf.core.Graph;&#xA;import no.hib.dpf.core.Node;&#xA;import no.hib.dpf.utils.Checker;&#xA;public class CheckInstance implements Checker{&#xA;&#x9;@Override&#xA;&#x9;public boolean check(Map&lt;String, String> paras, Graph graph, Map&lt;Node, List&lt;Node>> nodeMap,&#xA;&#x9;&#x9;&#x9;Map&lt;Arrow, List&lt;Arrow>> arrowMap) {&#xA;&#x9;&#x9;return true;&#xA;&#x9;}&#xA;}&#xA;"/>
    </predicates>
    <predicates symbol="and_join">
      <shape id="9b74f9be-f707-49ee-bf39-c420ae4a8249">
        <nodes id="0ef2bed9-c72c-42cb-94e6-c44f89f604fe" name="Node0" outgoings="/1/@predicates.1/@shape/@arrows.0"/>
        <nodes id="847b6fbe-fbf6-4f30-be8b-fac7962fdd94" name="Node1" outgoings="/1/@predicates.1/@shape/@arrows.1"/>
        <nodes id="e386fdf5-57ea-4f2a-930c-9a0a3c0943b7" name="Node2" incomings="/1/@predicates.1/@shape/@arrows.0 /1/@predicates.1/@shape/@arrows.1"/>
        <arrows id="ec287247-2835-44a1-abcd-0ccfb753688e" name="Arrow0" source="/1/@predicates.1/@shape/@nodes.0" target="/1/@predicates.1/@shape/@nodes.2"/>
        <arrows id="6bfd5116-7a88-4c0f-bf23-bec3ea4fe2e8" name="Arrow1" source="/1/@predicates.1/@shape/@nodes.1" target="/1/@predicates.1/@shape/@nodes.2"/>
      </shape>
      <validator type="JAVA" validator="import java.util.List;&#xA;import java.util.Map;&#xA;import no.hib.dpf.core.Arrow;&#xA;import no.hib.dpf.core.Graph;&#xA;import no.hib.dpf.core.Node;&#xA;import no.hib.dpf.utils.Checker;&#xA;public class CheckInstance implements Checker{&#xA;&#x9;@Override&#xA;&#x9;public boolean check(Map&lt;String, String> paras, Graph graph, Map&lt;Node, List&lt;Node>> nodeMap,&#xA;&#x9;&#x9;&#x9;Map&lt;Arrow, List&lt;Arrow>> arrowMap) {&#xA;&#x9;&#x9;return true;&#xA;&#x9;}&#xA;}&#xA;"/>
    </predicates>
    <predicates symbol="xor_split">
      <shape id="8fe7ca24-7459-43c0-bfd6-0a03959d9e2b">
        <nodes id="24666a6e-ee54-4a53-ad89-9f45f1e74010" name="Node0" outgoings="/1/@predicates.2/@shape/@arrows.0 /1/@predicates.2/@shape/@arrows.1"/>
        <nodes id="4d3f3c35-bae8-4410-83dc-7ecf4e8babdf" name="Node1" incomings="/1/@predicates.2/@shape/@arrows.1"/>
        <nodes id="438991df-20ad-4298-b1d0-81949022d5dc" name="Node2" incomings="/1/@predicates.2/@shape/@arrows.0"/>
        <arrows id="e337827c-4d31-4b6e-a5c1-00c195c301f5" name="Arrow0" source="/1/@predicates.2/@shape/@nodes.0" target="/1/@predicates.2/@shape/@nodes.2"/>
        <arrows id="936d4032-fdb1-49e1-bc6f-150c0acfc4eb" name="Arrow1" source="/1/@predicates.2/@shape/@nodes.0" target="/1/@predicates.2/@shape/@nodes.1"/>
      </shape>
      <validator type="JAVA" validator="import java.util.List;&#xA;import java.util.Map;&#xA;import no.hib.dpf.core.Arrow;&#xA;import no.hib.dpf.core.Graph;&#xA;import no.hib.dpf.core.Node;&#xA;import no.hib.dpf.utils.Checker;&#xA;public class CheckInstance implements Checker{&#xA;&#x9;@Override&#xA;&#x9;public boolean check(Map&lt;String, String> paras, Graph graph, Map&lt;Node, List&lt;Node>> nodeMap,&#xA;&#x9;&#x9;&#x9;Map&lt;Arrow, List&lt;Arrow>> arrowMap) {&#xA;&#x9;&#x9;return true;&#xA;&#x9;}&#xA;}&#xA;"/>
    </predicates>
    <predicates symbol="xor_join">
      <shape id="15ca4b58-c687-4be3-a7f1-189e784332b9">
        <nodes id="85ca1f42-d151-49f2-85b8-cac51e27ab05" name="Node0" outgoings="/1/@predicates.3/@shape/@arrows.0"/>
        <nodes id="26f735df-e78a-4fe6-baf4-e94b893c0474" name="Node1" outgoings="/1/@predicates.3/@shape/@arrows.1"/>
        <nodes id="8af945e0-dcf8-4253-ad74-5801d44192e9" name="Node2" incomings="/1/@predicates.3/@shape/@arrows.0 /1/@predicates.3/@shape/@arrows.1"/>
        <arrows id="6ece58a8-bd71-4cc8-9252-00e14db2d9fc" name="Arrow0" source="/1/@predicates.3/@shape/@nodes.0" target="/1/@predicates.3/@shape/@nodes.2"/>
        <arrows id="080ee0a6-24ec-4d63-975d-610d0f04f7a0" name="Arrow1" source="/1/@predicates.3/@shape/@nodes.1" target="/1/@predicates.3/@shape/@nodes.2"/>
      </shape>
      <validator type="JAVA" validator="import java.util.List;&#xA;import java.util.Map;&#xA;import no.hib.dpf.core.Arrow;&#xA;import no.hib.dpf.core.Graph;&#xA;import no.hib.dpf.core.Node;&#xA;import no.hib.dpf.utils.Checker;&#xA;public class CheckInstance implements Checker{&#xA;&#x9;@Override&#xA;&#x9;public boolean check(Map&lt;String, String> paras, Graph graph, Map&lt;Node, List&lt;Node>> nodeMap,&#xA;&#x9;&#x9;&#x9;Map&lt;Arrow, List&lt;Arrow>> arrowMap) {&#xA;&#x9;&#x9;return true;&#xA;&#x9;}&#xA;}&#xA;"/>
    </predicates>
    <predicates symbol="nodemult" parameters="x">
      <shape id="7559971e-6c52-4881-a90d-e0ed7e2edd9d">
        <nodes id="00a10034-b580-45e2-ae92-06ef03f02a86" name="Node0"/>
      </shape>
      <validator type="JAVA" validator="import java.util.List;&#xA;import java.util.Map;&#xA;import no.hib.dpf.core.Arrow;&#xA;import no.hib.dpf.core.Graph;&#xA;import no.hib.dpf.core.Node;&#xA;import no.hib.dpf.utils.Checker;&#xA;public class CheckInstance implements Checker{&#xA;&#x9;@Override&#xA;&#x9;public boolean check(Map&lt;String, String> paras, Graph graph, Map&lt;Node, List&lt;Node>> nodeMap,&#xA;&#x9;&#x9;&#x9;Map&lt;Arrow, List&lt;Arrow>> arrowMap) {&#xA;&#x9;&#x9;return true;&#xA;&#x9;}&#xA;}&#xA;"/>
    </predicates>
    <predicates symbol="nodemult4" parameters="int x, int y">
      <shape id="9fd3f1df-c211-490a-8354-199bfd5508c7">
        <nodes id="ccc2209c-dd13-4586-9008-80fc52c63b66" name="Node0"/>
      </shape>
      <validator type="JAVA" validator="import java.util.List;&#xA;import java.util.Map;&#xA;import no.hib.dpf.core.Arrow;&#xA;import no.hib.dpf.core.Graph;&#xA;import no.hib.dpf.core.Node;&#xA;import no.hib.dpf.utils.Checker;&#xA;public class CheckInstance implements Checker{&#xA;&#x9;@Override&#xA;&#x9;public boolean check(Map&lt;String, String> paras, Graph graph, Map&lt;Node, List&lt;Node>> nodeMap,&#xA;&#x9;&#x9;&#x9;Map&lt;Arrow, List&lt;Arrow>> arrowMap) {&#xA;&#x9;&#x9;return true;&#xA;&#x9;}&#xA;}&#xA;"/>
    </predicates>
  </core:Signature>
</xmi:XMI>
