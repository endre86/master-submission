<?xml version="1.0" encoding="UTF-8"?>
<xmi:XMI xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:core="http://no.hib.dpf.core" xmlns:diagram="http://no.hib.dpf.diagram">
  <diagram:DSignature signature="/1">
    <dPredicates predicate="/1/@predicates.0">
      <dGraph graph="/1/@predicates.0/@shape">
        <dNodes node="/1/@predicates.0/@shape/@nodes.0" location="153 60" size="100 30"/>
      </dGraph>
      <visualization type="OnNode"/>
    </dPredicates>
    <dPredicates predicate="/1/@predicates.1">
      <dGraph graph="/1/@predicates.1/@shape">
        <dNodes node="/1/@predicates.1/@shape/@nodes.0" location="239 84" size="100 30"/>
      </dGraph>
      <visualization type="OnNode"/>
    </dPredicates>
    <dPredicates predicate="/1/@predicates.2">
      <dGraph graph="/1/@predicates.2/@shape">
        <dNodes node="/1/@predicates.2/@shape/@nodes.0" location="156 69" size="100 30"/>
      </dGraph>
      <visualization type="OnNode"/>
    </dPredicates>
    <dPredicates predicate="/1/@predicates.3">
      <dGraph graph="/1/@predicates.3/@shape">
        <dNodes node="/1/@predicates.3/@shape/@nodes.0" location="224 72" size="100 30"/>
      </dGraph>
      <visualization type="OnNode"/>
    </dPredicates>
    <dPredicates predicate="/1/@predicates.4">
      <dGraph graph="/1/@predicates.4/@shape">
        <dNodes node="/1/@predicates.4/@shape/@nodes.0" dOutgoings="/0/@dPredicates.4/@dGraph/@dArrows.0" location="100 89" size="100 30"/>
        <dNodes node="/1/@predicates.4/@shape/@nodes.1" dIncomings="/0/@dPredicates.4/@dGraph/@dArrows.0" location="437 129" size="100 30"/>
        <dArrows arrow="/1/@predicates.4/@shape/@arrows.0" dSource="/0/@dPredicates.4/@dGraph/@dNodes.0" dTarget="/0/@dPredicates.4/@dGraph/@dNodes.1">
          <nameOffset offset="10 -3" len="16"/>
        </dArrows>
      </dGraph>
      <visualization source="/1/@predicates.4/@shape/@arrows.0"/>
    </dPredicates>
    <dPredicates predicate="/1/@predicates.5">
      <dGraph graph="/1/@predicates.5/@shape">
        <dNodes node="/1/@predicates.5/@shape/@nodes.0" dOutgoings="/0/@dPredicates.5/@dGraph/@dArrows.0" location="21 85" size="100 30"/>
        <dNodes node="/1/@predicates.5/@shape/@nodes.1" dIncomings="/0/@dPredicates.5/@dGraph/@dArrows.0" location="362 76" size="100 30"/>
        <dArrows arrow="/1/@predicates.5/@shape/@arrows.0" dSource="/0/@dPredicates.5/@dGraph/@dNodes.0" dTarget="/0/@dPredicates.5/@dGraph/@dNodes.1">
          <nameOffset offset="10 -3" len="16"/>
        </dArrows>
      </dGraph>
      <visualization source="/1/@predicates.5/@shape/@arrows.0"/>
    </dPredicates>
  </diagram:DSignature>
  <core:Signature>
    <predicates symbol="D" parameters="null">
      <shape id="81ab0fdd-333a-41b9-8a9a-03aa06cfd8e9">
        <nodes id="7e7a60be-511b-4d23-a20d-0040648a51c2" name="Node0"/>
      </shape>
      <validator type="JAVA" validator="import java.util.List;&#xA;import java.util.Map;&#xA;import no.hib.dpf.core.Arrow;&#xA;import no.hib.dpf.core.Graph;&#xA;import no.hib.dpf.core.Node;&#xA;import no.hib.dpf.utils.Checker;&#xA;public class CheckInstance implements Checker{&#xA;&#x9;@Override&#xA;&#x9;public boolean check(Map&lt;String, String> paras, Graph graph, Map&lt;Node, List&lt;Node>> nodeMap,&#xA;&#x9;&#x9;&#x9;Map&lt;Arrow, List&lt;Arrow>> arrowMap) {&#xA;&#x9;&#x9;return true;&#xA;&#x9;}&#xA;}&#xA;"/>
    </predicates>
    <predicates symbol="E" parameters="null">
      <shape id="94ecbc11-6730-4b0c-9276-b775e3d3bb64">
        <nodes id="d683a5e9-5080-4e45-9f90-ba647f8ffb9f" name="Node0"/>
      </shape>
      <validator type="JAVA" validator="import java.util.List;&#xA;import java.util.Map;&#xA;import no.hib.dpf.core.Arrow;&#xA;import no.hib.dpf.core.Graph;&#xA;import no.hib.dpf.core.Node;&#xA;import no.hib.dpf.utils.Checker;&#xA;public class CheckInstance implements Checker{&#xA;&#x9;@Override&#xA;&#x9;public boolean check(Map&lt;String, String> paras, Graph graph, Map&lt;Node, List&lt;Node>> nodeMap,&#xA;&#x9;&#x9;&#x9;Map&lt;Arrow, List&lt;Arrow>> arrowMap) {&#xA;&#x9;&#x9;return true;&#xA;&#x9;}&#xA;}&#xA;"/>
    </predicates>
    <predicates symbol="R" parameters="null">
      <shape id="1aa6dd48-0b12-4f90-b8d1-c4b919a1dc62">
        <nodes id="2b9b31f2-8651-48b8-95f1-9885ca1ca257" name="Node0"/>
      </shape>
      <validator type="JAVA" validator="import java.util.List;&#xA;import java.util.Map;&#xA;import no.hib.dpf.core.Arrow;&#xA;import no.hib.dpf.core.Graph;&#xA;import no.hib.dpf.core.Node;&#xA;import no.hib.dpf.utils.Checker;&#xA;public class CheckInstance implements Checker{&#xA;&#x9;@Override&#xA;&#x9;public boolean check(Map&lt;String, String> paras, Graph graph, Map&lt;Node, List&lt;Node>> nodeMap,&#xA;&#x9;&#x9;&#x9;Map&lt;Arrow, List&lt;Arrow>> arrowMap) {&#xA;&#x9;&#x9;return true;&#xA;&#x9;}&#xA;}&#xA;"/>
    </predicates>
    <predicates symbol="F">
      <shape id="13c81cf9-5e28-48fe-b512-36367cd6982e">
        <nodes id="94e666a6-bd0b-4eba-a40d-3d734dfbea3e" name="Node0"/>
      </shape>
      <validator type="JAVA" validator="import java.util.List;&#xA;import java.util.Map;&#xA;import no.hib.dpf.core.Arrow;&#xA;import no.hib.dpf.core.Graph;&#xA;import no.hib.dpf.core.Node;&#xA;import no.hib.dpf.utils.Checker;&#xA;public class CheckInstance implements Checker{&#xA;&#x9;@Override&#xA;&#x9;public boolean check(Map&lt;String, String> paras, Graph graph, Map&lt;Node, List&lt;Node>> nodeMap,&#xA;&#x9;&#x9;&#x9;Map&lt;Arrow, List&lt;Arrow>> arrowMap) {&#xA;&#x9;&#x9;return true;&#xA;&#x9;}&#xA;}&#xA;"/>
    </predicates>
    <predicates symbol="t">
      <shape id="709bfae2-ef96-446c-bf8a-ca2b46e91e64">
        <nodes id="94ee1ccd-8498-4b00-a87e-38bb36ed84db" name="Node0" outgoings="/1/@predicates.4/@shape/@arrows.0"/>
        <nodes id="840d514d-ced1-413b-a9a1-6b5a2438c0cf" name="Node1" incomings="/1/@predicates.4/@shape/@arrows.0"/>
        <arrows id="b46a04c1-6121-4257-8ee3-7b458f399ef3" name="Arrow0" source="/1/@predicates.4/@shape/@nodes.0" target="/1/@predicates.4/@shape/@nodes.1"/>
      </shape>
      <validator type="JAVA" validator="import java.util.List;&#xA;import java.util.Map;&#xA;import no.hib.dpf.core.Arrow;&#xA;import no.hib.dpf.core.Graph;&#xA;import no.hib.dpf.core.Node;&#xA;import no.hib.dpf.utils.Checker;&#xA;public class CheckInstance implements Checker{&#xA;&#x9;@Override&#xA;&#x9;public boolean check(Map&lt;String, String> paras, Graph graph, Map&lt;Node, List&lt;Node>> nodeMap,&#xA;&#x9;&#x9;&#x9;Map&lt;Arrow, List&lt;Arrow>> arrowMap) {&#xA;&#x9;&#x9;return true;&#xA;&#x9;}&#xA;}&#xA;"/>
    </predicates>
    <predicates symbol="f">
      <shape id="479b88b7-5fc8-41e4-a115-79e067469906">
        <nodes id="993edef9-7f22-40d2-87e6-4554759fb1ab" name="Node0" outgoings="/1/@predicates.5/@shape/@arrows.0"/>
        <nodes id="ec750d3d-6633-4bac-8f1b-6f47507ea98a" name="Node1" incomings="/1/@predicates.5/@shape/@arrows.0"/>
        <arrows id="be49bfa1-30e7-4572-a2ff-076d1b8dad16" name="Arrow0" source="/1/@predicates.5/@shape/@nodes.0" target="/1/@predicates.5/@shape/@nodes.1"/>
      </shape>
      <validator type="JAVA" validator="import java.util.List;&#xA;import java.util.Map;&#xA;import no.hib.dpf.core.Arrow;&#xA;import no.hib.dpf.core.Graph;&#xA;import no.hib.dpf.core.Node;&#xA;import no.hib.dpf.utils.Checker;&#xA;public class CheckInstance implements Checker{&#xA;&#x9;@Override&#xA;&#x9;public boolean check(Map&lt;String, String> paras, Graph graph, Map&lt;Node, List&lt;Node>> nodeMap,&#xA;&#x9;&#x9;&#x9;Map&lt;Arrow, List&lt;Arrow>> arrowMap) {&#xA;&#x9;&#x9;return true;&#xA;&#x9;}&#xA;}&#xA;"/>
    </predicates>
  </core:Signature>
</xmi:XMI>
