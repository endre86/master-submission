package main;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JTable.PrintMode;

import no.hib.dpf.core.Arrow;
import no.hib.dpf.core.Constraint;
import no.hib.dpf.core.CorePackage;
import no.hib.dpf.core.Node;
import no.hib.dpf.core.Predicate;
import no.hib.dpf.core.Specification;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.UnitApplication;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.interpreter.impl.EngineImpl;
import org.eclipse.emf.henshin.interpreter.impl.UnitApplicationImpl;
import org.eclipse.emf.henshin.interpreter.util.InterpreterUtil;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.resource.HenshinResourceSet;

import rules.ApproachA;
import rules.ApproachB;
import rules.approachA.EnableNodeRuleGenerator;
import rules.approachB.AndJoinRule;
import rules.approachB.AndSplitRule;
import rules.approachB.InitialD2ERule;
import rules.approachB.SimpleFlowRule;
import rules.approachB.XorJoinRule;
import rules.approachB.XorLoopExitRule;
import rules.approachB.XorSplitRule;
import rules.shared.E2RRule;
import rules.shared.R2FRule;
import statespace.normal.State;
import statespace.normal.StatespaceExplorer;
import statespace.sweepline.SweeplineStatespaceExplorer;
import statespace.sweepline.SweeplineStatespaceExplorer;
import statespace.sweepline.progressMeasures.IProgressMapper;
import statespace.sweepline.progressMeasures.Strategy0;
import statespace.sweepline.progressMeasures.StrategyA1;
import statespace.sweepline.progressMeasures.StrategyB1;
import statespace.sweepline.progressMeasures.old.StrategyA2;
import statespace.sweepline.progressMeasures.old.StrategyB2;
import statespace.sweepline.progressMeasures.old.StrategyC1;
import statespace.sweepline.progressMeasures.old.StrategyC2;
import statespace.sweepline.progressMeasures.old.StrategyC3;
import statespace.sweepline.progressMeasures.old.StrategyC4;
import util.DERFConstants;
import util.TransformationUtil;
import util.DERFUtil;
import no.hib.dpf.diagram.DGraph;
import no.hib.dpf.diagram.DNode;
import no.hib.dpf.diagram.DSpecification;
import no.hib.dpf.diagram.DiagramFactory;
import no.hib.dpf.editor.DPFUtils;
import no.hib.dpf.utils.DPFCoreUtil;

public class Main {
	public static final HenshinFactory hf = HenshinFactory.eINSTANCE;
	public static final CorePackage cp = CorePackage.eINSTANCE;
	public static HenshinResourceSet hrs = new HenshinResourceSet();

	public static final String modelPath = "testmodels/";

	public static final String test = modelPath + "test.xmi";
	public static final String thesis = modelPath + "thesis.xmi";
	public static final String mixed = modelPath + "mixed.xmi";

	public static final String modelC = modelPath + "modelC.xmi";
	public static final String modelD = modelPath + "modelD.xmi";
	
	public static void main(String[] args) {
		Specification derfImplementation = DERFUtil.getSpecification(thesis);
		
		Specification initialState = ApproachB.getInitialState(derfImplementation);
		List<Rule> transformationRules = ApproachB.getAllRules();
		
//		StatespaceExplorer sse = statespaceTest(initialState, transformationRules);
//		sweeplineTest2(derfImplementation, initialState, transformationRules, new Strategy0());
		
		testAllSwe();
//		testAllSse();
		
//		printStrategy(new StrategyA1(), derfImplementation);
//		printStrategy(new StrategyA2(), derfImplementation);
//		printStrategy(new StrategyA3(), derfImplementation);
//		printStrategy(new StrategyB1(), derfImplementation);
//		printStrategy(new StrategyB2(), derfImplementation);
//		printStrategy(new StrategyC1(), derfImplementation);
//		printStrategy(new StrategyC2(), derfImplementation);
//		printStrategy(new StrategyC3(), derfImplementation);
//		printStrategy(new StrategyC4(), derfImplementation);
//		printStrategy(new StrategyD1(), derfImplementation);
//		printStrategy(new StrategyD2(), derfImplementation);
		
//		ruleTest(initialState, transformationRules);
//		ruleTest(initialState, transformationRules);
//		ruleTest(initialState, transformationRules);
//		ruleTest(initialState, transformationRules);
//		ruleTest(initialState, transformationRules);
//		ruleTest(initialState, transformationRules);
//		ruleTest(initialState, transformationRules);
//		ruleTest(initialState, transformationRules);
//		ruleTest(initialState, transformationRules);
		
	}
	
	private static void testAllSse() {
		String[] models = {
//				thesis,
//				mixed, 
//				modelC,
				modelD
			};
		
		List<Rule> rules = ApproachB.getAllRules();
		
		for(String model : models) {
			Specification derfImplementation = DERFUtil.getSpecification(model);
			Specification derfInterpretation = DERFUtil.createDERFInterpretation(derfImplementation);
			
			
			StatespaceExplorer sse = new StatespaceExplorer(rules, derfInterpretation);
			
			long timer = System.currentTimeMillis();
			while(sse.step());
			timer = System.currentTimeMillis() - timer;
			
			System.out.println("s: " + sse.getStatespaceGraph().getStates().size());
			System.out.println("e: " + sse.getStatespaceGraph().getEdges().size());
			System.out.println(sse);
			System.out.println("Total time: " + timer);
		}
		
	}

	private static void testAllSwe() {
		String[] models = {
			thesis,
//			mixed, 
//			modelC,
//			modelD
		};
		
		IProgressMapper[] progressMappers = {
			new Strategy0(),
//			new StrategyA1(),
//			new StrategyA2(),
//			new StrategyB1(),
//			new StrategyB2(),
//			new StrategyC1(),
//			new StrategyC2(),
//			new StrategyC3(),
//			new StrategyC4(),
//			new StrategyD1(),
//			new StrategyD2(),
		};
		
		List<Rule> rules = ApproachB.getAllRules();
		
		String result = "";
		
		for(String model : models) {
			result += "\n" + model;
			
			Specification derfImplementation = DERFUtil.getSpecification(model);
			Specification derfInterpretation = DERFUtil.createDERFInterpretation(derfImplementation);
			Specification derfInterpretationCopy = DERFUtil.copySpecification(derfInterpretation);
			
			StatespaceExplorer sse = new StatespaceExplorer(rules, derfInterpretationCopy);
			while(sse.step());
			
			for(IProgressMapper progressMapper : progressMappers) {
				result += "\n" + progressMapper.getClass().getName() + ": ";
				
				derfInterpretationCopy = DERFUtil.copySpecification(derfInterpretation);
				SweeplineStatespaceExplorer swe = new SweeplineStatespaceExplorer(rules, derfImplementation, derfInterpretationCopy, progressMapper);
				
				swe.step();
				while(sse.step());
				
				result += swe.toShortString();
//				result += " (" + swe.getStatespaceGraph().equals(sse.getStatespaceGraph()) + ")";
			}
		}
		
		System.out.println(result);
	}
	
	private static void printStrategy(IProgressMapper strategy, Specification derfImplementation) {
		strategy.initializeProgressMapper(derfImplementation);
		System.out.println(strategy);
	}
	
	public static DSpecification createDSpecification() {
		Specification implementation = DERFUtil.getSpecification(thesis);
		Specification interpretation = DERFUtil.createDERFInterpretation(implementation);
		
		DSpecification dImplementation = no.hib.dpf.editor.DPFUtils.loadDSpecification(URI.createFileURI(thesis.replace(".xmi", ".dpf")));
		dImplementation.setSpecification(implementation);
//		dImplementation.getDGraph().setGraph(implementation.getGraph());
//		
//		Node someNode = implementation.getGraph().getNodeByName("A");
//		System.out.println(someNode);
//		DNode someDNode = dImplementation.getDGraph().getDNode(someNode);
//		System.out.println(someDNode);
		
		return dImplementation;
	}
	
	public static void sweeplineTest(Specification derfImplementation, Specification derfInterpretation, List<Rule> rules, IProgressMapper progressMapper) {
		System.out.println("Starting sweepline test..");
		
		Specification derfInterpretationCopy = DERFUtil.copySpecification(derfInterpretation);

//		SweeplineStatespaceExplorer swe = new SweeplineStatespaceExplorer(rules, derfImplementation, derfInterpretation, progressMapper);

		SweeplineStatespaceExplorer swe = new SweeplineStatespaceExplorer(rules, derfImplementation, derfInterpretation, progressMapper);
		StatespaceExplorer sse = new StatespaceExplorer(rules, derfInterpretationCopy);
		
		swe.step(1);
		while(sse.step());
		
		System.out.println("\n------------------------------------------\n");
		System.out.println(swe);
		System.out.println("\n------------------------------------------\n");
		System.out.println("Correct state space? " + swe.getStatespaceGraph().equals(sse.getStatespaceGraph()));
//		System.out.println(swe.getStatespaceGraph());
	}
	
	public static void sweeplineTest2(Specification derfImplementation, Specification derfInterpretation, List<Rule> rules, IProgressMapper progressMapper) {
		System.out.println("Starting sweepline test..");
		
		Specification derfInterpretationCopy = DERFUtil.copySpecification(derfInterpretation);

//		SweeplineStatespaceExplorer swe = new SweeplineStatespaceExplorer(rules, derfImplementation, derfInterpretation, progressMapper);

		SweeplineStatespaceExplorer swe = new SweeplineStatespaceExplorer(rules, derfImplementation, derfInterpretation, progressMapper);
		StatespaceExplorer sse = new StatespaceExplorer(rules, derfInterpretationCopy);
		
		swe.step(1);
		while(sse.step());
		
		System.out.println("\n------------------------------------------\n");
		System.out.println(swe);
		System.out.println("\n------------------------------------------\n");
		System.out.println("Correct state space? " + swe.getStatespaceGraph().equals(sse.getStatespaceGraph()));
//		System.out.println(swe.getStatespaceGraph());
	}
	
	public static StatespaceExplorer statespaceTest(Specification derfInterpretation, List<Rule> rules) {		
		StatespaceExplorer sse = new StatespaceExplorer(rules, derfInterpretation);
		
		while(sse.step());
		
		System.out.println("\n------------------------------------------\n");
		System.out.println(sse.toString());	
		
		return sse;
	}
	
	private static void printTask(Node task) {
		System.out.println(task.getName());
		
		for(Constraint c : task.getConstraints()) {
			System.out.println(c.getPredicate().getSymbol() + ": ");
			System.out.println(c.getNodes());
			System.out.println(c.getArrows());
		}
		
		System.out.println();
	}
	
	private static void printFlow(Arrow flow) {
		System.out.println(flow.getName());
		
		for(Constraint c : flow.getConstraints()) {
			System.out.println(c.getPredicate().getSymbol() + ": ");
			System.out.println(c.getNodes());
			System.out.println(c.getArrows());
		}
		
		System.out.println();
	}
	
	public static void ruleTest(Specification state, List<Rule> rules) {
		for(Rule rule : rules) {
			System.out.println(rule.getName() + " =>");
			transformAndPrint(state, rule);
		}
	}
	
	private static void transformAndPrint(Specification s, Rule r) {
		EGraph specEGraph = new EGraphImpl(s);
		Engine engine = new EngineImpl();
		UnitApplication unitApp = new UnitApplicationImpl(engine, specEGraph, r, null);
		
		try {
			InterpreterUtil.executeOrDie(unitApp);
		} catch (AssertionError ae) {
			System.out.println(ae.toString());
		} catch (Exception e) {
			System.out.println(e);
		}

		print(s);
		System.out.println();
	}

	public static void print(Specification spec) {
		for (Node n : spec.getGraph().getNodes()) {
			System.out.print(n.getName());
			for (Constraint c : n.getConstraints()) {
				System.out.print(" [" + c.getPredicate().getSymbol() + "]");
			}
			System.out.println(": " + n.getTypeName());
		}

		for (Arrow a : spec.getGraph().getArrows()) {
			System.out.print(a.getName());
			for (Constraint c : a.getConstraints()) {
				System.out.print(" [" + c.getPredicate().getSymbol() + "]");
			}
			System.out.print(" (" + a.getSource().getName() + ", " + a.getTarget().getName() + ")");
			System.out.println(": " + a.getTypeName());
		}
	}

	public static void saveRule(Rule r) {
		Module m = HenshinFactory.eINSTANCE.createModule();
		m.getUnits().add(r);
		hrs.saveEObject(m, modelPath + r.getName() + ".henshin");
	}
}
