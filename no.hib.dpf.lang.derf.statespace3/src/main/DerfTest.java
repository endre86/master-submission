package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import no.hib.dpf.core.Arrow;
import no.hib.dpf.core.Graph;
import no.hib.dpf.core.Node;
import no.hib.dpf.utils.Checker;

public class DerfTest {

	public static boolean check(Map<String, String> paras, Graph graph, Map<Node, List<Node>> nodeMap,
			Map<Arrow, List<Arrow>> arrowMap) {
		
		int numKeys = 0;
		for(Arrow a : arrowMap.keySet()) {
			numKeys += 1;
		}
		
		if(numKeys != 2) {
			return false;
		}

		return true;
	}
}

