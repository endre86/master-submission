package main;

import java.util.List;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.FileChooserUI;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.henshin.model.Rule;

import no.hib.dpf.core.Specification;
import no.hib.dpf.utils.DPFCoreUtil;
import rules.ApproachB;
import statespace.normal.StatespaceExplorer;
import statespace.sweepline.SweeplineStatespaceExplorer;
import statespace.sweepline.progressMeasures.IProgressMapper;
import statespace.sweepline.progressMeasures.StrategyA1;
import statespace.sweepline.progressMeasures.StrategyB1;
import util.DERFUtil;

public class Program {

	private static final String CHOICE_EXIT = "e";
	private static final String CHOICE_NORMAL_STATE_SPACE_CHOICE = "a";
	private static final String CHOICE_SWEEPLINE_STATE_SPACE = "b";

	private static final String CHOICE_SWEEPLINE_STRATEGY_A = "a";
	private static final String CHOICE_SWEEPLINE_STRATEGY_B = "b";
	
	private static final String CHOICE_TESTMODEL_A = "a";
	private static final String CHOICE_TESTMODEL_B = "b";
	private static final String CHOICE_TESTMODEL_C = "c";
	private static final String CHOICE_TESTMODEL_D = "d";
	private static final String CHOICE_USERDEFINED_MODEL = "u";
	
	private static final String CHOICE_DO_FULL_SSE_EXPLORATION = "f";
	
	private static final String TESTMODEL_A = "testmodels/thesis.xmi";
	private static final String TESTMODEL_B = "testmodels/mixed.xmi";
	private static final String TESTMODEL_C = "testmodels/modelC.xmi";
	private static final String TESTMODEL_D = "testmodels/modelD.xmi";
	
	private List<Rule> rules = ApproachB.getAllRules();
	private Scanner input = new Scanner(System.in);
	
	
	public static void main(String[] args) {
		Program program = new Program();
		program.run();
	}

	private void run() {
		System.out.println("Please choose which state space exploration method you wish to use");
		System.out.println("by typing the indicating letter and pressing enter.");
		System.out.println("\t(" + CHOICE_NORMAL_STATE_SPACE_CHOICE + ") Normal state space exploring.");
		System.out.println("\t(" + CHOICE_SWEEPLINE_STATE_SPACE + ") Sweep-line state space exporing.");
		System.out.println("\t(" + CHOICE_EXIT + ") Exit the program");
		
		String answer = input.nextLine();
		
		if(answer.toLowerCase().equals(CHOICE_NORMAL_STATE_SPACE_CHOICE)) {
			System.out.println("\n The normal state space exploration method has been chosen.\n");
			doNormalStateSpaceExploring();
		}
		else if(answer.toLowerCase().equals(CHOICE_SWEEPLINE_STATE_SPACE)) {
			System.out.println("\n The sweep-line method has been chosen.\n");
			doSweepLineStateSpaceExploring();
		}
		else if(answer.toLowerCase().equals(CHOICE_EXIT)) {
			exit();
		}
		else {
			System.out.println("Could not recognize the input you supplied. Please try again.");
			run();
		}
	}

	private void doNormalStateSpaceExploring() {		
		Specification derfImplementation = chooseDerfImplementation();
		Specification derfInterpretation = DERFUtil.createDERFInterpretation(derfImplementation);
		
		StatespaceExplorer sse = new StatespaceExplorer(rules, derfInterpretation);
		
		System.out.println("You can choose how many steps the state space explorer shoud do at a time.");
		System.out.println("When the exploration has finished the steps, the current state space will print.");
		System.out.println("If there are more valid steps, you can choose how many steps you want to do again.");
		System.out.println("\t(Any integer) Enter any number to preform that number of steps.");
		System.out.println("\t(" + CHOICE_DO_FULL_SSE_EXPLORATION + ") Enter " + CHOICE_DO_FULL_SSE_EXPLORATION + " to go through the entire state space exploration.");
		System.out.println("\t(" + CHOICE_EXIT + ") Exit the program");
		
		while(true) {
			System.out.println("\n\nPlease enter your choice of steps: ");
			String answer = input.nextLine();
			
			if(answer.equals(CHOICE_EXIT)) {
				exit();
			}
			else if(answer.equals(CHOICE_DO_FULL_SSE_EXPLORATION)) {
				while(sse.step());
				System.out.println(sse);
				break;
			}
			else {
				int steps;
				
				try {
					steps = Integer.parseInt(answer);
					boolean notFinished = sse.step(steps);
					System.out.println(sse);
					
					if(!notFinished) {
						break;
					}
				}
				catch(Exception e) {
					System.out.println("Could not recognize the input you supplied. Please try again.");
				}
			}
		}
		
		System.out.println("The state space exploration is completed.");
	}

	private void doSweepLineStateSpaceExploring() {
		IProgressMapper strategy = chooseStrategy();
		Specification derfImplementation = chooseDerfImplementation();
		Specification derfInterpretation = DERFUtil.createDERFInterpretation(derfImplementation);
		
		SweeplineStatespaceExplorer swe = new SweeplineStatespaceExplorer(rules, derfImplementation, derfInterpretation, strategy);
		swe.step();
		
		System.out.println("\n\n\n" + swe.toString());
	}

	private Specification chooseDerfImplementation() {
		System.out.println("Please choose a DERF Implementation model:");
		System.out.println("\t(" + CHOICE_TESTMODEL_A + ") Model A (from thesis)");
		System.out.println("\t(" + CHOICE_TESTMODEL_B + ") Model B (from thesis)");
		System.out.println("\t(" + CHOICE_TESTMODEL_C + ") Model C (from thesis)");
		System.out.println("\t(" + CHOICE_TESTMODEL_D + ") Model D (from thesis)");
		System.out.println("\t(" + CHOICE_USERDEFINED_MODEL + ") Another model (opens up a filepicker)");
		System.out.println("\t(" + CHOICE_EXIT + ") Exit the program");
		
		String answer = input.nextLine();

		if(answer.toLowerCase().equals(CHOICE_TESTMODEL_A)) {
			System.out.println("\nYou have selected the Model A\n");
			return DERFUtil.getSpecification(TESTMODEL_A);
		}
		else if(answer.toLowerCase().equals(CHOICE_TESTMODEL_B)) {
			System.out.println("\nYou have selected the Model B\n");
			return DERFUtil.getSpecification(TESTMODEL_B);
		}
		else if(answer.toLowerCase().equals(CHOICE_TESTMODEL_C)) {
			System.out.println("\nYou have selected the Model C\n");
			return DERFUtil.getSpecification(TESTMODEL_C);
		}
		else if(answer.toLowerCase().equals(CHOICE_TESTMODEL_D)) {
			System.out.println("\nYou have selected the Model D\n");
			return DERFUtil.getSpecification(TESTMODEL_D);
		}
		else if(answer.toLowerCase().equals(CHOICE_USERDEFINED_MODEL)) {
			return getUserdefinedModel();
		}
		else if(answer.toLowerCase().equals(CHOICE_EXIT)) {
			exit();
		}
		else {
			System.out.println("Could not recognize the input you supplied. Please try again.");
			chooseDerfImplementation();
		}
		
		return null;
	}

	private Specification getUserdefinedModel() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
		fileChooser.setName("Choose your DERF Implementation model.");
		FileNameExtensionFilter xmiFilter = new FileNameExtensionFilter("DPF Files", "dpf");
		fileChooser.setFileFilter(xmiFilter);
		
		int returnVal = fileChooser.showOpenDialog(null);
		if(returnVal == fileChooser.APPROVE_OPTION) {
			String filePath = fileChooser.getSelectedFile().getPath();
			filePath = filePath.replaceAll("dpf", "xmi");
			Specification derfImplementation = DERFUtil.getSpecification(filePath);
			System.out.println(derfImplementation);
			System.out.println(filePath);
		}
		else {
			System.out.println("You did not select a valid file. Please choose a *.dpf file.");
		}
		
		return null;
	}

	private IProgressMapper chooseStrategy() {
		System.out.println("Please choose which progress measure strategy you want to use:");
		System.out.println("\t(" + CHOICE_SWEEPLINE_STRATEGY_A + ") Strategy A");
		System.out.println("\t(" + CHOICE_SWEEPLINE_STRATEGY_B + ") Strategy B");
		System.out.println("\t(" + CHOICE_EXIT + ") Exit the program");
		
		String answer = input.nextLine();
		
		if(answer.toLowerCase().equals(CHOICE_SWEEPLINE_STRATEGY_A)) {
			System.out.println("\n Strategy A has been chosen for progress measures.\n");
			return new StrategyA1();
		}
		else if(answer.toLowerCase().equals(CHOICE_SWEEPLINE_STRATEGY_B)) {
			System.out.println("\n Strategy B has been chosen for progress measures.\n");
			return new StrategyB1();
		}
		else if(answer.toLowerCase().equals(CHOICE_EXIT)) {
			exit();
		}
		else {
			System.out.println("Could not recognize the input you supplied. Please try again.");
			chooseStrategy();
		}
		return null;
	}

	private void exit() {
		System.out.println("Goodbye!");
		System.exit(0);
	}
	
}
