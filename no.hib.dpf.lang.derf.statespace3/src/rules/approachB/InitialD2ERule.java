package rules.approachB;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.DELETE;
import static org.eclipse.emf.henshin.model.Action.Type.FORBID;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static org.eclipse.emf.henshin.model.Action.Type.REQUIRE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.TASK;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.RuleUtil;

public class InitialD2ERule implements IRule {

	private static CorePackage cp = CorePackage.eINSTANCE;
	
	@Override
	public Rule createRule() {
		Rule rule = RuleUtil.createRule("[D] => [E]");
		
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		// Core
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE);
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE);
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, "graph");
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);		
		
		// Predicate D
		Node dPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(dPredicate, cp.getPredicate_Symbol(), PREDICATE_DISABLED, REQUIRE);
		Node dPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node dPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, dPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(dPredicate, dPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(dPredicateGraph, dPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate E
		Node ePredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(ePredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, REQUIRE);
		Node ePredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node ePredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, ePredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(ePredicate, ePredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(ePredicateGraph, ePredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Task
		Node task = createNode(lhs, cp.getNode(), PRESERVE, "task");
		createEdge(graph, task, cp.getGraph_Nodes(), PRESERVE);
		
		// Forbid incomming arrows
		Node flow = createNode(lhs, cp.getArrow(), FORBID);
		Node forbidGraph = flow.getGraph().getNode("graph");
		Node forbidTask = flow.getGraph().getNode("task");
//		Node forbidSource = createNode(lhs, cp.getNode(), FORBID);
//		createEdge(flow, forbidSource, cp.getArrow_Source(), FORBID);
		createEdge(forbidGraph, flow, cp.getGraph_Arrows(), FORBID);
		createEdge(flow, forbidTask, cp.getArrow_Target(), FORBID);
		
		// Remove D predicate
		Node oldConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node oldGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node oldNode2NodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, oldConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(task, oldConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(oldConstraint, dPredicate, cp.getConstraint_Predicate(), DELETE);
		createEdge(oldConstraint, task, cp.getConstraint_Nodes(), PRESERVE);
		createEdge(oldConstraint, oldGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		
		createEdge(oldGraphHomom, oldNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(oldNode2NodeMap, dPredicateNode, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(oldNode2NodeMap, task, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		// Add E predicate
		createEdge(rhs(oldConstraint, rule), rhs(ePredicate, rule), cp.getConstraint_Predicate(), CREATE);
		createEdge(rhs(oldNode2NodeMap, rule), rhs(ePredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
		
//		Node newConstraint = createNode(rhs, cp.getConstraint(), CREATE);
//		Node newGraphHomom = createNode(rhs, cp.getGraphHomomorphism(), CREATE);
//		Node newNode2NodeMap = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
//		
//		createEdge(rhs(specification, rule), newConstraint, cp.getSpecification_Constraints(), CREATE);
//		createEdge(rhs(task, rule), newConstraint, cp.getNode_Constraints(), CREATE);
//		createEdge(newConstraint, rhs(ePredicate, rule), cp.getConstraint_Predicate(), CREATE);
//		createEdge(newConstraint, newGraphHomom, cp.getConstraint_Mappings(), CREATE);
//		
//		createEdge(newGraphHomom, newNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), CREATE);
//		createEdge(newNode2NodeMap, rhs(dPredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
//		createEdge(newNode2NodeMap, rhs(task, rule), cp.getNodeToNodeMap_Value(), CREATE);
		
		return rule;
	}
	
}
