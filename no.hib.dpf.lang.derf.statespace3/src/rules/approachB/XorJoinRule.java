package rules.approachB;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.DELETE;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.PREDICATE_XOR_JOIN;
import static util.DERFConstants.PREDICATE_FLOW_FALSE;
import static util.DERFConstants.PREDICATE_FLOW_TRUE;
import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FINISHED;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.RuleUtil;

public class XorJoinRule implements IRule{
	
	private static CorePackage cp = CorePackage.eINSTANCE;

	@Override
	public Rule createRule() {
		Rule rule = RuleUtil.createRule("[xor_join]");
		
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		// Core
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE);
		Node typeSpecification = createNode(lhs, cp.getSpecification(), PRESERVE);
		createEdge(specification, typeSpecification, cp.getSpecification_Type(), PRESERVE);
		
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE);
		Node typeSignature = createNode(lhs, cp.getSignature(), PRESERVE);
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		createEdge(typeSpecification, typeSignature, cp.getSpecification_Signature(), PRESERVE);
		
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, "graph");
		Node typeGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);
		createEdge(typeSpecification, typeGraph, cp.getSpecification_Graph(), PRESERVE);
		createEdge(graph, typeGraph, cp.getGraph_Type(), PRESERVE);
		
		// Predicate D
		Node dPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(dPredicate, cp.getPredicate_Symbol(), PREDICATE_DISABLED, PRESERVE);
		Node dPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node dPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, dPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(dPredicate, dPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(dPredicateGraph, dPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate E
		Node ePredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(ePredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, PRESERVE);
		Node ePredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node ePredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, ePredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(ePredicate, ePredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(ePredicateGraph, ePredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate F
		Node fPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(fPredicate, cp.getPredicate_Symbol(), PREDICATE_FINISHED, PRESERVE);
		Node fPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node fPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, fPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(fPredicate, fPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(fPredicateGraph, fPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate f nodes & arrow
		Node afPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(afPredicate, cp.getPredicate_Symbol(), PREDICATE_FLOW_FALSE, PRESERVE);
		Node afPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node afPredicateArrow = createNode(lhs, cp.getArrow(), PRESERVE);
		Node afPredicateSource = createNode(lhs, cp.getNode(), PRESERVE);
		Node afPredicateTarget = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, afPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(afPredicate, afPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		
		createEdge(afPredicateGraph, afPredicateArrow, cp.getGraph_Arrows(), PRESERVE);
		createEdge(afPredicateGraph, afPredicateSource, cp.getGraph_Nodes(), PRESERVE);
		createEdge(afPredicateGraph, afPredicateTarget, cp.getGraph_Nodes(), PRESERVE);

		createEdge(afPredicateArrow, afPredicateSource, cp.getArrow_Source(), PRESERVE);
		createEdge(afPredicateArrow, afPredicateTarget, cp.getArrow_Target(), PRESERVE);
		
		// Predicate t nodes & arrow
		Node atPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(atPredicate, cp.getPredicate_Symbol(), PREDICATE_FLOW_TRUE, PRESERVE);
		Node atPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node atPredicateArrow = createNode(lhs, cp.getArrow(), PRESERVE);
		Node atPredicateSource = createNode(lhs, cp.getNode(), PRESERVE);
		Node atPredicateTarget = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, atPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(atPredicate, atPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		
		createEdge(atPredicateGraph, atPredicateArrow, cp.getGraph_Arrows(), PRESERVE);
		createEdge(atPredicateGraph, atPredicateSource, cp.getGraph_Nodes(), PRESERVE);
		createEdge(atPredicateGraph, atPredicateTarget, cp.getGraph_Nodes(), PRESERVE);

		createEdge(atPredicateArrow, atPredicateSource, cp.getArrow_Source(), PRESERVE);
		createEdge(atPredicateArrow, atPredicateTarget, cp.getArrow_Target(), PRESERVE);
		
		// Predicate [xor_join] arrows
		Node xorJoinPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(xorJoinPredicate, cp.getPredicate_Symbol(), PREDICATE_XOR_JOIN, PRESERVE);
		Node xorJoinPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node xorJoinPredicateArrowA = createNode(lhs, cp.getArrow(), PRESERVE);
		Node xorJoinPredicateArrowB = createNode(lhs, cp.getArrow(), PRESERVE);
		
		createEdge(typeSignature, xorJoinPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(xorJoinPredicate, xorJoinPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		
		createEdge(xorJoinPredicateGraph, xorJoinPredicateArrowA, cp.getGraph_Arrows(), PRESERVE);
		createEdge(xorJoinPredicateGraph, xorJoinPredicateArrowB, cp.getGraph_Arrows(), PRESERVE);
		
		
		
		// Source A
		Node sourceA = createNode(lhs, cp.getNode(), PRESERVE);
		Node sourceAType = createNode(lhs, cp.getNode(), PRESERVE);
		createEdge(graph, sourceA, cp.getGraph_Nodes(), PRESERVE);
		createEdge(typeGraph, sourceAType, cp.getGraph_Nodes(), PRESERVE);
		createEdge(sourceA, sourceAType, cp.getNode_TypeNode(), PRESERVE);
		
		// Preserve source F predicate
		Node sourceAConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node sourceAGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node sourceANode2NodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, sourceAConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(sourceA, sourceAConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(sourceAConstraint, fPredicate, cp.getConstraint_Predicate(), PRESERVE);
		createEdge(sourceAConstraint, sourceA, cp.getConstraint_Nodes(), PRESERVE);
		createEdge(sourceAConstraint, sourceAGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		
		createEdge(sourceAGraphHomom, sourceANode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(sourceANode2NodeMap, fPredicateNode, cp.getNodeToNodeMap_Key(), PRESERVE);
		createEdge(sourceANode2NodeMap, sourceA, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		// Source B
		Node sourceB = createNode(lhs, cp.getNode(), PRESERVE);
		Node sourceBType = createNode(lhs, cp.getNode(), PRESERVE);
		createEdge(graph, sourceB, cp.getGraph_Nodes(), PRESERVE);
		createEdge(typeGraph, sourceBType, cp.getGraph_Nodes(), PRESERVE);
		createEdge(sourceB, sourceBType, cp.getNode_TypeNode(), PRESERVE);
		
		
		// Target
		Node target = createNode(lhs, cp.getNode(), PRESERVE);
		Node targetType = createNode(lhs, cp.getNode(), PRESERVE);
		createEdge(graph, target, cp.getGraph_Nodes(), PRESERVE);
		createEdge(typeGraph, targetType, cp.getGraph_Nodes(), PRESERVE);
		createEdge(target, targetType, cp.getNode_TypeNode(), PRESERVE);
		
		// Remove target D predicate
		Node targetConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node targetGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node targetNode2NodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, targetConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(target, targetConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(targetConstraint, dPredicate, cp.getConstraint_Predicate(), DELETE);
		createEdge(targetConstraint, target, cp.getConstraint_Nodes(), PRESERVE);
		createEdge(targetConstraint, targetGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		
		createEdge(targetGraphHomom, targetNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(targetNode2NodeMap, dPredicateNode, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(targetNode2NodeMap, target, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		// Add target E predicate
		createEdge(rhs(targetConstraint, rule), rhs(ePredicate, rule), cp.getConstraint_Predicate(), CREATE);
		createEdge(rhs(targetNode2NodeMap, rule), rhs(ePredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
		
		
		// Flow src A => trg
		Node flowA = createNode(lhs, cp.getArrow(), PRESERVE);
		Node flowAType = createNode(lhs, cp.getArrow(), PRESERVE);
		createEdge(graph, flowA, cp.getGraph_Arrows(), PRESERVE);
		createEdge(typeGraph, flowAType, cp.getGraph_Arrows(), PRESERVE);
		createEdge(flowA, flowAType, cp.getArrow_TypeArrow(), PRESERVE);

		createEdge(flowA, sourceA, cp.getArrow_Source(), PRESERVE);
		createEdge(flowA, target, cp.getArrow_Target(), PRESERVE);
		createEdge(flowAType, sourceAType, cp.getArrow_Source(), PRESERVE);
		createEdge(flowAType, targetType, cp.getArrow_Target(), PRESERVE);
		
		// Remove arrow f predicate
		Node flowAConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node flowAGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		
		Node flowAArrow2ArrowMap = createNode(lhs, cp.getArrowToArrowMap(), PRESERVE);
		Node flowANode2NodeMapSource = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		Node flowANode2NodeMapTarget = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, flowAConstraint, cp.getSpecification_Constraints(), PRESERVE);
//		createEdge(flowAConstraint, afPredicate, cp.getConstraint_Predicate(), DELETE);
		createEdge(flowAConstraint, flowAGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		
		createEdge(flowA, flowAConstraint, cp.getArrow_Constraints(), PRESERVE);
		createEdge(sourceA, flowAConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(target, flowAConstraint, cp.getNode_Constraints(), PRESERVE);
		
		createEdge(flowAConstraint, flowA, cp.getConstraint_Arrows(), PRESERVE);
		createEdge(flowAConstraint, sourceA, cp.getConstraint_Nodes(), PRESERVE);
		createEdge(flowAConstraint, target, cp.getConstraint_Nodes(), PRESERVE);
		
		createEdge(flowAGraphHomom, flowAArrow2ArrowMap, cp.getGraphHomomorphism_ArrowMapping(), PRESERVE);
		createEdge(flowAArrow2ArrowMap, afPredicateArrow, cp.getArrowToArrowMap_Key(), DELETE);
		createEdge(flowAArrow2ArrowMap, flowA, cp.getArrowToArrowMap_Value(), PRESERVE);
		
		createEdge(flowAGraphHomom, flowANode2NodeMapSource, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(flowANode2NodeMapSource, afPredicateSource, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(flowANode2NodeMapSource, sourceA, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		createEdge(flowAGraphHomom, flowANode2NodeMapTarget, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(flowANode2NodeMapTarget, afPredicateTarget, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(flowANode2NodeMapTarget, target, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		// Add arrow t predicate
		createEdge(rhs(flowAConstraint, rule), rhs(atPredicate, rule), cp.getConstraint_Predicate(), CREATE);
		createEdge(rhs(flowAArrow2ArrowMap, rule), rhs(atPredicateArrow, rule), cp.getArrowToArrowMap_Key(), CREATE);
		createEdge(rhs(flowANode2NodeMapSource, rule), rhs(atPredicateSource, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(rhs(flowANode2NodeMapTarget, rule), rhs(atPredicateSource, rule), cp.getNodeToNodeMap_Key(), CREATE);
		
		// Flow src B => trg
		Node flowB = createNode(lhs, cp.getArrow(), PRESERVE);
		Node flowBType = createNode(lhs, cp.getArrow(), PRESERVE);
		createEdge(graph, flowB, cp.getGraph_Arrows(), PRESERVE);
		createEdge(typeGraph, flowBType, cp.getGraph_Arrows(), PRESERVE);
		createEdge(flowB, flowBType, cp.getArrow_TypeArrow(), PRESERVE);

		createEdge(flowB, sourceB, cp.getArrow_Source(), PRESERVE);
		createEdge(flowB, target, cp.getArrow_Target(), PRESERVE);
		createEdge(flowBType, sourceBType, cp.getArrow_Source(), PRESERVE);
		createEdge(flowBType, targetType, cp.getArrow_Target(), PRESERVE);
		
		// Require arrow f predicate
		Node flowBConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		createEdge(flowBConstraint, afPredicate, cp.getConstraint_Predicate(), PRESERVE);
		createEdge(flowBConstraint, flowB, cp.getConstraint_Arrows(), PRESERVE);
		
		// Require (preserve) [xor_split] on type arrows
		Node xorSplitConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		createEdge(xorSplitConstraint, xorJoinPredicate, cp.getConstraint_Predicate(), PRESERVE);
		createEdge(xorSplitConstraint, flowAType, cp.getConstraint_Arrows(), PRESERVE);
		createEdge(xorSplitConstraint, flowBType, cp.getConstraint_Arrows(), PRESERVE);
		
		
		return rule;
	}

}
