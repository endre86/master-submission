package rules.approachB;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.DELETE;
import static org.eclipse.emf.henshin.model.Action.Type.FORBID;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.PREDICATE_XOR_SPLIT;
import static util.DERFConstants.PREDICATE_FLOW_FALSE;
import static util.DERFConstants.PREDICATE_FLOW_TRUE;
import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FINISHED;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.RuleUtil;

public class XorLoopExitRule implements IRule{
	
	private static CorePackage cp = CorePackage.eINSTANCE;

	@Override
	public Rule createRule() {
		Rule rule = RuleUtil.createRule("XorSplitLoopExit");
		
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		// Core
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE);
		Node typeSpecification = createNode(lhs, cp.getSpecification(), PRESERVE);
		createEdge(specification, typeSpecification, cp.getSpecification_Type(), PRESERVE);
		
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE);
		Node typeSignature = createNode(lhs, cp.getSignature(), PRESERVE);
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		createEdge(typeSpecification, typeSignature, cp.getSpecification_Signature(), PRESERVE);
		
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, "graph");
		Node typeGraph = createNode(lhs, cp.getGraph(), PRESERVE, "typegraph");
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);
		createEdge(typeSpecification, typeGraph, cp.getSpecification_Graph(), PRESERVE);
		createEdge(graph, typeGraph, cp.getGraph_Type(), PRESERVE);
		
		// Predicate D
		Node dPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(dPredicate, cp.getPredicate_Symbol(), PREDICATE_DISABLED, PRESERVE);
		Node dPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node dPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, dPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(dPredicate, dPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(dPredicateGraph, dPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate E
		Node ePredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(ePredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, PRESERVE);
		Node ePredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node ePredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, ePredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(ePredicate, ePredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(ePredicateGraph, ePredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate F
		Node fPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(fPredicate, cp.getPredicate_Symbol(), PREDICATE_FINISHED, PRESERVE);
		Node fPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node fPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, fPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(fPredicate, fPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(fPredicateGraph, fPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate f nodes & arrow
		Node afPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(afPredicate, cp.getPredicate_Symbol(), PREDICATE_FLOW_FALSE, PRESERVE);
		Node afPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node afPredicateArrow = createNode(lhs, cp.getArrow(), PRESERVE);
		Node afPredicateSource = createNode(lhs, cp.getNode(), PRESERVE);
		Node afPredicateTarget = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, afPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(afPredicate, afPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		
		createEdge(afPredicateGraph, afPredicateArrow, cp.getGraph_Arrows(), PRESERVE);
		createEdge(afPredicateGraph, afPredicateSource, cp.getGraph_Nodes(), PRESERVE);
		createEdge(afPredicateGraph, afPredicateTarget, cp.getGraph_Nodes(), PRESERVE);

		createEdge(afPredicateArrow, afPredicateSource, cp.getArrow_Source(), PRESERVE);
		createEdge(afPredicateArrow, afPredicateTarget, cp.getArrow_Target(), PRESERVE);
		
		// Predicate t nodes & arrow
		Node atPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(atPredicate, cp.getPredicate_Symbol(), PREDICATE_FLOW_TRUE, PRESERVE);
		Node atPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node atPredicateArrow = createNode(lhs, cp.getArrow(), PRESERVE);
		Node atPredicateSource = createNode(lhs, cp.getNode(), PRESERVE);
		Node atPredicateTarget = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, atPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(atPredicate, atPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		
		createEdge(atPredicateGraph, atPredicateArrow, cp.getGraph_Arrows(), PRESERVE);
		createEdge(atPredicateGraph, atPredicateSource, cp.getGraph_Nodes(), PRESERVE);
		createEdge(atPredicateGraph, atPredicateTarget, cp.getGraph_Nodes(), PRESERVE);

		createEdge(atPredicateArrow, atPredicateSource, cp.getArrow_Source(), PRESERVE);
		createEdge(atPredicateArrow, atPredicateTarget, cp.getArrow_Target(), PRESERVE);
		
		// Predicate [xor_split] arrows
		Node xorSplitPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(xorSplitPredicate, cp.getPredicate_Symbol(), PREDICATE_XOR_SPLIT, PRESERVE);
		Node xorSplitPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node xorSplitPredicateArrowA = createNode(lhs, cp.getArrow(), PRESERVE);
		Node xorSplitPredicateArrowB = createNode(lhs, cp.getArrow(), PRESERVE);
		
		createEdge(typeSignature, xorSplitPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(xorSplitPredicate, xorSplitPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		
		createEdge(xorSplitPredicateGraph, xorSplitPredicateArrowA, cp.getGraph_Arrows(), PRESERVE);
		createEdge(xorSplitPredicateGraph, xorSplitPredicateArrowB, cp.getGraph_Arrows(), PRESERVE);
		
		
		
		// Source
		Node source = createNode(lhs, cp.getNode(), PRESERVE, "source");
		Node sourceType = createNode(lhs, cp.getNode(), PRESERVE);
		createEdge(graph, source, cp.getGraph_Nodes(), PRESERVE);
		createEdge(typeGraph, sourceType, cp.getGraph_Nodes(), PRESERVE);
		createEdge(source, sourceType, cp.getNode_TypeNode(), PRESERVE);
		
		// Preserve source F predicate
		Node sourceConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node sourceGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node sourceNode2NodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, sourceConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(source, sourceConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(sourceConstraint, fPredicate, cp.getConstraint_Predicate(), PRESERVE);
		createEdge(sourceConstraint, source, cp.getConstraint_Nodes(), PRESERVE);
		createEdge(sourceConstraint, sourceGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		
		createEdge(sourceGraphHomom, sourceNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(sourceNode2NodeMap, fPredicateNode, cp.getNodeToNodeMap_Key(), PRESERVE);
		createEdge(sourceNode2NodeMap, source, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		// Target A
		Node targetA = createNode(lhs, cp.getNode(), PRESERVE);
		Node targetAType = createNode(lhs, cp.getNode(), PRESERVE);
		createEdge(graph, targetA, cp.getGraph_Nodes(), PRESERVE);
		createEdge(typeGraph, targetAType, cp.getGraph_Nodes(), PRESERVE);
		createEdge(targetA, targetAType, cp.getNode_TypeNode(), PRESERVE);
		
		// Remove target D predicate
		Node targetAConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node targetAGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node targetANode2NodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, targetAConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(targetA, targetAConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(targetAConstraint, dPredicate, cp.getConstraint_Predicate(), DELETE);
		createEdge(targetAConstraint, targetA, cp.getConstraint_Nodes(), PRESERVE);
		createEdge(targetAConstraint, targetAGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		
		createEdge(targetAGraphHomom, targetANode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(targetANode2NodeMap, dPredicateNode, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(targetANode2NodeMap, targetA, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		// Add target E predicate
		createEdge(rhs(targetAConstraint, rule), rhs(ePredicate, rule), cp.getConstraint_Predicate(), CREATE);
		createEdge(rhs(targetANode2NodeMap, rule), rhs(ePredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
		
		// Target B
		Node targetB = createNode(lhs, cp.getNode(), FORBID, "targetB");
		Node targetBType = createNode(lhs, cp.getNode(), PRESERVE, "targetBType");
		
		Node forbidGraph = targetB.getGraph().getNode("graph");
		Node forbidTargetBType = targetB.getGraph().getNode("targetBType");
		
		createEdge(forbidGraph, targetB, cp.getGraph_Nodes(), FORBID);
		createEdge(typeGraph, targetBType, cp.getGraph_Nodes(), PRESERVE);
		createEdge(targetB, targetBType, cp.getNode_TypeNode(), FORBID);
		
		// Flow src => trg A
		Node flowA = createNode(lhs, cp.getArrow(), PRESERVE);
		Node flowAType = createNode(lhs, cp.getArrow(), PRESERVE);
		createEdge(graph, flowA, cp.getGraph_Arrows(), PRESERVE);
		createEdge(typeGraph, flowAType, cp.getGraph_Arrows(), PRESERVE);
		createEdge(flowA, flowAType, cp.getArrow_TypeArrow(), PRESERVE);

		createEdge(flowA, source, cp.getArrow_Source(), PRESERVE);
		createEdge(flowA, targetA, cp.getArrow_Target(), PRESERVE);
		createEdge(flowAType, sourceType, cp.getArrow_Source(), PRESERVE);
		createEdge(flowAType, targetAType, cp.getArrow_Target(), PRESERVE);
		
		// Remove arrow f predicate
		Node flowAConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node flowAGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		
		Node flowAArrow2ArrowMap = createNode(lhs, cp.getArrowToArrowMap(), PRESERVE);
		Node flowANode2NodeMapSource = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		Node flowANode2NodeMapTarget = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, flowAConstraint, cp.getSpecification_Constraints(), PRESERVE);
//		createEdge(flowAConstraint, afPredicate, cp.getConstraint_Predicate(), DELETE);
		createEdge(flowAConstraint, flowAGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		
		createEdge(flowA, flowAConstraint, cp.getArrow_Constraints(), PRESERVE);
		createEdge(source, flowAConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(targetA, flowAConstraint, cp.getNode_Constraints(), PRESERVE);
		
		createEdge(flowAConstraint, flowA, cp.getConstraint_Arrows(), PRESERVE);
		createEdge(flowAConstraint, source, cp.getConstraint_Nodes(), PRESERVE);
		createEdge(flowAConstraint, targetA, cp.getConstraint_Nodes(), PRESERVE);
		
		createEdge(flowAGraphHomom, flowAArrow2ArrowMap, cp.getGraphHomomorphism_ArrowMapping(), PRESERVE);
		createEdge(flowAArrow2ArrowMap, afPredicateArrow, cp.getArrowToArrowMap_Key(), DELETE);
		createEdge(flowAArrow2ArrowMap, flowA, cp.getArrowToArrowMap_Value(), PRESERVE);
		
		createEdge(flowAGraphHomom, flowANode2NodeMapSource, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(flowANode2NodeMapSource, afPredicateSource, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(flowANode2NodeMapSource, source, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		createEdge(flowAGraphHomom, flowANode2NodeMapTarget, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(flowANode2NodeMapTarget, afPredicateTarget, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(flowANode2NodeMapTarget, targetA, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		// Add arrow t predicate
		createEdge(rhs(flowAConstraint, rule), rhs(atPredicate, rule), cp.getConstraint_Predicate(), CREATE);
		createEdge(rhs(flowAArrow2ArrowMap, rule), rhs(atPredicateArrow, rule), cp.getArrowToArrowMap_Key(), CREATE);
		createEdge(rhs(flowANode2NodeMapSource, rule), rhs(atPredicateSource, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(rhs(flowANode2NodeMapTarget, rule), rhs(atPredicateSource, rule), cp.getNodeToNodeMap_Key(), CREATE);
		

		// Flow src => trg B
		
		Node flowB = createNode(lhs, cp.getArrow(), FORBID, "flowb");
		Node flowBType = createNode(lhs, cp.getArrow(), PRESERVE, "flowBType");		
		
		Node forbidXorSplitConstraint = flowB.getGraph().getNode("xorSplitConstraint");
		Node forbidFlowBType = flowB.getGraph().getNode("flowBType");
		Node forbidSource = flowB.getGraph().getNode("source");
		System.out.println(forbidFlowBType);
		
		createEdge(forbidGraph, flowB, cp.getGraph_Arrows(), FORBID);
		createEdge(typeGraph, flowBType, cp.getGraph_Arrows(), PRESERVE);
		createEdge(flowB, forbidFlowBType, cp.getArrow_TypeArrow(), FORBID);
		
		Node xorSplitConstraint = createNode(lhs, cp.getConstraint(), PRESERVE, "xorSplitConstraint");
		createEdge(xorSplitConstraint, xorSplitPredicate, cp.getConstraint_Predicate(), PRESERVE);
		createEdge(xorSplitConstraint, flowAType, cp.getConstraint_Arrows(), PRESERVE);
		createEdge(xorSplitConstraint, flowBType, cp.getConstraint_Arrows(), PRESERVE);

		createEdge(flowB, forbidSource, cp.getArrow_Source(), FORBID);
		createEdge(flowB, targetB, cp.getArrow_Target(), FORBID);
		createEdge(flowBType, sourceType, cp.getArrow_Source(), PRESERVE);
		createEdge(flowBType, targetBType, cp.getArrow_Target(), PRESERVE);
		
		// Require arrow f predicate
//		Node flowBConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
//		createEdge(flowBConstraint, afPredicate, cp.getConstraint_Predicate(), PRESERVE);
//		createEdge(flowBConstraint, flowB, cp.getConstraint_Arrows(), PRESERVE);
		
		// Require (preserve) [and_split] on type arrows
		
		
		
		
		return rule;
	}

}
