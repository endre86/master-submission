package rules.approachB;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.DELETE;
import static org.eclipse.emf.henshin.model.Action.Type.FORBID;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.PREDICATE_AND_SPLIT;
import static util.DERFConstants.PREDICATE_FLOW_FALSE;
import static util.DERFConstants.PREDICATE_FLOW_TRUE;
import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FINISHED;
import static util.DERFConstants.PREDICATE_AND_JOIN;
import static util.DERFConstants.PREDICATE_XOR_JOIN;
import static util.DERFConstants.PREDICATE_XOR_SPLIT;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.RuleUtil;

public class SimpleFlowRule implements IRule{
	
	private static CorePackage cp = CorePackage.eINSTANCE;

	@Override
	public Rule createRule() {
		Rule rule = RuleUtil.createRule("SimpleFlow");
		
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		// Core
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE);
		Node typeSpecification = createNode(lhs, cp.getSpecification(), PRESERVE);
		createEdge(specification, typeSpecification, cp.getSpecification_Type(), PRESERVE);
		
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE);
		Node typeSignature = createNode(lhs, cp.getSignature(), PRESERVE);
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		createEdge(typeSpecification, typeSignature, cp.getSpecification_Signature(), PRESERVE);
		
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, "graph");
		Node typeGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);
		createEdge(typeSpecification, typeGraph, cp.getSpecification_Graph(), PRESERVE);
		createEdge(graph, typeGraph, cp.getGraph_Type(), PRESERVE);
		
		// Predicate D
		Node dPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(dPredicate, cp.getPredicate_Symbol(), PREDICATE_DISABLED, PRESERVE);
		Node dPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node dPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, dPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(dPredicate, dPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(dPredicateGraph, dPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate E
		Node ePredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(ePredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, PRESERVE);
		Node ePredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node ePredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, ePredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(ePredicate, ePredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(ePredicateGraph, ePredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate F
		Node fPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(fPredicate, cp.getPredicate_Symbol(), PREDICATE_FINISHED, PRESERVE);
		Node fPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node fPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, fPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(fPredicate, fPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(fPredicateGraph, fPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate f nodes & arrow
		Node afPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(afPredicate, cp.getPredicate_Symbol(), PREDICATE_FLOW_FALSE, PRESERVE);
		Node afPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node afPredicateArrow = createNode(lhs, cp.getArrow(), PRESERVE);
		Node afPredicateSource = createNode(lhs, cp.getNode(), PRESERVE);
		Node afPredicateTarget = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, afPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(afPredicate, afPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		
		createEdge(afPredicateGraph, afPredicateArrow, cp.getGraph_Arrows(), PRESERVE);
		createEdge(afPredicateGraph, afPredicateSource, cp.getGraph_Nodes(), PRESERVE);
		createEdge(afPredicateGraph, afPredicateTarget, cp.getGraph_Nodes(), PRESERVE);

		createEdge(afPredicateArrow, afPredicateSource, cp.getArrow_Source(), PRESERVE);
		createEdge(afPredicateArrow, afPredicateTarget, cp.getArrow_Target(), PRESERVE);
		
		// Predicate t nodes & arrow
		Node atPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(atPredicate, cp.getPredicate_Symbol(), PREDICATE_FLOW_TRUE, PRESERVE);
		Node atPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node atPredicateArrow = createNode(lhs, cp.getArrow(), PRESERVE);
		Node atPredicateSource = createNode(lhs, cp.getNode(), PRESERVE);
		Node atPredicateTarget = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, atPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(atPredicate, atPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		
		createEdge(atPredicateGraph, atPredicateArrow, cp.getGraph_Arrows(), PRESERVE);
		createEdge(atPredicateGraph, atPredicateSource, cp.getGraph_Nodes(), PRESERVE);
		createEdge(atPredicateGraph, atPredicateTarget, cp.getGraph_Nodes(), PRESERVE);

		createEdge(atPredicateArrow, atPredicateSource, cp.getArrow_Source(), PRESERVE);
		createEdge(atPredicateArrow, atPredicateTarget, cp.getArrow_Target(), PRESERVE);	
		
		// DERF Implementation Predicates
		Node andJoinPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(andJoinPredicate, cp.getPredicate_Symbol(), PREDICATE_AND_JOIN, PRESERVE);
		
		Node andSplitPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(andSplitPredicate, cp.getPredicate_Symbol(), PREDICATE_AND_SPLIT, PRESERVE);
		
		Node xorJoinPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(xorJoinPredicate, cp.getPredicate_Symbol(), PREDICATE_XOR_JOIN, PRESERVE);
		
		Node xorSplitPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(xorSplitPredicate, cp.getPredicate_Symbol(), PREDICATE_XOR_SPLIT, PRESERVE);
		
		// Source
		Node source = createNode(lhs, cp.getNode(), PRESERVE);
		Node sourceType = createNode(lhs, cp.getNode(), PRESERVE);
		createEdge(graph, source, cp.getGraph_Nodes(), PRESERVE);
		createEdge(typeGraph, sourceType, cp.getGraph_Nodes(), PRESERVE);
		createEdge(source, sourceType, cp.getNode_TypeNode(), PRESERVE);
		
		// Preserve source F predicate
		Node sourceConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node sourceGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node sourceNode2NodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, sourceConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(source, sourceConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(sourceConstraint, fPredicate, cp.getConstraint_Predicate(), PRESERVE);
		createEdge(sourceConstraint, source, cp.getConstraint_Nodes(), PRESERVE);
		createEdge(sourceConstraint, sourceGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		
		createEdge(sourceGraphHomom, sourceNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(sourceNode2NodeMap, fPredicateNode, cp.getNodeToNodeMap_Key(), PRESERVE);
		createEdge(sourceNode2NodeMap, source, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		// Target A
		Node target = createNode(lhs, cp.getNode(), PRESERVE);
		Node targetType = createNode(lhs, cp.getNode(), PRESERVE);
		createEdge(graph, target, cp.getGraph_Nodes(), PRESERVE);
		createEdge(typeGraph, targetType, cp.getGraph_Nodes(), PRESERVE);
		createEdge(target, targetType, cp.getNode_TypeNode(), PRESERVE);
		
		// Remove target D predicate
		Node targetConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node targetGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node targetNode2NodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, targetConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(target, targetConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(targetConstraint, dPredicate, cp.getConstraint_Predicate(), DELETE);
		createEdge(targetConstraint, target, cp.getConstraint_Nodes(), PRESERVE);
		createEdge(targetConstraint, targetGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		
		createEdge(targetGraphHomom, targetNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(targetNode2NodeMap, dPredicateNode, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(targetNode2NodeMap, target, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		// Add target E predicate
		createEdge(rhs(targetConstraint, rule), rhs(ePredicate, rule), cp.getConstraint_Predicate(), CREATE);
		createEdge(rhs(targetNode2NodeMap, rule), rhs(ePredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
		
		// Flow src => trg A
		Node flow = createNode(lhs, cp.getArrow(), PRESERVE);
		Node flowType = createNode(lhs, cp.getArrow(), PRESERVE, "flowType");
		createEdge(graph, flow, cp.getGraph_Arrows(), PRESERVE);
		createEdge(typeGraph, flowType, cp.getGraph_Arrows(), PRESERVE);
		createEdge(flow, flowType, cp.getArrow_TypeArrow(), PRESERVE);

		createEdge(flow, source, cp.getArrow_Source(), PRESERVE);
		createEdge(flow, target, cp.getArrow_Target(), PRESERVE);
		
		createEdge(flowType, sourceType, cp.getArrow_Source(), PRESERVE);
		createEdge(flowType, targetType, cp.getArrow_Target(), PRESERVE);
		
		// Remove arrow f predicate
		Node flowConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node flowGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		
		Node flowArrow2ArrowMap = createNode(lhs, cp.getArrowToArrowMap(), PRESERVE);
		Node flowNode2NodeMapSource = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		Node flowNode2NodeMapTarget = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, flowConstraint, cp.getSpecification_Constraints(), PRESERVE);
//		createEdge(flowAConstraint, afPredicate, cp.getConstraint_Predicate(), DELETE);
		createEdge(flowConstraint, flowGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		
		createEdge(flow, flowConstraint, cp.getArrow_Constraints(), PRESERVE);
		createEdge(source, flowConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(target, flowConstraint, cp.getNode_Constraints(), PRESERVE);
		
		createEdge(flowConstraint, flow, cp.getConstraint_Arrows(), PRESERVE);
		createEdge(flowConstraint, source, cp.getConstraint_Nodes(), PRESERVE);
		createEdge(flowConstraint, target, cp.getConstraint_Nodes(), PRESERVE);
		
		createEdge(flowGraphHomom, flowArrow2ArrowMap, cp.getGraphHomomorphism_ArrowMapping(), PRESERVE);
		createEdge(flowArrow2ArrowMap, afPredicateArrow, cp.getArrowToArrowMap_Key(), DELETE);
		createEdge(flowArrow2ArrowMap, flow, cp.getArrowToArrowMap_Value(), PRESERVE);
		
		createEdge(flowGraphHomom, flowNode2NodeMapSource, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(flowNode2NodeMapSource, afPredicateSource, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(flowNode2NodeMapSource, source, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		createEdge(flowGraphHomom, flowNode2NodeMapTarget, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(flowNode2NodeMapTarget, afPredicateTarget, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(flowNode2NodeMapTarget, target, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		// Add arrow t predicate
		createEdge(rhs(flowConstraint, rule), rhs(atPredicate, rule), cp.getConstraint_Predicate(), CREATE);
		createEdge(rhs(flowArrow2ArrowMap, rule), rhs(atPredicateArrow, rule), cp.getArrowToArrowMap_Key(), CREATE);
		createEdge(rhs(flowNode2NodeMapSource, rule), rhs(atPredicateSource, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(rhs(flowNode2NodeMapTarget, rule), rhs(atPredicateSource, rule), cp.getNodeToNodeMap_Key(), CREATE);
		
		// Forbid constraints on flow type
		Node flowTypeConstraint = createNode(lhs, cp.getConstraint(), FORBID);
		
		Graph forbidGraph = flowTypeConstraint.getGraph();
		Node forbidFlowType = forbidGraph.getNode("flowType");
		
		createEdge(flowTypeConstraint, forbidFlowType, cp.getConstraint_Arrows(), FORBID);
//		createEdge(flowTypeConstraint, andJoinPredicate, cp.getConstraint_Predicate(), FORBID);
//		createEdge(flowTypeConstraint, andSplitPredicate, cp.getConstraint_Predicate(), FORBID);
//		createEdge(flowTypeConstraint, xorJoinPredicate, cp.getConstraint_Predicate(), FORBID);
//		createEdge(flowTypeConstraint, xorSplitPredicate, cp.getConstraint_Predicate(), FORBID);
		
		
		return rule;
	}

}
