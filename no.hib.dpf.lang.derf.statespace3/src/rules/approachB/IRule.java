package rules.approachB;

import java.util.List;

import no.hib.dpf.core.Node;

import org.eclipse.emf.henshin.model.Rule;

/**
 * General interface for all Rule classes in Approach A.
 * @author endre
 *
 */
public interface IRule {
	/**
	 * Generates a Henshin transformation rule.
	 * 
	 * @return
	 * 		The generated rule
	 */
	Rule createRule();

}
