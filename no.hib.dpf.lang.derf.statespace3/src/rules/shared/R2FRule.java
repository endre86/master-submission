package rules.shared;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.DELETE;
import static org.eclipse.emf.henshin.model.Action.Type.FORBID;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FINISHED;
import static util.DERFConstants.PREDICATE_RUNNING;
import static util.DERFConstants.TASK;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.RuleUtil;
import rules.approachB.IRule;

public class R2FRule implements IRule{

	private static CorePackage cp = CorePackage.eINSTANCE;
	
	@Override
	public Rule createRule() {
Rule rule = RuleUtil.createRule("[R] => [F]");
		
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		// Core
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE);
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE);
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, "graph");
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);		
		
		// Predicate R
		Node rPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(rPredicate, cp.getPredicate_Symbol(), PREDICATE_RUNNING, PRESERVE);
		Node rPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node rPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, rPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(rPredicate, rPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(rPredicateGraph, rPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate F
		Node fPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(fPredicate, cp.getPredicate_Symbol(), PREDICATE_FINISHED, PRESERVE);
		Node fPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node fPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, fPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(fPredicate, fPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(fPredicateGraph, fPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Task
		Node task = createNode(lhs, cp.getNode(), PRESERVE, "task");
		createEdge(graph, task, cp.getGraph_Nodes(), PRESERVE);
		
		// Remove R predicate
		Node oldConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node oldGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node oldNode2NodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, oldConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(task, oldConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(oldConstraint, rPredicate, cp.getConstraint_Predicate(), DELETE);
		createEdge(oldConstraint, task, cp.getConstraint_Nodes(), PRESERVE);
		createEdge(oldConstraint, oldGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		
		createEdge(oldGraphHomom, oldNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(oldNode2NodeMap, rPredicateNode, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(oldNode2NodeMap, task, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		// Add F predicate
		createEdge(rhs(oldConstraint, rule), rhs(fPredicate, rule), cp.getConstraint_Predicate(), CREATE);
		createEdge(rhs(oldNode2NodeMap, rule), rhs(fPredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
		
		return rule;
	}
	
}
