package rules;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.henshin.model.Action;
import org.eclipse.emf.henshin.model.Attribute;
import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Formula;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.NestedCondition;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Not;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Action.Type;

public class RuleUtil {
	public static Rule createRule(String name) {
		return HenshinFactory.eINSTANCE.createRule(name);
	}
	
	public static Node rhs(Node node, Rule rule) {
		return rule.getMappings().getImage(node, rule.getRhs());
	}
	
	public static Node createNode(Graph graph, EClass type, Type action) {
		Node node = HenshinFactory.eINSTANCE.createNode(graph, type, null);
		
		if(action != null) {
			node.setAction(new Action(action));
		}
		return node;
	}
	
	public static Node createNode(Graph graph, EClass type, Type action, String name) {
		Node node = HenshinFactory.eINSTANCE.createNode(graph, type, null);
		node.setName(name);
		
		if(action != null) {
			node.setAction(new Action(action));
		}
		return node;
	}
	
	public static Node createNode(Graph graph, EClass type) {
		Node node = HenshinFactory.eINSTANCE.createNode(graph, type, null);
		return node;
	}
	
	public static Attribute createAttr(Node element, EAttribute type, String value, Type action) {
		Attribute attr = HenshinFactory.eINSTANCE.createAttribute(element, type, "\"" + value + "\"");
		
		if(action != null) {
			attr.setAction(new Action(action));
		}
		return attr;
	}
	
	public static Edge createEdge(Node src, Node trg, EReference type, Type action) {
		Edge edge = HenshinFactory.eINSTANCE.createEdge(src, trg, type);
		
		if(action != null) {
			edge.setAction(new Action(action));
		}
		return edge;
	}
}
