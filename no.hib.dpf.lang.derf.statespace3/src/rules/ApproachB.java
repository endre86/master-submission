package rules;

import java.util.LinkedList;
import java.util.List;

import no.hib.dpf.core.Specification;

import org.eclipse.emf.henshin.model.Rule;

import rules.approachB.AndJoinRule;
import rules.approachB.AndSplitRule;
import rules.approachB.InitialD2ERule;
import rules.approachB.SimpleFlowRule;
import rules.approachB.XorJoinRule;
import rules.approachB.XorLoopExitRule;
import rules.approachB.XorSplitRule;
import rules.shared.E2RRule;
import rules.shared.R2FRule;
import util.DERFUtil;

public class ApproachB {
	
	public static List<Rule> getAllRules() {
		List<Rule> rules = new LinkedList<Rule>();
		
		rules.add(new InitialD2ERule().createRule());
		rules.add(new SimpleFlowRule().createRule());
		rules.add(new E2RRule().createRule());
		rules.add(new R2FRule().createRule());
		rules.add(new AndSplitRule().createRule());
		rules.add(new AndJoinRule().createRule());
		rules.add(new XorSplitRule().createRule());
		rules.add(new XorJoinRule().createRule());
		rules.add(new XorLoopExitRule().createRule());
		
		return rules;
	}
	
	public static Specification getInitialState(Specification derfImplementation) {
		return DERFUtil.createDERFInterpretation(derfImplementation);
	}

}
