package rules;

import java.util.LinkedList;
import java.util.List;

import no.hib.dpf.core.Arrow;
import no.hib.dpf.core.Node;
import no.hib.dpf.core.Specification;

import org.eclipse.emf.henshin.model.Rule;

import rules.approachA.AndJoinRuleGenerator;
import rules.approachA.AndSplitRuleGenerator;
import rules.approachA.EnableNodeRuleGenerator;
import rules.approachA.SimpleFlowRuleGenerator;
import rules.approachA.XorJoinRuleGenerator;
import rules.approachA.XorSplitRuleGenerator;
import rules.shared.E2RRule;
import rules.shared.R2FRule;
import util.DERFUtil;

public class ApproachA {
	
	public static List<Rule> getAllRules(Specification derfImplementation) {
		List<Rule> rules = new LinkedList<Rule>();
		
		rules.add(new E2RRule().createRule());
		rules.add(new R2FRule().createRule());
		
		List<Node> initialNodes = new LinkedList<Node>();
		for(Node task : derfImplementation.getGraph().getNodes()) {
			if(task.getIncomings().size() == 0) {
				initialNodes.add(task);
			}
		}
		
		rules.addAll(new EnableNodeRuleGenerator().createRules(initialNodes));
		
//		List<Arrow> simpleFlows = new LinkedList<Arrow>();
//		for(Arrow flow : derfImplementation.getGraph().getArrows()) {
//			simpleFlows.add(flow);
//		}

//		rules.addAll(new SimpleFlowRuleGenerator().createRules(derfImplementation.getGraph().getArrows()));
//		rules.addAll(new AndJoinRuleGenerator().createRules(derfImplementation.getGraph().getArrows()));
//		rules.addAll(new XorJoinRuleGenerator().createRules(derfImplementation.getGraph().getArrows()));
//		rules.addAll(new XorJoinRuleGenerator().createRules(derfImplementation.getGraph().getArrows()));
		rules.addAll(new XorSplitRuleGenerator().createRules(derfImplementation.getGraph().getArrows()));
		
		return rules;
	}
	
	public static Specification getInitialState(Specification derfImplementation) {
		Specification derfInterpretation = DERFUtil.createEmptySpecification(
				derfImplementation, DERFUtil.getDERFInterpretationSignature());
		
		derfInterpretation.setType(derfImplementation);
		derfInterpretation.getGraph().setType(derfImplementation.getGraph());
		
		return derfInterpretation;		
	}
	
}
