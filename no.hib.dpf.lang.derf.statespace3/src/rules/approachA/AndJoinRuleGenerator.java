package rules.approachA;

import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FINISHED;
import static util.DERFConstants.PREDICATE_FLOW_TRUE;

import java.util.LinkedList;
import java.util.List;

import no.hib.dpf.core.Arrow;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

public class AndJoinRuleGenerator implements IFlowRule {

	private static CorePackage cp = CorePackage.eINSTANCE;

	@Override
	/**
	 * @param flows
	 * 		The two flows that are connected in the and_split
	 */
	public List<Rule> createRules(List<Arrow> flows) {
		List<Rule> rules = new LinkedList<Rule>();

		rules.add(createRule(flows.get(0), flows.get(1)));
		
		return rules;
	}

	private Rule createRule(Arrow flowA, Arrow flowB) {
		String flowAName = flowA.getName();
		String flowBName = flowB.getName();
		String sourceAName = flowA.getSource().getName();
		String sourceBName = flowB.getSource().getName();
		String targetName = flowB.getTarget().getName();

		Rule r = ApproachARuleUtil.createRule("[and_join]: " + flowAName + " & " + flowBName);

		Node sourceA = ApproachARuleUtil.requireTask(r, sourceAName, PREDICATE_FINISHED);
		Node sourceB = ApproachARuleUtil.requireTask(r, sourceBName, PREDICATE_FINISHED);
		Node target = ApproachARuleUtil.createTask(r, targetName, PREDICATE_ENABLED);
		Node _flowA = ApproachARuleUtil.createFlow(r, flowAName, sourceA, target, PREDICATE_FLOW_TRUE);
		Node _flowB = ApproachARuleUtil.createFlow(r, flowBName, sourceB, target, PREDICATE_FLOW_TRUE);

		return r;
	}

}
