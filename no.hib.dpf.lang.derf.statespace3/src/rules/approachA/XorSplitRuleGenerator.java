package rules.approachA;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.FORBID;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FINISHED;
import static util.DERFConstants.PREDICATE_FLOW_FALSE;
import static util.DERFConstants.PREDICATE_FLOW_TRUE;
import static util.DERFConstants.PREDICATE_RUNNING;

import java.util.LinkedList;
import java.util.List;

import main.Main;
import no.hib.dpf.core.Arrow;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.henshin.model.Action.Type;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.RuleUtil;
import rules.approachA.ApproachARuleUtil.FlowPredicate;
import rules.approachA.ApproachARuleUtil.SpecificationElements;
import rules.approachA.ApproachARuleUtil.TaskPredicate;
import util.DERFUtil;

public class XorSplitRuleGenerator implements IFlowRule {

	private static CorePackage cp = CorePackage.eINSTANCE;

	@Override
	/**
	 * @param flows
	 * 		The two flows that are connected in the and_split
	 */
	public List<Rule> createRules(List<Arrow> flows) {
		List<Rule> rules = new LinkedList<Rule>();

		rules.add(createRule(flows.get(0), flows.get(1)));
		rules.add(createRule(flows.get(1), flows.get(0)));
		
		return rules;
	}
	
	private Rule createRule(Arrow createFlow, Arrow forbidFlow) {
		String sourceName = createFlow.getSource().getName();
		String createTargetName = createFlow.getTarget().getName();
		String createFlowName = createFlow.getName();
		String forbidFlowName = forbidFlow.getName();
		String forbidTargetName = forbidFlow.getTarget().getName();

		Rule rule = RuleUtil.createRule("[xor_split]" + createFlowName + " NOT " + forbidFlowName);
		
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		// Core
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE, SpecificationElements.SPECIFICATION.name);
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE, SpecificationElements.SIGNATURE.name);
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, SpecificationElements.GRAPH.name);
		Node typeGraph = createNode(lhs, cp.getGraph(), PRESERVE, SpecificationElements.TYPEGRAPH.name);
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);
		createEdge(graph, typeGraph, cp.getGraph_Type(), PRESERVE);
		
		// Predicate D
		Node dPredicate = createNode(lhs, cp.getPredicate(), PRESERVE, TaskPredicate.DISABLED.predicate);
		createAttr(dPredicate, cp.getPredicate_Symbol(), PREDICATE_DISABLED, PRESERVE);
		Node dPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE, TaskPredicate.DISABLED.predicateGraph);
		Node dPredicateNode = createNode(lhs, cp.getNode(), PRESERVE, TaskPredicate.DISABLED.predicateNode);
		
		createEdge(signature, dPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(dPredicate, dPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(dPredicateGraph, dPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate E
		Node ePredicate = createNode(lhs, cp.getPredicate(), PRESERVE, TaskPredicate.ENABLED.predicate);
		createAttr(ePredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, PRESERVE);
		Node ePredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE, TaskPredicate.ENABLED.predicateGraph);
		Node ePredicateNode = createNode(lhs, cp.getNode(), PRESERVE, TaskPredicate.ENABLED.predicateNode);
		
		createEdge(signature, ePredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(ePredicate, ePredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(ePredicateGraph, ePredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate F
		Node fPredicate = createNode(lhs, cp.getPredicate(), PRESERVE, TaskPredicate.FINISHED.predicate);
		createAttr(fPredicate, cp.getPredicate_Symbol(), PREDICATE_FINISHED, PRESERVE);
		Node fPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE, TaskPredicate.FINISHED.predicateGraph);
		Node fPredicateNode = createNode(lhs, cp.getNode(), PRESERVE, TaskPredicate.FINISHED.predicateNode);
		
		createEdge(signature, fPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(fPredicate, fPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(fPredicateGraph, fPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate t nodes & arrow
		Node atPredicate = createNode(lhs, cp.getPredicate(), PRESERVE, FlowPredicate.TRUE.predicate);
		createAttr(atPredicate, cp.getPredicate_Symbol(), PREDICATE_FLOW_TRUE, PRESERVE);
		Node atPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE, FlowPredicate.TRUE.predicateGraph);
		Node atPredicateFlow = createNode(lhs, cp.getArrow(), PRESERVE, FlowPredicate.TRUE.predicateFlow);
		Node atPredicateSource = createNode(lhs, cp.getNode(), PRESERVE, FlowPredicate.TRUE.predicateSource);
		Node atPredicateTarget = createNode(lhs, cp.getNode(), PRESERVE, FlowPredicate.TRUE.predicateTarget);
		
		createEdge(signature, atPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(atPredicate, atPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		
		createEdge(atPredicateGraph, atPredicateFlow, cp.getGraph_Arrows(), PRESERVE);
		createEdge(atPredicateGraph, atPredicateSource, cp.getGraph_Nodes(), PRESERVE);
		createEdge(atPredicateGraph, atPredicateTarget, cp.getGraph_Nodes(), PRESERVE);

		createEdge(atPredicateFlow, atPredicateSource, cp.getArrow_Source(), PRESERVE);
		createEdge(atPredicateFlow, atPredicateTarget, cp.getArrow_Target(), PRESERVE);
		
		// Require source
		Node source = createNode(lhs, cp.getNode(), PRESERVE, "source");
		createAttr(source, cp.getNode_Name(), sourceName, PRESERVE);
		Node sourceType = createNode(lhs, cp.getNode(), PRESERVE);
		createAttr(sourceType, cp.getNode_Name(), sourceName, PRESERVE);
		
		createEdge(graph, source, cp.getGraph_Nodes(), PRESERVE);
		createEdge(typeGraph, sourceType, cp.getGraph_Nodes(), PRESERVE);
		createEdge(source, sourceType, cp.getNode_TypeNode(), CREATE);
		
		Node sourceConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node sourceGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node sourceNode2NodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, sourceConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(source, sourceConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(sourceConstraint, fPredicate, cp.getConstraint_Predicate(), PRESERVE);
		createEdge(sourceConstraint, source, cp.getConstraint_Nodes(), PRESERVE);
		createEdge(sourceConstraint, sourceGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		
		createEdge(sourceGraphHomom, sourceNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(sourceNode2NodeMap, fPredicateNode, cp.getNodeToNodeMap_Key(), PRESERVE);
		createEdge(sourceNode2NodeMap, source, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		// Create target a
		Node targetA = createNode(rhs, cp.getNode(), CREATE, createTargetName);
		createAttr(targetA, cp.getNode_Name(), createTargetName, CREATE);
		Node targetAType = createNode(lhs, cp.getNode(), PRESERVE, "targetAType");
		createAttr(targetAType, cp.getNode_Name(), createTargetName, PRESERVE);
		
		createEdge(rhs(graph, rule), targetA, cp.getGraph_Nodes(), CREATE);
		createEdge(typeGraph, targetAType, cp.getGraph_Nodes(), PRESERVE);
		createEdge(targetA, rhs(targetAType, rule), cp.getNode_TypeNode(), CREATE);
		
		Node targetAConstraint = createNode(rhs, cp.getConstraint(), CREATE);
		Node targetAGraphHomom = createNode(rhs, cp.getGraphHomomorphism(), CREATE);
		Node targetANode2NodeMap = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
		
		createEdge(rhs(specification, rule), targetAConstraint, cp.getSpecification_Constraints(), CREATE);
		createEdge(targetA, targetAConstraint, cp.getNode_Constraints(), CREATE);
		createEdge(targetAConstraint, rhs(ePredicate, rule), cp.getConstraint_Predicate(), CREATE);
		createEdge(targetAConstraint, targetA, cp.getConstraint_Nodes(), CREATE);
		createEdge(targetAConstraint, targetAGraphHomom, cp.getConstraint_Mappings(), CREATE);
		
		createEdge(targetAGraphHomom, targetANode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(targetANode2NodeMap, rhs(ePredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(targetANode2NodeMap, targetA, cp.getNodeToNodeMap_Value(), CREATE);
		
		// Create flow a
		Node flow = createNode(lhs, cp.getArrow(), CREATE, createFlowName);
		createAttr(flow, cp.getNode_Name(), createFlowName, CREATE);
		Node flowType = createNode(lhs, cp.getArrow(), PRESERVE, "flowAType");
		createAttr(flowType, cp.getNode_Name(), createFlowName, PRESERVE);
		
		createEdge(rhs(graph, rule), flow, cp.getGraph_Arrows(), CREATE);
		createEdge(typeGraph, flowType, cp.getGraph_Arrows(), PRESERVE);
		createEdge(flow, rhs(flowType, rule), cp.getArrow_TypeArrow(), CREATE);
		
		createEdge(flow, rhs(source, rule), cp.getArrow_Source(), CREATE);
		createEdge(flow, targetA, cp.getArrow_Target(), CREATE);
		
		Node flowConstraint = createNode(lhs, cp.getConstraint(), CREATE);
		Node flowGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), CREATE);
		Node flowArrow2ArrowMap = createNode(lhs, cp.getArrowToArrowMap(), CREATE);
		Node flowNode2NodeMapSource = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
		Node flowNode2NodeMapTarget = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
		
		createEdge(rhs(specification, rule), flowConstraint, cp.getSpecification_Constraints(), CREATE);
		createEdge(flowConstraint, rhs(atPredicate, rule), cp.getConstraint_Predicate(), CREATE);
		createEdge(flowConstraint, flowGraphHomom, cp.getConstraint_Mappings(), CREATE);
		
		createEdge(flowConstraint, flow, cp.getConstraint_Arrows(), CREATE);
		createEdge(flowConstraint, rhs(source, rule), cp.getConstraint_Nodes(), CREATE);
		createEdge(flowConstraint, targetA, cp.getConstraint_Nodes(), CREATE);
		
		createEdge(flow, flowConstraint, cp.getArrow_Constraints(), CREATE);
		createEdge(rhs(source, rule), flowConstraint, cp.getNode_Constraints(), CREATE);
		createEdge(targetA, flowConstraint, cp.getNode_Constraints(), CREATE);
		
		createEdge(flowGraphHomom, flowArrow2ArrowMap, cp.getGraphHomomorphism_ArrowMapping(), CREATE);
		createEdge(flowArrow2ArrowMap, rhs(atPredicateFlow, rule), cp.getArrowToArrowMap_Key(), CREATE);
		createEdge(flowArrow2ArrowMap, flow, cp.getArrowToArrowMap_Value(), CREATE);
		
		createEdge(flowGraphHomom, flowNode2NodeMapSource, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(flowNode2NodeMapSource, rhs(atPredicateSource, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(flowNode2NodeMapSource, rhs(source, rule), cp.getNodeToNodeMap_Value(), CREATE);
		
		createEdge(flowGraphHomom, flowNode2NodeMapTarget, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(flowNode2NodeMapTarget, rhs(atPredicateTarget, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(flowNode2NodeMapTarget, targetA, cp.getNodeToNodeMap_Value(), CREATE);
		
		// Forbid flow a to already exist
		Node forbidFlowA = createNode(lhs, cp.getArrow(), FORBID);
		createAttr(forbidFlowA, cp.getArrow_Name(), createFlowName, FORBID);
		
		Graph forbidGraph = forbidFlowA.getGraph();
		
		// Forbid target 
		Node forbidTarget = createNode(lhs, cp.getNode(), FORBID);
		createAttr(forbidTarget, cp.getNode_Name(), forbidTargetName, FORBID);
		
		// Forbid source
		Node forbidSource = forbidTarget.getGraph().getNode("source");
		
		// Forbid flow
		Node _forbidFlow = createNode(lhs, cp.getArrow(), FORBID);
		createAttr(_forbidFlow, cp.getArrow_Name(), forbidFlowName, FORBID);
		createEdge(_forbidFlow, forbidSource, cp.getArrow_Source(), FORBID);
		createEdge(_forbidFlow, forbidTarget, cp.getArrow_Target(), FORBID);
		
		// Forbid flow B
//		Node flowBType = createNode(lhs, cp.getArrow(), PRESERVE, "FlowBType");
//		createAttr(flowBType, cp.getArrow_Name(), forbidFlowName, PRESERVE);
//		createEdge(typeGraph, flowBType, cp.getGraph_Arrows(), PRESERVE);
//		
//		Node forbidFlowB = createNode(lhs, cp.getArrow(), FORBID, "forbidFlowB");
//		createAttr(forbidFlowB, cp.getArrow_Name(), forbidFlowName, FORBID);
//		
//		Graph forbidhs = forbidFlowB.getGraph();
//		
//		Node forbidGraph = forbidhs.getNode(SpecificationElements.GRAPH.name);
//		Node forbidFlowBType = forbidhs.getNode("FlowBType");
//		createEdge(forbidGraph, forbidFlowB, cp.getGraph_Arrows(), FORBID);
//		createEdge(forbidFlowB, forbidFlowBType, cp.getArrow_TypeArrow(), FORBID);
//		
//		// Forbid flow a to already exist
//		Node forbidFlowA = createNode(forbidhs, cp.getArrow(), FORBID, "forbidFlowA");
//		createAttr(forbidFlowA, cp.getArrow_Name(), createFlowName, FORBID);
//		Node forbidFlowAType = forbidhs.getNode("flowAType");
//		createEdge(forbidGraph, forbidFlowA, cp.getGraph_Arrows(), FORBID);
//		createEdge(forbidFlowA, forbidFlowAType, cp.getArrow_TypeArrow(), FORBID);
//		
//		for(Node n : forbidhs.getNodes()) {
//			if(n.getAction() != null) {
//				if(n.getAction().getType() == FORBID) {
//					System.out.println(n.getOutgoing());
//				}
//			}
//		}
		
		
		// Forbid flow A to already exist
//		Node flowAType = createNode(lhs, cp.getArrow(), PRESERVE, "FlowAType");
//		createAttr(flowAType, cp.getArrow_Name(), createFlowName, PRESERVE);
//		createEdge(typeGraph, flowAType, cp.getGraph_Arrows(), PRESERVE);
		
//		Node forbidFlowA = createNode(lhs, cp.getArrow(), FORBID, createFlowName);
//				createAttr(forbidFlowA, cp.getArrow_Name(), createFlowName, FORBID);
		
//		Node forbidFlowAType = forbidFlowA.getGraph().getNode("FlowAType");
//		createEdge(forbidFlowA, forbidFlowAType, cp.getArrow_TypeArrow(), FORBID);
		
		// Forbid the new task to already exist
//		Node forbidTargetA = createNode(lhs, cp.getNode(), FORBID);
//		createAttr(forbidTargetA, cp.getNode_Name(), createTargetName, FORBID);
//		Node forbidTargetAType = forbidFlowB.getGraph().getNode("targetAType");
//		createEdge(forbidTargetA, forbidTargetAType, cp.getNode_TypeNode(), FORBID);
		
		Main.saveRule(rule);
		
		return rule;
	}
}
