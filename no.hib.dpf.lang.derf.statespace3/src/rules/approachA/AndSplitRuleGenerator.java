package rules.approachA;

import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FINISHED;
import static util.DERFConstants.PREDICATE_FLOW_TRUE;

import java.util.LinkedList;
import java.util.List;

import no.hib.dpf.core.Arrow;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

public class AndSplitRuleGenerator implements IFlowRule {

	private static CorePackage cp = CorePackage.eINSTANCE;

	@Override
	/**
	 * @param flows
	 * 		The two flows that are connected in the and_split
	 */
	public List<Rule> createRules(List<Arrow> flows) {
		List<Rule> rules = new LinkedList<Rule>();

		rules.add(createRule(flows.get(0), flows.get(1)));
		
		return rules;
	}

	private Rule createRule(Arrow flowA, Arrow flowB) {
		String flowAName = flowA.getName();
		String flowBName = flowB.getName();
		String sourceName = flowA.getSource().getName();
		String targetAName = flowA.getTarget().getName();
		String targetBName = flowB.getTarget().getName();

		Rule r = ApproachARuleUtil.createRule("[and_split]: " + flowAName + " & " + flowBName);

		Node source = ApproachARuleUtil.requireTask(r, sourceName, PREDICATE_FINISHED);
		Node targetA = ApproachARuleUtil.createTask(r, targetAName, PREDICATE_ENABLED);
		Node targetB = ApproachARuleUtil.createTask(r, targetBName, PREDICATE_ENABLED);
		Node _flowA = ApproachARuleUtil.createFlow(r, flowAName, source, targetA, PREDICATE_FLOW_TRUE);
		Node _flowB = ApproachARuleUtil.createFlow(r, flowBName, source, targetB, PREDICATE_FLOW_TRUE);

		return r;
	}

}
