package rules.approachA;

import java.util.LinkedList;
import java.util.List;

import no.hib.dpf.core.CoreFactory;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.RuleUtil;
import static org.eclipse.emf.henshin.model.Action.Type.*;
import static rules.RuleUtil.*;
import static util.DERFConstants.*;


public class EnableNodeRuleGenerator implements ITaskRule {
	
	private static CorePackage cp = CorePackage.eINSTANCE;

	@Override
	public List<Rule> createRules(List<no.hib.dpf.core.Node> tasks) {
		List<Rule> rules = new LinkedList<Rule>();
		
		for(no.hib.dpf.core.Node task : tasks) {
			rules.add(createRule(task));
		}
		
		return rules;
	}
	
	private Rule createRule(no.hib.dpf.core.Node task) {
		String taskName = task.getName();
		
		Rule r = RuleUtil.createRule("EnableNode: " + taskName);
		

		Graph lhs = r.getLhs();
		Graph rhs = r.getRhs();
		
		// Standard
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE);
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE);
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, "graph");
		Node typeGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);
		createEdge(graph, typeGraph, cp.getGraph_Type(), PRESERVE);
		
		// Predicate E
		Node ePredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(ePredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, PRESERVE);
		Node ePredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node ePredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, ePredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(ePredicate, ePredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(ePredicateGraph, ePredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// New task
		Node _task = createNode(rhs, cp.getNode(), CREATE);
		createAttr(_task, cp.getNode_Name(), taskName, CREATE);
		Node targetType = createNode(lhs, cp.getNode(), PRESERVE);
		createAttr(targetType, cp.getNode_Name(), taskName, PRESERVE);
		
		createEdge(rhs(graph, r), _task, cp.getGraph_Nodes(), CREATE);
		createEdge(typeGraph, targetType, cp.getGraph_Nodes(), PRESERVE);
		createEdge(_task, rhs(targetType, r), cp.getNode_TypeNode(), CREATE);
		
		Node targetConstraint = createNode(rhs, cp.getConstraint(), CREATE);
		Node targetGraphHomom = createNode(rhs, cp.getGraphHomomorphism(), CREATE);
		Node targetNode2NodeMap = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
		
		createEdge(rhs(specification, r), targetConstraint, cp.getSpecification_Constraints(), CREATE);
		createEdge(_task, targetConstraint, cp.getNode_Constraints(), CREATE);
		createEdge(targetConstraint, rhs(ePredicate, r), cp.getConstraint_Predicate(), CREATE);
		createEdge(targetConstraint, _task, cp.getConstraint_Nodes(), CREATE);
		createEdge(targetConstraint, targetGraphHomom, cp.getConstraint_Mappings(), CREATE);
		
		createEdge(targetGraphHomom, targetNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(targetNode2NodeMap, rhs(ePredicateNode, r), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(targetNode2NodeMap, _task, cp.getNodeToNodeMap_Value(), CREATE);
		
		// Forbid the new task to already exist
		Node forbidTask = createNode(lhs, cp.getNode(), FORBID);
		createAttr(forbidTask, cp.getNode_Name(), taskName, FORBID);
		Node forbidGraph = forbidTask.getGraph().getNode("graph");
		createEdge(forbidGraph, forbidTask, cp.getGraph_Nodes(), FORBID);
		
		return r;
	}

}
