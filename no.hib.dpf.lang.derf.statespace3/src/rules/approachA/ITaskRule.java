package rules.approachA;

import java.util.List;

import no.hib.dpf.core.Arrow;
import no.hib.dpf.core.Node;

import org.eclipse.emf.henshin.model.Rule;

/**
 * General interface for all task rules in Approach B
 * 
 * @author Endre Vestbø
 */
public interface ITaskRule {
	/**
	 * Generates a list of Henshin transformation rule.
	 * 
	 * @param arrows 
	 * 		The tasks to generate the rule for.
	 * @return
	 * 		The generated rule.
	 */
	List<Rule> createRules(List<Node> tasks);
}