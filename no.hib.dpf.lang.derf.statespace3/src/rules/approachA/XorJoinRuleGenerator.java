package rules.approachA;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.FORBID;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FINISHED;
import static util.DERFConstants.PREDICATE_FLOW_FALSE;
import static util.DERFConstants.PREDICATE_FLOW_TRUE;
import static util.DERFConstants.PREDICATE_RUNNING;

import java.util.LinkedList;
import java.util.List;

import no.hib.dpf.core.Arrow;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.RuleUtil;
import rules.approachA.ApproachARuleUtil.FlowPredicate;
import rules.approachA.ApproachARuleUtil.SpecificationElements;
import rules.approachA.ApproachARuleUtil.TaskPredicate;

public class XorJoinRuleGenerator implements IFlowRule {

	private static CorePackage cp = CorePackage.eINSTANCE;

	@Override
	/**
	 * @param flows
	 * 		The two flows that are connected in the and_split
	 */
	public List<Rule> createRules(List<Arrow> flows) {
		List<Rule> rules = new LinkedList<Rule>();

		rules.add(createRule(flows.get(0)));
		rules.add(createRule(flows.get(1)));
		
		return rules;
	}
	
	private Rule createRule(Arrow flow) {
		String sourceName = flow.getSource().getName();
		String targetName = flow.getTarget().getName();
		String flowName = flow.getName();

		Rule rule = ApproachARuleUtil.createRule("SimpleFlow: " + flowName);

		Node source = ApproachARuleUtil.requireTask(rule, sourceName, PREDICATE_FINISHED);
		Node target = ApproachARuleUtil.createTask(rule, targetName, PREDICATE_ENABLED);
		Node _flow = ApproachARuleUtil.createFlow(rule, flowName, source, target, PREDICATE_FLOW_TRUE);

		return rule;
	}

	private Rule createRule2(Arrow createFlow, Arrow forbidFlow) {
		String createFlowName = createFlow.getName();
		String requireSourceName = createFlow.getSource().getName();
		String targetName = createFlow.getTarget().getName();
		
		String forbidFlowName = forbidFlow.getName();
		String forbidSourceName = forbidFlow.getSource().getName();
		
		
		Rule rule = RuleUtil.createRule("[xor_join]: ");// + createFlowName + " NOT " + forbidFlowName);
		
		System.out.println(requireSourceName);
		System.out.println(targetName);
		System.out.println(createFlowName);
		
		Node source = ApproachARuleUtil.requireTask(rule, requireSourceName, PREDICATE_FINISHED);
		Node target = ApproachARuleUtil.createTask(rule, targetName, PREDICATE_ENABLED);
		Node flow = ApproachARuleUtil.createFlow(rule, createFlowName, source, target, PREDICATE_FLOW_TRUE);
		
//		Graph lhs = rule.getLhs();
		

//		Node typeGraph = lhs.getNode(ApproachARuleUtil.SpecificationElements.TYPEGRAPH.name());
//		Node forbidFlowType = createNode(lhs, cp.getArrow(), PRESERVE, "forbidFlowType");
//		createAttr(forbidFlowType, cp.getArrow_Name(), forbidFlowName, PRESERVE);
//		createEdge(typeGraph, forbidFlowType, cp.getGraph_Arrows(), PRESERVE);
		
		
//		Node _forbidFlow = createNode(lhs, cp.getArrow(), FORBID);
//		Graph forbidGraph = _forbidFlow.getGraph();
//		Node _forbidFlowType = _forbidFlow.getGraph().getNode("forbidFlowType");
//		
//		for(Node n : forbidGraph.getNodes()) {
//			System.out.println(n);
//		}
		
//		Graph lhs = rule.getLhs();
//		Graph rhs = rule.getRhs();
//		
//		// Core
//		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE);
//		Node signature = createNode(lhs, cp.getSignature(), PRESERVE);
//		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
//		
//		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, "graph");
//		Node typeGraph = createNode(lhs, cp.getGraph(), PRESERVE);
//		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);
//		createEdge(graph, typeGraph, cp.getGraph_Type(), PRESERVE);
//		
//		// Predicate E
//		Node ePredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
//		createAttr(ePredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, PRESERVE);
//		Node ePredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
//		Node ePredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
//		
//		createEdge(signature, ePredicate, cp.getSignature_Predicates(), PRESERVE);
//		createEdge(ePredicate, ePredicateGraph, cp.getPredicate_Shape(), PRESERVE);
//		createEdge(ePredicateGraph, ePredicateNode, cp.getGraph_Nodes(), PRESERVE);
//		
//		// Predicate F
//		Node fPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
//		createAttr(fPredicate, cp.getPredicate_Symbol(), PREDICATE_FINISHED, PRESERVE);
//		Node fPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
//		Node fPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
//		
//		createEdge(signature, fPredicate, cp.getSignature_Predicates(), PRESERVE);
//		createEdge(fPredicate, fPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
//		createEdge(fPredicateGraph, fPredicateNode, cp.getGraph_Nodes(), PRESERVE);
//		
//		// Predicate t nodes & arrow
//		Node atPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
//		createAttr(atPredicate, cp.getPredicate_Symbol(), PREDICATE_FLOW_TRUE, PRESERVE);
//		Node atPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
//		Node atPredicateArrow = createNode(lhs, cp.getArrow(), PRESERVE);
//		Node atPredicateSource = createNode(lhs, cp.getNode(), PRESERVE);
//		Node atPredicateTarget = createNode(lhs, cp.getNode(), PRESERVE);
//		
//		createEdge(signature, atPredicate, cp.getSignature_Predicates(), PRESERVE);
//		createEdge(atPredicate, atPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
//		
//		createEdge(atPredicateGraph, atPredicateArrow, cp.getGraph_Arrows(), PRESERVE);
//		createEdge(atPredicateGraph, atPredicateSource, cp.getGraph_Nodes(), PRESERVE);
//		createEdge(atPredicateGraph, atPredicateTarget, cp.getGraph_Nodes(), PRESERVE);
//
//		createEdge(atPredicateArrow, atPredicateSource, cp.getArrow_Source(), PRESERVE);
//		createEdge(atPredicateArrow, atPredicateTarget, cp.getArrow_Target(), PRESERVE);
//		
//		// Source
//		Node source = createNode(rhs, cp.getNode(), PRESERVE);
//		createAttr(source, cp.getNode_Name(), createSourceName, PRESERVE);
//		Node sourceType = createNode(lhs, cp.getNode(), PRESERVE);
//		createAttr(sourceType, cp.getNode_Name(), createSourceName, PRESERVE);
//		
//		createEdge(rhs(graph, rule), source, cp.getGraph_Nodes(), PRESERVE);
//		createEdge(typeGraph, sourceType, cp.getGraph_Nodes(), PRESERVE);
//		createEdge(source, rhs(sourceType, rule), cp.getNode_TypeNode(), PRESERVE);
//		
//		Node sourceConstraint = createNode(rhs, cp.getConstraint(), PRESERVE);
//		Node sourceGraphHomom = createNode(rhs, cp.getGraphHomomorphism(), PRESERVE);
//		Node sourceNode2NodeMap = createNode(rhs, cp.getNodeToNodeMap(), PRESERVE);
//		
//		createEdge(rhs(specification, rule), sourceConstraint, cp.getSpecification_Constraints(), PRESERVE);
//		createEdge(source, sourceConstraint, cp.getNode_Constraints(), PRESERVE);
//		createEdge(sourceConstraint, rhs(fPredicate, rule), cp.getConstraint_Predicate(), PRESERVE);
//		createEdge(sourceConstraint, source, cp.getConstraint_Nodes(), PRESERVE);
//		createEdge(sourceConstraint, sourceGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
//		
//		createEdge(sourceGraphHomom, sourceNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
//		createEdge(sourceNode2NodeMap, rhs(fPredicateNode, rule), cp.getNodeToNodeMap_Key(), PRESERVE);
//		createEdge(sourceNode2NodeMap, source, cp.getNodeToNodeMap_Value(), PRESERVE);
//		
//		// Target
//		Node target = createNode(rhs, cp.getNode(), CREATE, "target");
//		createAttr(target, cp.getNode_Name(), targetName, CREATE);
//		Node targetType = createNode(lhs, cp.getNode(), PRESERVE);
//		createAttr(targetType, cp.getNode_Name(), targetName, PRESERVE);
//		
//		createEdge(rhs(graph, rule), target, cp.getGraph_Nodes(), CREATE);
//		createEdge(typeGraph, targetType, cp.getGraph_Nodes(), PRESERVE);
//		createEdge(target, rhs(targetType, rule), cp.getNode_TypeNode(), CREATE);
//		
//		Node targetConstraint = createNode(rhs, cp.getConstraint(), CREATE);
//		Node targetGraphHomom = createNode(rhs, cp.getGraphHomomorphism(), CREATE);
//		Node targetNode2NodeMap = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
//		
//		createEdge(rhs(specification, rule), targetConstraint, cp.getSpecification_Constraints(), CREATE);
//		createEdge(target, targetConstraint, cp.getNode_Constraints(), CREATE);
//		createEdge(targetConstraint, rhs(ePredicate, rule), cp.getConstraint_Predicate(), CREATE);
//		createEdge(targetConstraint, target, cp.getConstraint_Nodes(), CREATE);
//		createEdge(targetConstraint, targetGraphHomom, cp.getConstraint_Mappings(), CREATE);
//		
//		createEdge(targetGraphHomom, targetNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), CREATE);
//		createEdge(targetNode2NodeMap, rhs(ePredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
//		createEdge(targetNode2NodeMap, target, cp.getNodeToNodeMap_Value(), CREATE);
//				
//		
//		// Create flow
//		Node _createFlow = createNode(lhs, cp.getArrow(), CREATE, createFlowName);
//		createAttr(_createFlow, cp.getNode_Name(), createFlowName, CREATE);
//		Node createFlowType = createNode(lhs, cp.getArrow(), PRESERVE);
//		createAttr(createFlowType, cp.getNode_Name(), createFlowName, PRESERVE);
//		
//		createEdge(rhs(graph, rule), _createFlow, cp.getGraph_Arrows(), CREATE);
//		createEdge(typeGraph, createFlowType, cp.getGraph_Arrows(), PRESERVE);
//		createEdge(_createFlow, rhs(createFlowType, rule), cp.getArrow_TypeArrow(), CREATE);
//		
//		createEdge(_createFlow, rhs(source, rule), cp.getArrow_Source(), CREATE);
//		createEdge(_createFlow, target, cp.getArrow_Target(), CREATE);
//		
//		Node createFlowConstraint = createNode(lhs, cp.getConstraint(), CREATE);
//		Node createFlowGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), CREATE);
//		Node createFlowArrow2ArrowMap = createNode(lhs, cp.getArrowToArrowMap(), CREATE);
//		Node createFlowNode2NodeMapSource = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
//		Node createFlowNode2NodeMapTarget = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
//		
//		createEdge(rhs(specification, rule), createFlowConstraint, cp.getSpecification_Constraints(), CREATE);
//		createEdge(createFlowConstraint, rhs(atPredicate, rule), cp.getConstraint_Predicate(), CREATE);
//		createEdge(createFlowConstraint, createFlowGraphHomom, cp.getConstraint_Mappings(), CREATE);
//		
//		createEdge(createFlowConstraint, _createFlow, cp.getConstraint_Arrows(), CREATE);
//		createEdge(createFlowConstraint, rhs(source, rule), cp.getConstraint_Nodes(), CREATE);
//		createEdge(createFlowConstraint, target, cp.getConstraint_Nodes(), CREATE);
//		
//		createEdge(_createFlow, createFlowConstraint, cp.getArrow_Constraints(), CREATE);
//		createEdge(rhs(source, rule), createFlowConstraint, cp.getNode_Constraints(), CREATE);
//		createEdge(target, createFlowConstraint, cp.getNode_Constraints(), CREATE);
//		
//		createEdge(createFlowGraphHomom, createFlowArrow2ArrowMap, cp.getGraphHomomorphism_ArrowMapping(), CREATE);
//		createEdge(createFlowArrow2ArrowMap, rhs(atPredicateArrow, rule), cp.getArrowToArrowMap_Key(), CREATE);
//		createEdge(createFlowArrow2ArrowMap, _createFlow, cp.getArrowToArrowMap_Value(), CREATE);
//		
//		createEdge(createFlowGraphHomom, createFlowNode2NodeMapSource, cp.getGraphHomomorphism_NodeMapping(), CREATE);
//		createEdge(createFlowNode2NodeMapSource, rhs(atPredicateSource, rule), cp.getNodeToNodeMap_Key(), CREATE);
//		createEdge(createFlowNode2NodeMapSource, rhs(source, rule), cp.getNodeToNodeMap_Value(), CREATE);
//		
//		createEdge(createFlowGraphHomom, createFlowNode2NodeMapTarget, cp.getGraphHomomorphism_NodeMapping(), CREATE);
//		createEdge(createFlowNode2NodeMapTarget, rhs(atPredicateTarget, rule), cp.getNodeToNodeMap_Key(), CREATE);
//		createEdge(createFlowNode2NodeMapTarget, target, cp.getNodeToNodeMap_Value(), CREATE);
//
//		// Forbid the new flow to already exist
////		Node forbidCreateFlow = createNode(lhs, cp.getArrow(), FORBID);
////		Node forbidGraph = forbidCreateFlow.getGraph().getNode("graph");
////		
////		createAttr(forbidCreateFlow, cp.getArrow_Name(), createFlowName, FORBID);
////		createEdge(forbidGraph, forbidCreateFlow, cp.getGraph_Arrows(), FORBID);
//		
//		// Forbid the forbid flow
//		Node _forbidFlow = createNode(lhs, cp.getArrow(), FORBID, "forbidFlow");
//		createAttr(_forbidFlow, cp.getArrow_Name(), forbidFlowName, FORBID);
////		Node forbidFlowType = createNode(lhs, cp.getArrow(), PRESERVE, "forbidFlowType");
////		createAttr(forbidFlowType, cp.getArrow_Name(), forbidFlowName, PRESERVE);
////		
//		Node forbidGraph = _forbidFlow.getGraph().getNode("graph");
//		forbidGraph.setName("forbidGraph");
////		Node forbidForbidFlowType = _forbidFlow.getGraph().getNode("forbidFlowType");
//		
//		createEdge(forbidGraph, _forbidFlow, cp.getGraph_Arrows(), FORBID);
////		createEdge(typeGraph, forbidFlowType, cp.getGraph_Arrows(), PRESERVE);
////		createEdge(_forbidFlow, forbidForbidFlowType, cp.getArrow_TypeArrow(), FORBID);
//		
//		// Forbid the new task to already exist
////		Node forbidTask = createNode(lhs, cp.getNode(), FORBID);
////		createAttr(forbidTask, cp.getNode_Name(), targetName, FORBID);;
////		createEdge(forbidGraph, forbidTask, cp.getGraph_Nodes(), FORBID);

		return rule;
	}

}
