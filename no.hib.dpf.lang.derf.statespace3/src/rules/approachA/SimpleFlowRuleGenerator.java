package rules.approachA;

import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FINISHED;
import static util.DERFConstants.PREDICATE_FLOW_TRUE;

import java.util.LinkedList;
import java.util.List;

import no.hib.dpf.core.Arrow;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

public class SimpleFlowRuleGenerator implements IFlowRule {

	private static CorePackage cp = CorePackage.eINSTANCE;

	@Override
	/**
	 * @param flows
	 * 		All flows that should get independent rules generated.
	 */
	public List<Rule> createRules(List<Arrow> flows) {
		List<Rule> rules = new LinkedList<Rule>();

		for (Arrow flow : flows) {
			rules.add(createRule(flow));
		}

		return rules;
	}

	private Rule createRule(Arrow flow) {
		String sourceName = flow.getSource().getName();
		String targetName = flow.getTarget().getName();
		String flowName = flow.getName();

		Rule rule = ApproachARuleUtil.createRule("SimpleFlow: " + flowName);

		Node source = ApproachARuleUtil.requireTask(rule, sourceName, PREDICATE_FINISHED);
		Node target = ApproachARuleUtil.createTask(rule, targetName, PREDICATE_ENABLED);
		Node _flow = ApproachARuleUtil.createFlow(rule, flowName, source, target, PREDICATE_FLOW_TRUE);

		return rule;
	}

}
