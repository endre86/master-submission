package rules.approachA;

import java.util.List;

import no.hib.dpf.core.Arrow;

import org.eclipse.emf.henshin.model.Rule;

/**
 * General interface for all flow rules in Approach B
 * 
 * @author Endre Vestbø
 */
public interface IFlowRule {
	/**
	 * Generates a list of Henshin transformation rule.
	 * 
	 * @param arrows 
	 * 		The arrows to generate the rule for.
	 * @return
	 * 		The generated rule.
	 */
	List<Rule> createRules(List<Arrow> flows);
}