package rules.approachA;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.FORBID;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FLOW_FALSE;
import static util.DERFConstants.PREDICATE_FLOW_TRUE;
import static util.DERFConstants.PREDICATE_RUNNING;
import static util.DERFConstants.PREDICATE_FINISHED;

import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.RuleUtil;
import util.DERFConstants;
import no.hib.dpf.core.CorePackage;



public class ApproachARuleUtil {
	
	enum SpecificationElements {
		SPECIFICATION("specification"),
		SIGNATURE("signature"),
		GRAPH("graph"),
		TYPEGRAPH("typeGraph");
		
		public final String name;
		
		SpecificationElements(String name) {
			this.name = name;
		}
	}
	
	enum TaskPredicate {
		DISABLED(DERFConstants.PREDICATE_DISABLED),
		ENABLED(DERFConstants.PREDICATE_ENABLED),
		RUNNING(DERFConstants.PREDICATE_RUNNING),
		FINISHED(DERFConstants.PREDICATE_FINISHED),
		NULL("NULL");
		
		public final String predicate;
		public final String predicateGraph;
		public final String predicateNode;
		
		TaskPredicate(String predicate) {
			this.predicate = predicate;
			this.predicateGraph = predicate + "Graph";
			this.predicateNode = predicate + "Node";
		}
		
		protected static TaskPredicate get(String predicate) {
			for(TaskPredicate taskPredicate : TaskPredicate.values()) {
				if(taskPredicate.predicate.equals(predicate)) {
					return taskPredicate;
				}
			}
			return NULL;
		}
	}
	
	enum FlowPredicate {
		FALSE(DERFConstants.PREDICATE_FLOW_FALSE),
		TRUE(DERFConstants.PREDICATE_FLOW_TRUE),
		NULL("NULL");
		
		public final String predicate;
		public final String predicateGraph;
		public final String predicateFlow;
		public final String predicateSource;
		public final String predicateTarget;
		
		FlowPredicate(String predicate) {
			this.predicate = predicate;
			this.predicateGraph = predicate + "Graph";
			this.predicateFlow = predicate + "Flow";
			this.predicateSource = predicate + "Source";
			this.predicateTarget = predicate + "Target";
		}
		
		protected static FlowPredicate get(String predicate) {
			for(FlowPredicate flowPredicate : FlowPredicate.values()) {
				if(flowPredicate.predicate.equals(predicate)) {
					return flowPredicate;
				}
			}
			return NULL;
		}
	}

	private static CorePackage cp = CorePackage.eINSTANCE;
	
	public static Rule createRule(String ruleName) {
		Rule rule = RuleUtil.createRule(ruleName);
		
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		// Core
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE, SpecificationElements.SPECIFICATION.name);
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE, SpecificationElements.SIGNATURE.name);
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, SpecificationElements.GRAPH.name);
		Node typeGraph = createNode(lhs, cp.getGraph(), PRESERVE, SpecificationElements.TYPEGRAPH.name);
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);
		createEdge(graph, typeGraph, cp.getGraph_Type(), PRESERVE);
		
		// Predicate D
		Node dPredicate = createNode(lhs, cp.getPredicate(), PRESERVE, TaskPredicate.DISABLED.predicate);
		createAttr(dPredicate, cp.getPredicate_Symbol(), PREDICATE_DISABLED, PRESERVE);
		Node dPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE, TaskPredicate.DISABLED.predicateGraph);
		Node dPredicateNode = createNode(lhs, cp.getNode(), PRESERVE, TaskPredicate.DISABLED.predicateNode);
		
		createEdge(signature, dPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(dPredicate, dPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(dPredicateGraph, dPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate E
		Node ePredicate = createNode(lhs, cp.getPredicate(), PRESERVE, TaskPredicate.ENABLED.predicate);
		createAttr(ePredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, PRESERVE);
		Node ePredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE, TaskPredicate.ENABLED.predicateGraph);
		Node ePredicateNode = createNode(lhs, cp.getNode(), PRESERVE, TaskPredicate.ENABLED.predicateNode);
		
		createEdge(signature, ePredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(ePredicate, ePredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(ePredicateGraph, ePredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate R
		Node rPredicate = createNode(lhs, cp.getPredicate(), PRESERVE, TaskPredicate.RUNNING.predicate);
		createAttr(rPredicate, cp.getPredicate_Symbol(), PREDICATE_RUNNING, PRESERVE);
		Node rPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE, TaskPredicate.RUNNING.predicateGraph);
		Node rPredicateNode = createNode(lhs, cp.getNode(), PRESERVE, TaskPredicate.RUNNING.predicateNode);
		
		createEdge(signature, rPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(rPredicate, rPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(rPredicateGraph, rPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate F
		Node fPredicate = createNode(lhs, cp.getPredicate(), PRESERVE, TaskPredicate.FINISHED.predicate);
		createAttr(fPredicate, cp.getPredicate_Symbol(), PREDICATE_FINISHED, PRESERVE);
		Node fPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE, TaskPredicate.FINISHED.predicateGraph);
		Node fPredicateNode = createNode(lhs, cp.getNode(), PRESERVE, TaskPredicate.FINISHED.predicateNode);
		
		createEdge(signature, fPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(fPredicate, fPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(fPredicateGraph, fPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate f nodes & arrow
		Node afPredicate = createNode(lhs, cp.getPredicate(), PRESERVE, FlowPredicate.FALSE.predicate);
		createAttr(afPredicate, cp.getPredicate_Symbol(), PREDICATE_FLOW_FALSE, PRESERVE);
		Node afPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE, FlowPredicate.FALSE.predicateGraph);
		Node afPredicateFlow = createNode(lhs, cp.getArrow(), PRESERVE, FlowPredicate.FALSE.predicateFlow);
		Node afPredicateSource = createNode(lhs, cp.getNode(), PRESERVE, FlowPredicate.FALSE.predicateSource);
		Node afPredicateTarget = createNode(lhs, cp.getNode(), PRESERVE, FlowPredicate.FALSE.predicateTarget);
		
		createEdge(signature, afPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(afPredicate, afPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		
		createEdge(afPredicateGraph, afPredicateFlow, cp.getGraph_Arrows(), PRESERVE);
		createEdge(afPredicateGraph, afPredicateSource, cp.getGraph_Nodes(), PRESERVE);
		createEdge(afPredicateGraph, afPredicateTarget, cp.getGraph_Nodes(), PRESERVE);

		createEdge(afPredicateFlow, afPredicateSource, cp.getArrow_Source(), PRESERVE);
		createEdge(afPredicateFlow, afPredicateTarget, cp.getArrow_Target(), PRESERVE);
		

		// Predicate t nodes & arrow
		Node atPredicate = createNode(lhs, cp.getPredicate(), PRESERVE, FlowPredicate.TRUE.predicate);
		createAttr(atPredicate, cp.getPredicate_Symbol(), PREDICATE_FLOW_TRUE, PRESERVE);
		Node atPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE, FlowPredicate.TRUE.predicateGraph);
		Node atPredicateFlow = createNode(lhs, cp.getArrow(), PRESERVE, FlowPredicate.TRUE.predicateFlow);
		Node atPredicateSource = createNode(lhs, cp.getNode(), PRESERVE, FlowPredicate.TRUE.predicateSource);
		Node atPredicateTarget = createNode(lhs, cp.getNode(), PRESERVE, FlowPredicate.TRUE.predicateTarget);
		
		createEdge(signature, atPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(atPredicate, atPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		
		createEdge(atPredicateGraph, atPredicateFlow, cp.getGraph_Arrows(), PRESERVE);
		createEdge(atPredicateGraph, atPredicateSource, cp.getGraph_Nodes(), PRESERVE);
		createEdge(atPredicateGraph, atPredicateTarget, cp.getGraph_Nodes(), PRESERVE);

		createEdge(atPredicateFlow, atPredicateSource, cp.getArrow_Source(), PRESERVE);
		createEdge(atPredicateFlow, atPredicateTarget, cp.getArrow_Target(), PRESERVE);
		
		return rule;
	}
	
	public static Node requireTask(Rule rule, String taskName, String predicateName) {
		Graph lhs = rule.getLhs();
		
		Node specification = lhs.getNode(SpecificationElements.SPECIFICATION.name);
		Node graph = lhs.getNode(SpecificationElements.GRAPH.name); 
		Node typeGraph = lhs.getNode(SpecificationElements.TYPEGRAPH.name);
		
		Node predicate = lhs.getNode(
				TaskPredicate.get(predicateName).predicate);
		Node predicateNode = lhs.getNode(
				TaskPredicate.get(predicateName).predicateNode);
		
		Node source = createNode(lhs, cp.getNode(), PRESERVE, taskName);
		createAttr(source, cp.getNode_Name(), taskName, PRESERVE);
		Node sourceType = createNode(lhs, cp.getNode(), PRESERVE);
		createAttr(sourceType, cp.getNode_Name(), taskName, PRESERVE);
		
		createEdge(graph, source, cp.getGraph_Nodes(), PRESERVE);
		createEdge(typeGraph, sourceType, cp.getGraph_Nodes(), PRESERVE);
		createEdge(source, sourceType, cp.getNode_TypeNode(), CREATE);
		
		Node sourceConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node sourceGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node sourceNode2NodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, sourceConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(source, sourceConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(sourceConstraint, predicate, cp.getConstraint_Predicate(), PRESERVE);
		createEdge(sourceConstraint, source, cp.getConstraint_Nodes(), PRESERVE);
		createEdge(sourceConstraint, sourceGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		
		createEdge(sourceGraphHomom, sourceNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(sourceNode2NodeMap, predicateNode, cp.getNodeToNodeMap_Key(), PRESERVE);
		createEdge(sourceNode2NodeMap, source, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		return source;
	}
	
	public static Node createTask(Rule rule, String taskName, String predicateName) {
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		Node specification = lhs.getNode(SpecificationElements.SPECIFICATION.name);
		Node graph = lhs.getNode(SpecificationElements.GRAPH.name); 
		Node typeGraph = lhs.getNode(SpecificationElements.TYPEGRAPH.name);
		
		Node predicate = lhs.getNode(
				TaskPredicate.get(predicateName).predicate);
		Node predicateNode = lhs.getNode(
				TaskPredicate.get(predicateName).predicateNode);
		
		Node task = createNode(rhs, cp.getNode(), CREATE, taskName);
		createAttr(task, cp.getNode_Name(), taskName, CREATE);
		Node taskType = createNode(lhs, cp.getNode(), PRESERVE);
		createAttr(taskType, cp.getNode_Name(), taskName, PRESERVE);
		
		createEdge(rhs(graph, rule), task, cp.getGraph_Nodes(), CREATE);
		createEdge(typeGraph, taskType, cp.getGraph_Nodes(), PRESERVE);
		createEdge(task, rhs(taskType, rule), cp.getNode_TypeNode(), CREATE);
		
		Node constraint = createNode(rhs, cp.getConstraint(), CREATE);
		Node graphHomom = createNode(rhs, cp.getGraphHomomorphism(), CREATE);
		Node node2NodeMap = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
		
		createEdge(rhs(specification, rule), constraint, cp.getSpecification_Constraints(), CREATE);
		createEdge(task, constraint, cp.getNode_Constraints(), CREATE);
		createEdge(constraint, rhs(predicate, rule), cp.getConstraint_Predicate(), CREATE);
		createEdge(constraint, task, cp.getConstraint_Nodes(), CREATE);
		createEdge(constraint, graphHomom, cp.getConstraint_Mappings(), CREATE);
		
		createEdge(graphHomom, node2NodeMap, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(node2NodeMap, rhs(predicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(node2NodeMap, task, cp.getNodeToNodeMap_Value(), CREATE);
		
		// Forbid the new task to already exist
		Node forbidTask= createNode(lhs, cp.getNode(), FORBID);
		createAttr(forbidTask, cp.getNode_Name(), taskName, FORBID);
		Node forbidGraph = forbidTask.getGraph().getNode(SpecificationElements.GRAPH.name);
		createEdge(forbidGraph, forbidTask, cp.getGraph_Nodes(), FORBID);
				
		return task;
	}
	
	public static Node forbidFlow(Rule rule, String flowName, String sourceName, String targetName) {
		Graph lhs = rule.getLhs();
		
		Node forbidFlow = createNode(lhs, cp.getArrow(), FORBID);
		createAttr(forbidFlow, cp.getArrow_Name(), flowName, FORBID);
		
		Node forbidGraph = forbidFlow.getGraph().getNode(SpecificationElements.GRAPH.name);
		createEdge(forbidGraph, forbidFlow, cp.getGraph_Arrows(), FORBID);
		
		return forbidFlow;
	}
	
	public static Node createFlow(Rule rule, String flowName, Node source, Node target, String predicateName) {
		
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		Node specification = lhs.getNode(SpecificationElements.SPECIFICATION.name);
		Node graph = lhs.getNode(SpecificationElements.GRAPH.name); 
		Node typeGraph = lhs.getNode(SpecificationElements.TYPEGRAPH.name);
		
		Node predicate = lhs.getNode(
				FlowPredicate.get(predicateName).predicate);
		Node predicateFlow = lhs.getNode(
				FlowPredicate.get(predicateName).predicateFlow);
		Node predicateSource = lhs.getNode(
				FlowPredicate.get(predicateName).predicateSource);
		Node predicateTarget = lhs.getNode(
				FlowPredicate.get(predicateName).predicateTarget);
		
		Node flow = createNode(lhs, cp.getArrow(), CREATE, flowName);
		createAttr(flow, cp.getNode_Name(), flowName, CREATE);
		Node flowType = createNode(lhs, cp.getArrow(), PRESERVE);
		createAttr(flowType, cp.getNode_Name(), flowName, PRESERVE);
		
		createEdge(rhs(graph, rule), flow, cp.getGraph_Arrows(), CREATE);
		createEdge(typeGraph, flowType, cp.getGraph_Arrows(), PRESERVE);
		createEdge(flow, rhs(flowType, rule), cp.getArrow_TypeArrow(), CREATE);
		
		createEdge(flow, rhs(source, rule), cp.getArrow_Source(), CREATE);
		createEdge(flow, target, cp.getArrow_Target(), CREATE);
		
		Node flowConstraint = createNode(lhs, cp.getConstraint(), CREATE);
		Node flowGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), CREATE);
		Node flowArrow2ArrowMap = createNode(lhs, cp.getArrowToArrowMap(), CREATE);
		Node flowNode2NodeMapSource = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
		Node flowNode2NodeMapTarget = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
		
		createEdge(rhs(specification, rule), flowConstraint, cp.getSpecification_Constraints(), CREATE);
		createEdge(flowConstraint, rhs(predicate, rule), cp.getConstraint_Predicate(), CREATE);
		createEdge(flowConstraint, flowGraphHomom, cp.getConstraint_Mappings(), CREATE);
		
		createEdge(flowConstraint, flow, cp.getConstraint_Arrows(), CREATE);
		createEdge(flowConstraint, rhs(source, rule), cp.getConstraint_Nodes(), CREATE);
		createEdge(flowConstraint, target, cp.getConstraint_Nodes(), CREATE);
		
		createEdge(flow, flowConstraint, cp.getArrow_Constraints(), CREATE);
		createEdge(rhs(source, rule), flowConstraint, cp.getNode_Constraints(), CREATE);
		createEdge(target, flowConstraint, cp.getNode_Constraints(), CREATE);
		
		createEdge(flowGraphHomom, flowArrow2ArrowMap, cp.getGraphHomomorphism_ArrowMapping(), CREATE);
		createEdge(flowArrow2ArrowMap, rhs(predicateFlow, rule), cp.getArrowToArrowMap_Key(), CREATE);
		createEdge(flowArrow2ArrowMap, flow, cp.getArrowToArrowMap_Value(), CREATE);
		
		createEdge(flowGraphHomom, flowNode2NodeMapSource, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(flowNode2NodeMapSource, rhs(predicateSource, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(flowNode2NodeMapSource, rhs(source, rule), cp.getNodeToNodeMap_Value(), CREATE);
		
		createEdge(flowGraphHomom, flowNode2NodeMapTarget, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(flowNode2NodeMapTarget, rhs(predicateTarget, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(flowNode2NodeMapTarget, target, cp.getNodeToNodeMap_Value(), CREATE);

		// Forbid the new flow to already exist
//		Node forbidFlow = createNode(lhs, cp.getArrow(), FORBID);
//		createAttr(forbidFlow, cp.getArrow_Name(), flowName, FORBID);
//		Node forbidGraph = forbidFlow.getGraph().getNode(SpecificationElements.GRAPH.name);
//		createEdge(forbidGraph, forbidFlow, cp.getGraph_Arrows(), FORBID);
		
		return flow;
	}

}
