package util;

public class FILOQueue<T>  {
	
	class Element<T> {
		T data;
		Element<T> tailing = null;
		
		public Element(T e) {
			this.data = e;
		}
	}
	
	private Element<T> head = null;
	private Element<T> tail = null;
	private int size = 0;
	
	public void queue(T e) {
		size++;
		
		Element<T> newElem = new Element<T>(e);
		if(head == null) {
			head = newElem;
			tail = newElem;
			return;
		}
		
		tail.tailing = newElem;
		tail = newElem;
	}
	
	public T dequeue() {
		size--;
		
		Element<T> oldHead = head;
		
		if(size == 0) {
			tail = null;
			head = null;
		}
		else {
			head = oldHead.tailing;
		}

		return oldHead.data;
	}
	
	public int size() {
		return size;
	}
}