package util;

public class DERFConstants {
	public static final String FLOW = "Flow";
	public static final String TASK = "Task";
	
	public static final String PREDICATE_DISABLED = "D";
	public static final String PREDICATE_ENABLED = "E";
	public static final String PREDICATE_RUNNING = "R";
	public static final String PREDICATE_FINISHED = "F";
	
	public static final String PREDICATE_FLOW_TRUE = "t";
	public static final String PREDICATE_FLOW_FALSE = "f";
	
	public static final String PREDICATE_AND_SPLIT = "and_split";
	public static final String PREDICATE_AND_JOIN = "and_join";
	public static final String PREDICATE_XOR_SPLIT = "xor_split";
	public static final String PREDICATE_XOR_JOIN = "xor_join";
	public static final String PREDICATE_NODEMULT = "nodemult";
}
