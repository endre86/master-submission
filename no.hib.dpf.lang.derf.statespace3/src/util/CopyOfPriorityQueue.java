package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import statespace.sweepline.SweeplineState;

public class CopyOfPriorityQueue<T extends Comparable<T>> {
	class Element implements Comparable<Element>{
		T data;
		Element tailing = null;
		
		public Element(T e) {
			this.data = e;
		}

		@Override
		public int compareTo(CopyOfPriorityQueue<T>.Element o) {
			return data.compareTo(o.data);
		}
	}
	
	private Element head = null;
	private int size = 0;
	
	public void add(T e) {
		Element newElement = new Element(e);
		size++;
		
		if(head == null) {
			head = newElement;
			return;
		}
		
		if(head.compareTo(newElement) > 0) {
			newElement.tailing = head;
			head = newElement;
			return;
		}
		
		if(head.tailing == null) {
			head.tailing = newElement;
		}
		
		Element current = head;
		
		int counter = 0;
		while(current.tailing != null) {
			if(current.tailing.compareTo(newElement) > 0) {
				newElement.tailing = current.tailing;
				current.tailing = newElement;
				return;
			}
			
			current = current.tailing;
		}
		
		current.tailing = newElement;
		
		
	}
	
	public T getMin() {
		if(head == null) {
			DERFUtil.debug("getMin from returned null! And size is " + size);
			return null;
		}

		return head.data;
	}
	
	public T getMinElement() {
		if(head == null) {
			return null;
		}
		
		size--;
		
		T result = head.data;
		head = head.tailing;
		return result;
	}
	
	public void removeMin() {
		if(size == 0) {
			return;
		}
		
		size--;
		
		if(head.tailing == null) {
			head = null;
			return;
		}
		
		head = head.tailing;
	}
	
	public int size() {
		return size;
	}

	public List<T> getAll() {
		List<T> allElements = new ArrayList<T>();
		
		Element current = head;
		
		while(current != null) {
			allElements.add(current.data);
			current = current.tailing;
		}
		
		return allElements;
	}
	
	public static void main(String[] args) {
		CopyOfPriorityQueue<String> pq = new CopyOfPriorityQueue<String>();
		pq.add("d");
		pq.add("d");
		pq.add("a");
		
		while(pq.size() > 0) {
			System.out.println(pq.getMin());
			pq.removeMin();
		}
	}
}
