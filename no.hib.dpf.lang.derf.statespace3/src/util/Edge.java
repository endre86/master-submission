package util;

public class Edge<T, E> {
	public final T source;
	public final E target;
	
	public Edge(T source, E target) {
		this.source = source;
		this.target = target;
	}
	
	public boolean equals(Object o) {
		if(!(o instanceof Edge<?,?>)) {
			return false;
		}
		
		Edge that = (Edge) o;
		
		return this.source.equals(that.source) && this.target.equals(that.target);
	}
}
