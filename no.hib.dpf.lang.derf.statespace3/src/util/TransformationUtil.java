package util;

import java.util.LinkedList;
import java.util.List;

import main.Main;
import no.hib.dpf.core.Specification;

import org.eclipse.emf.henshin.interpreter.ApplicationMonitor;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.InterpreterFactory;
import org.eclipse.emf.henshin.interpreter.Match;
import org.eclipse.emf.henshin.interpreter.RuleApplication;
import org.eclipse.emf.henshin.interpreter.UnitApplication;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.interpreter.impl.EngineImpl;
import org.eclipse.emf.henshin.interpreter.impl.InterpreterFactoryImpl;
import org.eclipse.emf.henshin.interpreter.impl.MatchImpl;
import org.eclipse.emf.henshin.interpreter.impl.RuleApplicationImpl;
import org.eclipse.emf.henshin.interpreter.impl.UnitApplicationImpl;
import org.eclipse.emf.henshin.interpreter.util.InterpreterUtil;
import org.eclipse.emf.henshin.model.Formula;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.HenshinFactory;
import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;
import org.eclipse.emf.henshin.model.Unit;

public class TransformationUtil {
	
	private static Engine engine = new EngineImpl();
	
	public static List<Specification> transformAllMatches(final Specification specification, Rule rule) {
		List<Specification> result = new LinkedList<Specification>();	
		
		try {
			Module module = HenshinFactory.eINSTANCE.createModule();
			module.getUnits().add(rule);
			
			List<Match> matches = InterpreterUtil.findAllMatches(engine, module, new EGraphImpl(specification));
			
//			DERFUtil.debug(rule.getName() + " gave " + matches.size() + " matches.");
			
			// TODO: Midlertidlig mens tester approach A
//			Specification specCopy = DERFUtil.copyDERFInterpretation(specification);
//			EGraph specGraph = new EGraphImpl(specCopy);
//			UnitApplication ua = new UnitApplicationImpl(engine, specGraph, module.getUnits().get(0), null);
//			InterpreterUtil.executeOrDie(ua);
//			result.add(specCopy);
			
			
			for(int i = 0; i < matches.size(); i++) {
				if(matches.get(i).isComplete()){ // && matches.get(i).isValid()) {
					Specification specCopy = DERFUtil.copySpecification(specification);
					EGraph specEGraph = new EGraphImpl(specCopy);
					List<Match> currentMatches = InterpreterUtil.findAllMatches(engine, module, specEGraph);
					RuleApplication ruleApp = new RuleApplicationImpl(engine, specEGraph, rule, null);
					ruleApp.setCompleteMatch(currentMatches.get(i));
					if(!ruleApp.execute(null)) {
						DERFUtil.debug("Failed transformation:\n\t" + rule.getName() +
							"\nmatch:\n\t" + currentMatches.get(i).toString() +
							"\nspecification:\n\t" + DERFUtil.specificationToString(specCopy));
					}
					result.add(specCopy);
				}
			}			
		} catch (Error ae) {
			DERFUtil.debug(ae.toString());
		} catch (Exception e) {
			System.err.println("Transformationed failed with an exception!");
			System.err.println(e);
			System.err.println(e.getStackTrace());
			System.exit(-1);
		}
		
		return result;
	}
}
