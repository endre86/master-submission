package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class PriorityQueue<T extends Comparable<T>> {
	class Element {
		T data;
		Element tailing = null;
		
		public Element(T e) {
			this.data = e;
		}
	}
	
	private Element head = null;
	private Element tail = null;
	private int size = 0;
	
	public void add(T e) {
		size++;
		
		Element newElem = new Element(e);
		if(head == null) {
			head = newElem;
			tail = newElem;
			return;
		}
		
		if(head.data.compareTo(e) > 0) {
			newElem.tailing = head;
			head = newElem;
			return;
		}
		
		Element current = head.tailing;
		Element prev = head;
		
		while(current != null) {
			if(current.data.compareTo(e) > 0) {
				newElem.tailing = current;
				prev.tailing = newElem;
				return;
			}
			current = current.tailing;
		}
		
		tail.tailing = newElem;
		tail = newElem;
	}
	
	public T getMin() {
		if(head == null) {
			return null;
		}
		
		return head.data;
	}
	
	public void removeMin() {
		if(size == 0) {
			return;
		}
		
		size--;
		
		if(head == tail) {
			tail = null;
			head = null;
			return;
		}
		
		head = head.tailing;
	}
	
	public int size() {
		return size;
	}

	public List<T> getAll() {
		List<T> allElements = new ArrayList<T>();
		
		Element current = head;
		
		while(current != null) {
			allElements.add(current.data);
			current = current.tailing;
		}
		
		return allElements;
	}
}