package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import statespace.sweepline.SweeplineState;

public class PriorityQueue2 {
	class Element {
		SweeplineState data;
		Element tailing = null;
		
		public Element(SweeplineState e) {
			this.data = e;
		}
		
		public int getPG() {
			return data.getProgressValue();
		}
	}
	
	private Element head = null;
	private int size = 0;
	
	public void add(SweeplineState e) {
		Element newElement = new Element(e);
		size++;
		
		if(head == null) {
			head = newElement;
			return;
		}
		
		if(head.getPG() > newElement.getPG()) {
			newElement.tailing = head;
			head = newElement;
			return;
		}
		
		if(head.tailing == null) {
			head.tailing = newElement;
			return;
		}
		
		Element current = head;
		
		int counter = 1;
		while(current.tailing != null) {
			if(current.tailing.getPG() > newElement.getPG()) {
				newElement.tailing = current.tailing;
				current.tailing = newElement;
				return;
			}
			
			current = current.tailing;
		}
		
		current.tailing = newElement;
	}
	
	public SweeplineState getMin() {
		if(head == null) {
			DERFUtil.debug("getMin from returned null! And size is " + size);
			return null;
		}

		return head.data;
	}
	
	public SweeplineState getMinElement() {
		if(head == null) {
			return null;
		}
		
		size--;
		
		SweeplineState result = head.data;
		head = head.tailing;
		return result;
	}
	
	public void removeMin() {
		if(size == 0) {
			return;
		}
		
		size--;
		
		head = head.tailing;
	}
	
	public int size() {
		return size;
	}
	
	public List<SweeplineState> getAll() {
		List<SweeplineState> all = new LinkedList<SweeplineState>();
		
		Element current = head;
		while(current != null) {
			all.add(current.data);
			current = current.tailing;
		}
		
		return all;
		
	}

}
