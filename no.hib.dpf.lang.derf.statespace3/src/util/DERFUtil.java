package util;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import no.hib.dpf.core.Arrow;
import no.hib.dpf.core.Constraint;
import no.hib.dpf.core.CoreFactory;
import no.hib.dpf.core.Graph;
import no.hib.dpf.core.GraphHomomorphism;
import no.hib.dpf.core.Node;
import no.hib.dpf.core.Predicate;
import no.hib.dpf.core.Signature;
import no.hib.dpf.core.Specification;
import no.hib.dpf.core.impl.ArrowImpl;
import no.hib.dpf.utils.DPFCoreUtil;

import org.eclipse.emf.common.util.URI;

public class DERFUtil {
	private static void addFlowConstraint(Arrow flow, Specification flowsSpecification, Predicate predicate) {
		Constraint newConstraint = coreFactory.createConstraint();
		Arrow predicateArrow = predicate.getShape().getArrows().get(0);

		GraphHomomorphism graphHomom = coreFactory.createGraphHomomorphism();
		graphHomom.getArrowMapping().put(predicateArrow, flow);
		graphHomom.getNodeMapping().put(predicateArrow.getSource(), flow.getSource());
		graphHomom.getNodeMapping().put(predicateArrow.getTarget(), flow.getTarget());

		newConstraint.setMappings(graphHomom);
		newConstraint.setPredicate(predicate);

		flow.getConstraints().add(newConstraint);
		flow.getSource().getConstraints().add(newConstraint);
		flow.getTarget().getConstraints().add(newConstraint);
		flowsSpecification.getConstraints().add(newConstraint);
	}
	private static void addTaskConstraint(Node task, Specification tasksSpecification, Predicate predicate) {
		Constraint newConstraint = coreFactory.createConstraint();
		Node predicateNode = predicate.getShape().getNodes().get(0);

		GraphHomomorphism graphHomom = coreFactory.createGraphHomomorphism();
		graphHomom.getNodeMapping().put(predicateNode, task);

		newConstraint.setMappings(graphHomom);
		newConstraint.setPredicate(predicate);

		task.getConstraints().add(newConstraint);
		tasksSpecification.getConstraints().add(newConstraint);
	}
	public static Specification copySpecification(Specification derfInterpretation) {
		Specification derfInterpretationCopy = createEmptySpecification(derfInterpretation.getType(),
				derfInterpretation.getSignature());

		Graph graph = derfInterpretationCopy.getGraph();
		Graph typeGraph = derfInterpretationCopy.getGraph().getType();

		for (Node task : derfInterpretation.getGraph().getNodes()) {
			Node taskCopy = coreFactory.createConstantNode();

			taskCopy.setName(task.getName());
			taskCopy.setTypeNode(typeGraph.getNodeByName(task.getTypeName()));
			graph.addNode(taskCopy);

			for (Constraint c : task.getConstraints()) {
				if (c.getArrows().size() == 0 && c.getPredicate().getShape().getNodes().size() == 1) {
					Predicate predicate = derfInterpretationCopy.getSignature().getPredicateBySymbol(
							c.getPredicate().getSymbol());

					addTaskConstraint(taskCopy, derfInterpretationCopy, predicate);
				}
			}
		}

		for (Arrow flow : derfInterpretation.getGraph().getArrows()) {
			Arrow flowCopy = coreFactory.createConstantArrow();

			flowCopy.setName(flow.getName());
			flowCopy.setTypeArrow(typeGraph.getArrowByName(flow.getTypeName()));

			flowCopy.setSource(graph.getNodeByName(flow.getSource().getName()));
			flowCopy.setTarget(graph.getNodeByName(flow.getTarget().getName()));

			graph.addArrow(flowCopy);

			for (Constraint c : flow.getConstraints()) {
				Predicate predicate = derfInterpretationCopy.getSignature().getPredicateBySymbol(
						c.getPredicate().getSymbol());
				addFlowConstraint(flowCopy, derfInterpretationCopy, predicate);
			}
		}

		return derfInterpretationCopy;
	}

	public static Specification createDERFInterpretation(Specification derfImplementation) {
		Signature derfInterpretationSignature = getDERFInterpretationSignature();

		Specification derfInterpretation = createEmptySpecification(derfImplementation.getType(),
				derfInterpretationSignature); // TODO: Bug?
												// derfImplementation.getType()

		derfInterpretation.setType(derfImplementation);
		derfInterpretation.getGraph().setType(derfImplementation.getGraph());

		Graph resultGraph = derfInterpretation.getGraph();
		Graph typeGraph = derfInterpretation.getType().getGraph();

		Predicate disabledPredicate = derfInterpretationSignature
				.getPredicateBySymbol(DERFConstants.PREDICATE_DISABLED);

		for (Node task : derfImplementation.getGraph().getNodes()) {
			Node newTask = coreFactory.createConstantNode();

			newTask.setName(task.getName());
			newTask.setTypeNode(typeGraph.getNodeByName(task.getName()));
			resultGraph.addNode(newTask);

			addTaskConstraint(newTask, derfInterpretation, disabledPredicate);
		}

		Predicate falsePredicate = derfInterpretationSignature.getPredicateBySymbol(DERFConstants.PREDICATE_FLOW_FALSE);

		for (Arrow flow : derfImplementation.getGraph().getArrows()) {
			Arrow newFlow = coreFactory.createArrow();
			newFlow.setName(flow.getName());
			newFlow.setSource(resultGraph.getNodeByName(flow.getSource().getName()));
			newFlow.setTarget(resultGraph.getNodeByName(flow.getTarget().getName()));
			newFlow.setTypeArrow(typeGraph.getArrowByName(flow.getName(), flow.getSource().getName(), flow.getTarget()
					.getName()));

			resultGraph.addArrow(newFlow);

			addFlowConstraint(newFlow, derfInterpretation, falsePredicate);
		}

		 expandNodemultLoops(derfInterpretation);

		return derfInterpretation;
	}

	public static Specification createEmptySpecification(final Specification typeSpecification,
			final Signature signature) {
		Specification result = CoreFactory.eINSTANCE.createDefaultSpecification();

		result.getGraph().getNodes().clear();
		result.getGraph().getArrows().clear();

		result.setType(typeSpecification);
		result.setGraph(CoreFactory.eINSTANCE.createGraph());
		result.getGraph().setType(typeSpecification.getGraph());
		result.setSignature(signature);

		return result;
	}

	public static void debug(String s) {
		if (debug) {
			System.out.println(s);
		}
	}

	public static void debug(String s, int lvl) {
		if (debug) {
			String tabs = "";
			for (int i = 0; i < lvl; i++) {
				tabs += "\t";
			}

			System.out.println(tabs + s);
		}
	}

	private static void expandNodeMultLoop(Specification derfInterpretation, Node multTask) {
		LinkedHashSet<Arrow> loop = findLoop(multTask);

		if (loop.size() <= 0) {
			return;
		}

		int multiplier = 2; // TODO: Get multiplier from constraint

		// Keep track of flows and the order they are in the loop (a -> b -> a
		// -> b)
		LinkedList<Arrow> loopFlowsOrdered = new LinkedList<Arrow>();
		loopFlowsOrdered.addAll(loop);

		// Frequently used objects
		CoreFactory coreFactory = CoreFactory.eINSTANCE;
		Graph graph = derfInterpretation.getGraph();
		Predicate falseFlowPredicate = derfInterpretation.getSignature().getPredicateBySymbol(
				DERFConstants.PREDICATE_FLOW_FALSE);
		Predicate disabledTaskPredicate = derfInterpretation.getSignature().getPredicateBySymbol(
				DERFConstants.PREDICATE_DISABLED);

		// Find the last flow in the unexpanded loop
		Arrow endLoopFlow = null;
		for (Arrow flow : loop) {
			endLoopFlow = flow;
		}
		
		

		// Remove the constraint for the last flow
		Constraint oldConstraint = endLoopFlow.getConstraints().get(0);
		endLoopFlow.getConstraints().remove(oldConstraint);
		endLoopFlow.getSource().getConstraints().remove(oldConstraint);
		endLoopFlow.getTarget().getConstraints().remove(oldConstraint);
		derfInterpretation.getConstraints().remove(oldConstraint);

		// Add the new target for the flow (ie. a -> b -> a ==> a0 -> b0 -> a1)
		Node newendLoopFlowTarget = coreFactory.createDefaultNode();
		newendLoopFlowTarget.setName(endLoopFlow.getTarget().getName());
		newendLoopFlowTarget.setTypeNode(endLoopFlow.getTarget().getTypeNode());
		addTaskConstraint(newendLoopFlowTarget, derfInterpretation, disabledTaskPredicate);
		graph.addNode(newendLoopFlowTarget);

		endLoopFlow.setTarget(newendLoopFlowTarget);
		addFlowConstraint(endLoopFlow, derfInterpretation, falseFlowPredicate);

		// Start expanding the loop
		Node prevTarget = newendLoopFlowTarget;
		for (int i = 1; i < (multiplier - 1); i++) {
			Iterator<Arrow> loopIterator = loop.iterator();

			while (loopIterator.hasNext()) {
				Arrow currentFlow = loopIterator.next();

				Node newTarget = coreFactory.createDefaultNode();
				newTarget.setName(currentFlow.getTarget().getName());
				newTarget.setTypeNode(currentFlow.getTarget().getTypeNode());
				addTaskConstraint(newTarget, derfInterpretation, disabledTaskPredicate);
				graph.addNode(newTarget);

				Arrow newFlow = coreFactory.createDefaultArrow();
				newFlow.setName(currentFlow.getName());
				newFlow.setTypeArrow(currentFlow.getTypeArrow());
				graph.addArrow(newFlow);

				newFlow.setSource(prevTarget);
				newFlow.setTarget(newTarget);
				prevTarget = newTarget;

				addFlowConstraint(newFlow, derfInterpretation, falseFlowPredicate);
				loopFlowsOrdered.add(newFlow);
			}
		}

		// Update names on loop tasks and flows &&
		// Find all flows that can exit the loop
		HashMap<Node, Arrow> typeNodeToExitFlow = new HashMap<Node, Arrow>();

		int counter = 0;
		int i = 0;

		Node firstSource = loopFlowsOrdered.get(0).getSource();
		firstSource.setName(firstSource.getName() + "(" + i + ")");

		// Check for exit flows in the original first source
		for (Arrow potentialExitFlow : firstSource.getOutgoings()) {
			if (!loop.contains(potentialExitFlow)) {
				typeNodeToExitFlow.put(firstSource.getTypeNode(), potentialExitFlow);
			}
		}
		
		// Keep track of last flow that was updated to create the last potential exit flow
		Arrow lastFlowInLoop = null;

		// Update the names of tasks and flows in the loop (a -> b -> a ==> a0
		// -> b0 -> a1)
		for (Arrow flow : loopFlowsOrdered) {
			lastFlowInLoop = flow;
			flow.setName(flow.getName() + "(" + i + ")");

			counter++;
			if (counter % loop.size() == 0) {
				i++;
			}

			flow.getTarget().setName(flow.getTarget().getName() + "(" + i + ")");

			// Check to see if there are exit flows iff we are dealing with one
			// of the original flows
			if (counter < loop.size()) {
				for (Arrow potentialExitFlow : flow.getSource().getOutgoings()) {
					if (!loop.contains(potentialExitFlow)) {
						typeNodeToExitFlow.put(potentialExitFlow.getSource().getTypeNode(), potentialExitFlow);
					}
				}
			}
			// Else, add new exit flow on copy if the original had one
			else {
				Arrow exitFlow = typeNodeToExitFlow.get(flow.getSource().getTypeNode());
				if (exitFlow != null) {
					Arrow newExitFlow = coreFactory.createDefaultArrow();
					newExitFlow.setTypeArrow(exitFlow.getTypeArrow());
					newExitFlow.setName(exitFlow.getName() + "(" + i + ")");
					newExitFlow.setSource(flow.getSource());
					newExitFlow.setTarget(exitFlow.getTarget());
					addFlowConstraint(newExitFlow, derfInterpretation, falseFlowPredicate);
					graph.addArrow(newExitFlow);
				}
			}
		}
		
		// Check the last flows target as well
		Arrow lastFlowExitFlow = typeNodeToExitFlow.get(lastFlowInLoop.getTarget().getTypeNode());
		if (lastFlowExitFlow != null) {
			Arrow newExitFlow = coreFactory.createDefaultArrow();
			newExitFlow.setTypeArrow(lastFlowExitFlow.getTypeArrow());
			newExitFlow.setName(lastFlowExitFlow.getName() + "(" + i + ")");
			newExitFlow.setSource(lastFlowInLoop.getTarget());
			newExitFlow.setTarget(lastFlowExitFlow.getTarget());
			addFlowConstraint(newExitFlow, derfInterpretation, falseFlowPredicate);
			graph.addArrow(newExitFlow);
		}

		// Update the names of the original exit flows
		for (Node exitFlowKey : typeNodeToExitFlow.keySet()) {
			Arrow exitFlow = typeNodeToExitFlow.get(exitFlowKey);
			exitFlow.setName(exitFlow.getName() + "(0)");
		}
	}

	public static void expandNodemultLoops(Specification derfInterpretation) {
		List<Node> nodeMults = new LinkedList<Node>();

		for (Node node : derfInterpretation.getGraph().getNodes()) {
			for (Constraint constraint : node.getTypeNode().getConstraints()) {
				if(constraint.getPredicate().getSymbol() == null) {
					System.out.println(constraint.getPredicate());
				}
				else if (constraint.getPredicate().getSymbol().equals(DERFConstants.PREDICATE_NODEMULT)) {
					nodeMults.add(node);
				}
			}
		}

		for (Node node : nodeMults) {
			expandNodeMultLoop(derfInterpretation, node);
		}
	}

	private static LinkedHashSet<Arrow> findLoop(Node root) {
		LinkedHashSet<Arrow> result = new LinkedHashSet<Arrow>();

		for (Arrow a : root.getIncomings()) {
			if (findLoop(root, a.getSource(), result)) {
				result.add(a);
			}
			;
		}

		return result;
	}

	private static boolean findLoop(Node root, Node current, LinkedHashSet<Arrow> result) {
		if (current == root) {
			return true;
		}

		boolean inLoopFlag = false;
		for (Arrow a : current.getIncomings()) {
			if (findLoop(root, a.getSource(), result)) {
				result.add(a);
				inLoopFlag = true;
			}
		}

		return inLoopFlag;
	}

	public static Signature getDERFInterpretationSignature() {
		// URI signatureURI =
		// URI.createFileURI(PATH_TO_DERF_INTERPRETATION_SIGNATURE);
		//
		// ResourceSetImpl resourceSet = SignatureUtils.getResourceSet();
		//
		// Resource signature = null;
		// try {
		// signature = resourceSet.getResource(signatureURI, true);
		// }
		// catch(Exception e) {
		// System.out.println(e.toString());
		// System.exit(-1);
		// // signature = resourceSet.getResource(signatureURI, false);
		// }
		//
		// try {
		// signature.load(null);
		// }
		// catch(IOException e) {
		// System.out.println(e.toString());
		// System.exit(-1);
		// }
		//
		// return (Signature) signature.getContents().get(0);

		Specification signatureSpecification = DPFCoreUtil.loadSpecification(URI
				.createFileURI(PATH_TO_SPEC_WITH_DERF_INTERPRETATION_SIGNATURE));

		return signatureSpecification.getSignature();

	}

	public static Specification getSpecification(String filepath) {
		URI specURI = URI.createFileURI(filepath); // NOT OK!
		return DPFCoreUtil.loadSpecification(specURI);
	}

	public static void saveSpecification(final Specification dpfSpecification, String specificationFile) {
		URI specUri = URI.createFileURI(specificationFile);
		DPFCoreUtil.saveSpecification(specUri, dpfSpecification);
	}

	public static String specificationToString(Specification spec) {
		String result = "";

		for (Node n : spec.getGraph().getNodes()) {
			result += n.getName();
			for (Constraint c : n.getConstraints()) {
				result += " [" + c.getPredicate().getSymbol() + "]";
			}
			result += ": " + n.getTypeName() + "\n";
		}

		for (Arrow a : spec.getGraph().getArrows()) {
			result += a.getName();
			for (Constraint c : a.getConstraints()) {
				result += " [" + c.getPredicate().getSymbol() + "]";
			}
			result += " (" + a.getSource().getName() + ", " + a.getTarget().getName() + ")";
			result += ": " + a.getTypeName() + "\n";
		}

		return result;
	}

	public static Boolean debug = false;

	public static String PATH_TO_DERF_INTERPRETATION_SIGNATURE = "resources/derf/derfInterpretationSignature.sig";

	public static String PATH_TO_SPEC_WITH_DERF_INTERPRETATION_SIGNATURE = "resources/derf/derfInterpretation.xmi";

	private static CoreFactory coreFactory = CoreFactory.eINSTANCE;
}
