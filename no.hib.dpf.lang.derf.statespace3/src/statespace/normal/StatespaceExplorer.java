package statespace.normal;

import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FINISHED;
import static util.DERFConstants.PREDICATE_RUNNING;
import static util.TransformationUtil.transformAllMatches;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import main.Main;
import no.hib.dpf.core.Node;
import no.hib.dpf.core.Specification;

import org.eclipse.emf.henshin.model.Rule;

import util.FILOQueue;
import util.DERFUtil;

public class StatespaceExplorer {
	private List<Rule> interpretationRules;
	private FILOQueue<State> toExplore;
	private StatespaceGraph stateSpaceGraph;
	
	private int totalTransformationAttempts = 0;
	private int totalSuccessfulTransformations = 0;
	
	private long totalTransformationTime = 0;
	private long totalSuccessfulTransformationTime = 0;
	private long longestSuccessfulTransformationTime = 0;
	private long longestUnsuccessfulTransformationTime = 0;
	
	public StatespaceExplorer(List<Rule> interpretationRules, Specification initialState) {
		this.interpretationRules = interpretationRules;
		this.toExplore = new FILOQueue<State>();
		this.initialize(initialState);
	}
	
	public boolean step(int n) {
		for(int i = 0; i < n; i++) {
			if(!step()) {
				return false;
			}
		}
		
		return true;
	}
	
	public boolean step() {
		
		if(toExplore.size() <= 0) {
			return false;
		}
		
		State exploring = toExplore.dequeue();
		
		for(Rule rule : interpretationRules) {
			long transformationStart = System.currentTimeMillis();
			
			List<Specification> newStateSpecs = transformAllMatches(exploring.getDerfInterpretation(), rule);
			
			long transformationTime = System.currentTimeMillis() - transformationStart;
			totalTransformationTime += transformationTime;
			totalTransformationAttempts++;
			
			if(newStateSpecs.size() <= 0) {
				if(longestUnsuccessfulTransformationTime < transformationTime) {
					longestUnsuccessfulTransformationTime = transformationTime;
				}
			}
			
			for(Specification newStateSpec : newStateSpecs) {				
				State newState = new State("s" + stateSpaceGraph.getNumberOfStates(), newStateSpec);
				
				boolean isNewState = stateSpaceGraph.addState(newState);
				if(isNewState) {
					toExplore.queue(newState);
					stateSpaceGraph.addEdge(exploring, newState, rule.getName());
				}
				else {
					State original = stateSpaceGraph.getEqualState(newState);
					stateSpaceGraph.addEdge(exploring, original, rule.getName());
					
				}
				
				if(longestSuccessfulTransformationTime < transformationTime) {
					longestSuccessfulTransformationTime = transformationTime;
				}
				
				totalSuccessfulTransformationTime += transformationTime;
				totalSuccessfulTransformations++;
			}
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		String result = "\n\nNumber of transformation rules used: " + interpretationRules.size();
		for(Rule r : interpretationRules) {
			result += "\n\t" + r.getName();
		}
		result += "\n\nTIME STATISTICS:\n";
		result += getTimeStatistics();
		
		result += "\n\nSTATE SPACE\n";
		result += stateSpaceGraph.toString();
		return result;
	}

	private void initialize(Specification initialState) {
		stateSpaceGraph = new StatespaceGraph();
		
		State initial = new State("s0", initialState);
		
		stateSpaceGraph.addState(initial);
		toExplore.queue(initial);
	}
	
	private String getTimeStatistics() {
		String result = "";
		
		long avrgTransformationTime = -1;
		if(totalTransformationAttempts > 0) {
			avrgTransformationTime = totalTransformationTime / totalTransformationAttempts;
		}
		
		long longestTransformationTime = 
				longestSuccessfulTransformationTime > longestUnsuccessfulTransformationTime ?
						longestSuccessfulTransformationTime : longestUnsuccessfulTransformationTime;
				
		long avrgSuccessfullTransformationTime = -1;
		if(totalSuccessfulTransformations > 0) {
			avrgSuccessfullTransformationTime = totalSuccessfulTransformationTime / totalSuccessfulTransformations;
		}
		
		int totalUnsuccessfulTransformations = totalTransformationAttempts - totalSuccessfulTransformations;
		long totalUnsuccessfulTransformationTime = totalTransformationTime- totalSuccessfulTransformationTime;
		
		long avrgUnsuccessfulTransformationTime = -1;
		if(totalUnsuccessfulTransformations > 0) {
			avrgUnsuccessfulTransformationTime = totalUnsuccessfulTransformationTime / totalUnsuccessfulTransformations;
		}
		
		long avrgTotalTimeSuccessfulTransformations = -1;
		if(totalTransformationAttempts > 0) {
			avrgTotalTimeSuccessfulTransformations = totalSuccessfulTransformationTime / totalTransformationAttempts;
		}
		
		result += "Total transformations: " + totalTransformationAttempts +
				"\nTotal time used on transformations: " + totalTransformationTime + " ms" +
				"\nAverage time per transformation: " + avrgTransformationTime + " ms" +
				"\nLongest transformation time: " + longestTransformationTime + " ms" +
				
				"\n\nTotal successfull transformations: " + totalSuccessfulTransformations +
				"\nTotal time used on successfull transformations: " + totalSuccessfulTransformationTime + " ms" +
				"\nAverage time per successfull transformation: " + avrgSuccessfullTransformationTime +  " ms" +
				"\nLongest successfull transformation time: " + longestSuccessfulTransformationTime + " ms" +
				
				"\n\nFailed transformations: " + totalUnsuccessfulTransformations +
				"\nTime used on unsuccessful transformations: " + totalUnsuccessfulTransformationTime +  " ms" +
				"\nAverage time per unsuccessfull transformation: " + avrgUnsuccessfulTransformationTime +  " ms" +
				"\nLongest unsuccessfull transformation time: " + longestUnsuccessfulTransformationTime + " ms" +
				
				"\n\nAverage total time per successful transformation: " + avrgTotalTimeSuccessfulTransformations + " ms";
		
		return result;
	}

	public StatespaceGraph getStatespaceGraph() {
		return stateSpaceGraph;
	}
}