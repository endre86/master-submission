package statespace.normal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import no.hib.dpf.core.Arrow;
import no.hib.dpf.core.Constraint;
import no.hib.dpf.core.Node;
import no.hib.dpf.core.Specification;
import util.DERFConstants;

public class State {

	private String name;
	private Specification derfInterpretation;
	private int hashCode;
	
	public State(String name, Specification derfInterpretation) {
		this.derfInterpretation = derfInterpretation;
		this.name = name;
		pregenerateHashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == null) {
			return false;
		}
		
		if(!(o instanceof State)) {
			return false;
		}
		
		State that = (State) o;
		
		return this.hashCode() == that.hashCode();
	}
	
	@Override
	public String toString() {
		if(derfInterpretation.getGraph().getNodes().size() <= 0) {
			return name + ": Empty";
		}
		String tasks = "";
		
		for(Node task : derfInterpretation.getGraph().getNodes()) {
			String taskString = "";
			
			taskString += task.getName();
			
			for(Constraint constraint : task.getConstraints()) {
				String predicate = constraint.getPredicate().getSymbol();
				
				if(!predicate.equals(DERFConstants.PREDICATE_FLOW_FALSE) &&
						!predicate.equals(DERFConstants.PREDICATE_FLOW_TRUE)) {
					
					taskString += "[" + predicate + "]";
				}
			}
			
			tasks += taskString + ", ";
		}
		
		String flows = "";
		for(Arrow flow : derfInterpretation.getGraph().getArrows()) {
			String flowString = "";
			
			flowString += flow.getName();
			flowString += "[" + flow.getConstraints().get(0)
					.getPredicate().getSymbol() + "]";
			
			flows += flowString + ", ";
		}
		
		return name + " => {" + 
			tasks.substring(0, tasks.length() - 2) + "}" +
			" => {" + flows + "}";
	}
	
	public Specification getDerfInterpretation() {
		return derfInterpretation;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}
	
	// TODO: Do better!
	// Need to make sure nodes and arrows always comes in order for the hashcode to
	// be valid.. Therefore: QUICKFIX!!!!! and pregeneration.
	private void pregenerateHashCode() {
		ArrayList<String> tasks = new ArrayList<String>();
		ArrayList<String> flows = new ArrayList<String>();
		
		for(Node task : this.derfInterpretation.getGraph().getNodes()) {
			String taskString = task.getName();
			for(Constraint c : task.getConstraints()) {
				if(! c.getPredicate().getSymbol().equals(DERFConstants.PREDICATE_FLOW_FALSE) &&
						! c.getPredicate().getSymbol().equals(DERFConstants.PREDICATE_FLOW_TRUE)) {
					taskString += c.getPredicate().getSymbol();
				}
			}
			tasks.add(taskString);
		}
		
		for(Arrow flow : this.derfInterpretation.getGraph().getArrows()) {
			String flowString = flow.getName();
			for(Constraint c : flow.getConstraints()) {
				if(c.getPredicate().getSymbol().equals(DERFConstants.PREDICATE_FLOW_FALSE) ||
						c.getPredicate().getSymbol().equals(DERFConstants.PREDICATE_FLOW_TRUE)) {
					flowString += c.getPredicate().getSymbol();
				}
			}
			
			flows.add(flowString);
		}
		
		String[] tasksArray = new String[tasks.size()];
		tasks.toArray(tasksArray);
		Arrays.sort(tasksArray);
		
		String[] flowsArray = new String[flows.size()];
		flows.toArray(flowsArray);
		Arrays.sort(flowsArray);
		
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
		
		hashCodeBuilder = hashCodeBuilder.append(tasksArray);
		hashCodeBuilder = hashCodeBuilder.append(flowsArray);
		
		hashCode = hashCodeBuilder.toHashCode();
	}
}
