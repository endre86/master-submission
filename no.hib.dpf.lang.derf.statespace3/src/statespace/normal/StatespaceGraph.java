package statespace.normal;

import java.awt.Component;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import util.DERFUtil;
import util.Edge;

public class StatespaceGraph {
	private List<State> states;
	private List<Edge<State, State>> edges;
	private List<Edge<Edge<State, State>, String>> labels;
	
	public StatespaceGraph() {
		this.states = new LinkedList<State>();
		this.edges = new LinkedList<Edge<State, State>>();
		this.labels = new LinkedList<Edge<Edge<State, State>, String>>();
	}
	
	public boolean addState(State state) {
		if(this.containsState(state)) {
			return false;
		}
		
		states.add(state);
		return true;
	}
	
	public boolean addEdge(State source, State target) {
		if(this.containsEdge(source, target)) {
			return false;
		}
		
		Edge<State, State> newEdge = new Edge<State, State>(source, target);
		edges.add(newEdge);
		return true;
	}
	
	public boolean addEdge(State source, State target, String label) {
		if(this.containsEdge(source, target)) {
			return false;
		}
		
		Edge<State, State> newEdge = new Edge<State, State>(source, target);
		edges.add(newEdge);
		
		Edge<Edge<State, State>, String> newLabel = new Edge<Edge<State, State>, String>(newEdge, label);
		labels.add(newLabel);
		return true;
	}
	
	public boolean containsState(State state) {
		for(State s : states) {
			if(s.equals(state)) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean containsEdge(State source, State target) {
		for(Edge<State, State> edge : edges) {
			if(edge.source.equals(source) &&
			   edge.target.equals(target)) {
				return true;
			}
		}
		
		return false;
	}
	
	public int getNumberOfStates() {
		return states.size();
	}
	
	public int getNumberOfEdges() {
		return edges.size();
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == null) {
			return false;
		}
		
		if(!(o instanceof StatespaceGraph)) {
			return false;
		}
		
		StatespaceGraph other = (StatespaceGraph) o;
		
		if(other.getNumberOfEdges() != this.getNumberOfEdges()) {
			DERFUtil.debug("StatespaceGraph.equals => Less edges! This: " + this.getNumberOfEdges() + ", Other: " +  other.getNumberOfEdges());
			return false;
		}
		
		if(other.getNumberOfStates() != this.getNumberOfStates()) {
			DERFUtil.debug("StatespaceGraph.equals => Less states! This: " + this.getNumberOfStates() + ", Other: " +  other.getNumberOfStates());
			return false;
		}
		
		for(State thisState : this.states) {
			if(! other.containsState(thisState)) {
				DERFUtil.debug("Other does not contain state:");
				DERFUtil.debug(thisState.toString(), 1);
				return false;
			}
		}
		
		for(Edge<State, State> thisEdge : this.edges) {
			if(! other.containsEdge(thisEdge.source, thisEdge.target)) {
				DERFUtil.debug("Other does not contain edge:");
				DERFUtil.debug(thisEdge.source.toString(), 1);
				DERFUtil.debug("=>");
				DERFUtil.debug(thisEdge.target.toString(), 1);
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		String result = "";
		
		result += "Number of states: " + states.size();
		result += "\nNumber of edges: " + edges.size();
		
		result += "\n\nStates:";
		for(State state : states) {
			result += "\n\t" + state.toString();
		}
		
		result += "\n\nEdges:";
		for(int i = 0; i < edges.size(); i++) {
			Edge<State, State> edge = edges.get(i);
			result +="\n\t" + edge.source.getName() + " => " + edge.target.getName();
			
			if(labels.size() >= (i + 1)) {
				result += " (" + labels.get(i).target + ")";
			}
		}
		
		return result;
	}

	public List<State> getStates() {
		return states;
	}

	public State getEqualState(State state) {
		for(State s : states) {
			if(s.equals(state)) {
				return s;
			}
		}
		
		return null;
	}

	public List<Edge<State, State>> getEdges() {
		return edges;
	}
}
