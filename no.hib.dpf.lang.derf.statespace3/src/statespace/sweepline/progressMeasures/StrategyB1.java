package statespace.sweepline.progressMeasures;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import util.DERFUtil;
import static util.DERFConstants.*;
import no.hib.dpf.core.*;

public class StrategyB1 implements IProgressMapper {
	public Map<String, String> progressMap;
	private Specification derfImplementation;

	@Override
	public void initializeProgressMapper(Specification derfImplementation) {
		progressMap = new HashMap<String, String>();
		this.derfImplementation = derfImplementation;
		List<Node> initials = findInitials(derfImplementation);
		
		for(Node initial : initials) {
			HashSet<Arrow> processed = new HashSet<Arrow>();
			String newProgressValue = getMax(progressMap);
			progressMap.put(initial.getName(), newProgressValue);
			initializeProgressMap(initial, processed);
		}
	}
	
	private String getMax(Map<String, String> progressMap) {
		String max = "1";
		
		for(String value : progressMap.values()) {
			if(value.length() > max.length()) {
				max = value;
			}
		}
		
		return max;
	}

	private Node initializeProgressMap(Node current, HashSet<Arrow> processed) {
		
		if(current.getOutgoings().size() == 0) {
			return current;
		}
		
		List<Arrow> outgoings = current.getOutgoings();
		
		for(Arrow a : outgoings) {
			if(!processed.contains(a)) {
				if(hasPredicate(a, PREDICATE_XOR_SPLIT)) {
					Arrow b = getOther(a, PREDICATE_XOR_SPLIT);
					
					String newProgressValueA = progressMap.get(current.getName())+ "0";
					progressMap.put(a.getTarget().getName(), newProgressValueA);

					processed.add(a);
					current = initializeProgressMap(a.getTarget(), processed);
					
					String newProgressValueB = progressMap.get(current.getName()) + "0";
					progressMap.put(b.getTarget().getName(), newProgressValueB);
					
					processed.add(b);
					current = initializeProgressMap(b.getTarget(), processed);
				}
				else if(hasPredicate(a, PREDICATE_AND_SPLIT)) {
					Arrow b = getOther(a, PREDICATE_AND_SPLIT);
					
					String newProgressValue = progressMap.get(current.getName()) + "0";
					progressMap.put(a.getTarget().getName(), newProgressValue);
					progressMap.put(b.getTarget().getName(), newProgressValue);
					
					processed.add(a);
					processed.add(b);
					
					current = initializeProgressMap(a.getTarget(), processed);
					current = initializeProgressMap(b.getTarget(), processed);
				}
				else {
					String newProgressValue = progressMap.get(current.getName()) + "0";
					progressMap.put(a.getTarget().getName(), newProgressValue);
					
					if(!processed.contains(a)) {
						processed.add(a);
						current = initializeProgressMap(a.getTarget(), processed);
					}
				}
			}
		}
		
		return current;
	}

	private boolean hasPredicate(Node n, String predicate) {
		for(Constraint c : n.getConstraints()) {
			if(c.getPredicate().getSymbol().equals(predicate)) {
				return true;
			}
		}
		
		return false;
	}

	protected Arrow getOther(Arrow a, String predicate) {
		for(Constraint c : derfImplementation.getConstraints()) {
			for(Arrow _a : c.getArrows()) {
				if(a == _a) {
					if(a == c.getArrows().get(0)) {
						return c.getArrows().get(1);
					}
					else {
						return c.getArrows().get(0);
					}
				}
			}
		}
		
		return a;
	}

	private boolean hasPredicate(Arrow a, String predicate) {
		for(Constraint c : a.getConstraints()) {
			if(c.getPredicate().getSymbol().equals(predicate)) {
				return true;
			}
		}
		
		return false;
	}

	private List<Node> findInitials(Specification derfImplementation) {
		List<Node> result = new LinkedList<Node>();
		
		for(Node n : derfImplementation.getGraph().getNodes()) {
			if(n.getIncomings().size() == 0) {
				result.add(n);
			}
		}
		
		return result;
	}

	@Override
	public int getProgressMeasure(Specification derfInstance) {
		String result = "0";
		
		for(Node n : derfInstance.getGraph().getNodes()) {
			for(Constraint c : n.getConstraints()) {
				if(c.getPredicate().getSymbol().equals(PREDICATE_ENABLED) ||
				   c.getPredicate().getSymbol().equals(PREDICATE_RUNNING) ||
				   c.getPredicate().getSymbol().equals(PREDICATE_DISABLED)) {
					if(progressMap.containsKey((n.getTypeName()))) {
						String currentBin = progressMap.get(n.getTypeName());
						
						for(int i = 0; i < currentBin.length() - result.length(); i++) {
							result = "0" + result;
						}
						
						for(int i = 0; i < currentBin.length(); i++) {
							if(currentBin.charAt(i) == '1') {
								char[] resultCharArr = result.toCharArray();
								resultCharArr[i] = '1';
								result = String.copyValueOf(resultCharArr);
							}
						}
					}
					else {
						DERFUtil.debug("Progress mapping does not contain " + n.getName());
					}
				}
			}
		}
		
		return Integer.parseInt(result, 2);
	}
	
	@Override
	public String toString() {
		String result = "\n";
		
		for(Entry<String, String> entry  : progressMap.entrySet()) {
			result += entry.getKey() + " => " + entry.getValue() + "\n";
		}
		
		return result;
	}
}
