package statespace.sweepline.progressMeasures.old;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import statespace.sweepline.progressMeasures.IProgressMapper;
import util.DERFUtil;
import static util.DERFConstants.*;
import no.hib.dpf.core.*;

public class StrategyA2 implements IProgressMapper {
	public Map<String, Integer> progressMap;
	private Specification derfImplementation;

	@Override
	public void initializeProgressMapper(Specification derfImplementation) {
		progressMap = new HashMap<String, Integer>();
		this.derfImplementation = derfImplementation;
		List<Node> initials = findInitials(derfImplementation);
		
		for(Node initial : initials) {
			HashSet<Arrow> processed = new HashSet<Arrow>();
			int newProgressValue = getMax(progressMap);
			progressMap.put(initial.getName(), newProgressValue);
			initializeProgressMap(initial, processed);
		}
	}
	
	private int getMax(Map<String, Integer> progressMap) {
		int max = 0;
		
		for(int value : progressMap.values()) {
			if(value > max) {
				max = value;
			}
		}
		
		return max;
	}

	private Node initializeProgressMap(Node current, HashSet<Arrow> processed) {
		
		if(current.getOutgoings().size() == 0) {
			return current;
		}
		
		List<Arrow> outgoings = current.getOutgoings();
		
		for(Arrow a : outgoings) {
			if(!processed.contains(a)) {
				if(hasPredicate(a, PREDICATE_AND_SPLIT)) {
					Arrow b = getOther(a, PREDICATE_AND_SPLIT);
					
					int newProgressValueA = progressMap.get(current.getName())+ 1;
					progressMap.put(a.getTarget().getName(), newProgressValueA);

					processed.add(a);
					current = initializeProgressMap(a.getTarget(), processed);
					
					int newProgressValueB = progressMap.get(current.getName()) + 1;
					progressMap.put(b.getTarget().getName(), newProgressValueB);
					
					processed.add(b);
					current = initializeProgressMap(b.getTarget(), processed);
				}
				else if(hasPredicate(a, PREDICATE_XOR_SPLIT)) {
					Arrow b = getOther(a, PREDICATE_XOR_SPLIT);
					
					int newProgressValue = progressMap.get(current.getName()) + 1;
					progressMap.put(a.getTarget().getName(), newProgressValue);
					progressMap.put(b.getTarget().getName(), newProgressValue);
					
					processed.add(a);
					processed.add(b);
					
					initializeProgressMap(a.getTarget(), processed);
					initializeProgressMap(b.getTarget(), processed);
				}
				else {
					int newProgressValue = progressMap.get(current.getName()) + 1;
					progressMap.put(a.getTarget().getName(), newProgressValue);
					
					if(!processed.contains(a)) {
						processed.add(a);
						initializeProgressMap(a.getTarget(), processed);
					}
				}
			}
		}
		
		return current;
	}

	private boolean hasPredicate(Node n, String predicate) {
		for(Constraint c : n.getConstraints()) {
			if(c.getPredicate().getSymbol().equals(predicate)) {
				return true;
			}
		}
		
		return false;
	}

	protected Arrow getOther(Arrow a, String predicate) {
		for(Constraint c : derfImplementation.getConstraints()) {
			for(Arrow _a : c.getArrows()) {
				if(a == _a) {
					if(a == c.getArrows().get(0)) {
						return c.getArrows().get(1);
					}
					else {
						return c.getArrows().get(0);
					}
				}
			}
		}
		
		return a;
	}

	private boolean hasPredicate(Arrow a, String predicate) {
		for(Constraint c : a.getConstraints()) {
			if(c.getPredicate().getSymbol().equals(predicate)) {
				return true;
			}
		}
		
		return false;
	}

	private List<Node> findInitials(Specification derfImplementation) {
		List<Node> result = new LinkedList<Node>();
		
		for(Node n : derfImplementation.getGraph().getNodes()) {
			if(n.getIncomings().size() == 0) {
				result.add(n);
			}
		}
		
		return result;
	}

	@Override
	public int getProgressMeasure(Specification derfInstance) {
		int result = 0;
		
		for(Node n : derfInstance.getGraph().getNodes()) {
			for(Constraint c : n.getConstraints()) {
				if(c.getPredicate().getSymbol().equals(PREDICATE_ENABLED) ||
				   c.getPredicate().getSymbol().equals(PREDICATE_RUNNING) ||
				   c.getPredicate().getSymbol().equals(PREDICATE_DISABLED)) {
					if(progressMap.containsKey((n.getTypeName()))) {
						result += progressMap.get(n.getTypeName());
					}
					else {
						DERFUtil.debug("Progress mapping does not contain " + n.getName());
					}
				}
			}
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		String result = "\n";
		
		for(Entry<String, Integer> entry  : progressMap.entrySet()) {
			result += entry.getKey() + " => " + entry.getValue() + "\n";
		}
		
		return result;
	}
}
