package statespace.sweepline.progressMeasures;

import no.hib.dpf.core.Specification;

public class Strategy0 implements IProgressMapper{

	@Override
	public void initializeProgressMapper(Specification derfImplementation) {
		
	}

	@Override
	public int getProgressMeasure(Specification derfInstance) {
		return 0;
	}

}
