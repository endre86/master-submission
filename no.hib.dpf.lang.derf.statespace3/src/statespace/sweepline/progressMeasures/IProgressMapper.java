package statespace.sweepline.progressMeasures;

import no.hib.dpf.core.Specification;

public interface IProgressMapper {
	public void initializeProgressMapper(Specification derfImplementation);
	public int getProgressMeasure(Specification derfInstance);
}
