package statespace.sweepline;

import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FINISHED;
import static util.DERFConstants.PREDICATE_RUNNING;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.henshin.model.Module;
import org.eclipse.emf.henshin.model.Rule;

import statespace.normal.State;
import statespace.normal.StatespaceGraph;
import statespace.sweepline.progressMeasures.IProgressMapper;
import util.DERFUtil;
import util.Edge;
import util.FILOQueue;
import util.PriorityQueue2;
import util.TransformationUtil;
import no.hib.dpf.core.Arrow;
import no.hib.dpf.core.Node;
import no.hib.dpf.core.Signature;
import no.hib.dpf.core.Specification;

public class SweeplineStatespaceExplorer {
	private IProgressMapper progressMapper;

	private List<Rule> interpretationRules;
	
	private int stateCounter = 0;
	
	private PriorityQueue2 roots;
	private PriorityQueue2 unprocessed;
	private HashSet<SweeplineState> nodes;
	private HashSet<SweeplineState> layer;
	
	// Stats
	private int maxStatesInMemory = 0;
	private int maxStatesInLayer = 0;
	private int maxStatesInRoots = 0;
	
	private int totalTransformationAttempts = 0;
	private int totalSuccessfulTransformations = 0;
	
	private long totalTransformationTime = 0;
	private long totalSuccessfulTransformationTime = 0;
	private long longestSuccessfulTransformationTime = 0;
	private long longestUnsuccessfulTransformationTime = 0;
	
	// Debug / verify
	public StatespaceGraph statespaceGraph;
	
	public SweeplineStatespaceExplorer(List<Rule> interpretationRules, Specification derfImplementation, Specification derfInterpretation, IProgressMapper progressMapper) {
		this.interpretationRules = interpretationRules;
		this.progressMapper = progressMapper;
		
		this.roots = new PriorityQueue2();
		this.unprocessed = new PriorityQueue2();
		this.nodes = new HashSet<SweeplineState>();
		this.layer = new HashSet<SweeplineState>();

		this.initialize(derfImplementation, derfInterpretation);
	}
		
	public boolean step() {
		return step(1);
	}
	
	public boolean step(int steps) {
		if(steps <= 0) {
			return true;
		}
		
		// Roots.insert (initialize)
		// Nodes.insert (initialize)
		
		while(roots.size() > 0) { // while ! roots.emoty
			if(layer.size() > maxStatesInLayer) {
				maxStatesInLayer = layer.size();
			}
			
			if(roots.size() > maxStatesInRoots) {
				maxStatesInRoots = roots.size();
			}
			
			unprocessed = roots;
			roots = new PriorityQueue2();
			layer = new HashSet<SweeplineState>();
			
			int layerValue = unprocessed.getMin().getProgressValue();
			
			while(unprocessed.size() > 0) {
				SweeplineState current = unprocessed.getMin();
				unprocessed.removeMin();
				
				if(current != null && layerValue < current.getProgressValue()) {
					// for all s' such that not(s.persistent) do					
					for(SweeplineState s : layer) {
						if(!s.isPersistent()) {
							if(!nodes.remove(s)) {
								DERFUtil.debug("Cound not remove state on line 157!");
							}
						}
					}
					
					layer = new HashSet<SweeplineState>();
					layerValue = current.getProgressValue();
				}
				
				if(layer.size() > maxStatesInLayer) {
					maxStatesInLayer = layer.size();
				}
				
				layer.add(current);
				
				// for all (t, s') such that s -(t)-> s' do
				List<SweeplineState> successors = getSuccessors(current);
				
				// Keep track of max in memory
				HashSet<SweeplineState> allCurrentStates = new HashSet<SweeplineState>();
				allCurrentStates.addAll(successors);
				allCurrentStates.addAll(layer);
				allCurrentStates.addAll(unprocessed.getAll());
				allCurrentStates.addAll(roots.getAll());
				allCurrentStates.addAll(nodes);
				if(allCurrentStates.size() > maxStatesInMemory) {
					maxStatesInMemory = allCurrentStates.size();
				}
				
				for(SweeplineState successor : successors) {
					if(successor != null && !nodes.contains(successor)) {
						nodes.add(successor);
						successor.setName("s" + stateCounter++);
						if(layerValue > successor.getProgressValue()) {
							successor.setPersistent(true);
							roots.add(successor);
						}
						else {
							unprocessed.add(successor);
						}
						
						if(DERFUtil.debug) {
							statespaceGraph.addState((State)successor);
							statespaceGraph.addEdge((State) current, (State) successor);
						}
					}
					else if(DERFUtil.debug) {
						SweeplineState preexistingSuccessor = successor;
						
						Iterator<SweeplineState> nodesIterator = nodes.iterator();
						while(nodesIterator.hasNext()) {
							SweeplineState next = nodesIterator.next();
							if(next.equals(successor)) {
								preexistingSuccessor = next;
								break;
							}
						}
						
						statespaceGraph.addEdge((State) current, (State) preexistingSuccessor);
					}
				}
			}
			
			
			if(layer.size() > maxStatesInLayer) {
				maxStatesInLayer = layer.size();
			}
		}
		
		return false;
	}
	
	public StatespaceGraph getStatespaceGraph() {
		return statespaceGraph;
	}
	
	@Override
	public String toString() {
		String result = "\n\nNumber of transformation rules used: " + interpretationRules.size();
		
		for(Rule r : interpretationRules) {
			result += "\n\t" + r.getName();
		}
		
		long avrgTransformationTime = -1;
		if(totalTransformationAttempts > 0) {
			avrgTransformationTime = totalTransformationTime / totalTransformationAttempts;
		}
		
		long longestTransformationTime = 
				longestSuccessfulTransformationTime > longestUnsuccessfulTransformationTime ?
						longestSuccessfulTransformationTime : longestUnsuccessfulTransformationTime;
				
		long avrgSuccessfullTransformationTime = -1;
		if(totalSuccessfulTransformations > 0) {
			avrgSuccessfullTransformationTime = totalSuccessfulTransformationTime / totalSuccessfulTransformations;
		}
		
		int totalUnsuccessfulTransformations = totalTransformationAttempts - totalSuccessfulTransformations;
		long totalUnsuccessfulTransformationTime = totalTransformationTime- totalSuccessfulTransformationTime;
		
		long avrgUnsuccessfulTransformationTime = -1;
		if(totalUnsuccessfulTransformations > 0) {
			avrgUnsuccessfulTransformationTime = totalUnsuccessfulTransformationTime / totalUnsuccessfulTransformations;
		}
		
		long avrgTotalTimeSuccessfulTransformations = -1;
		if(totalTransformationAttempts > 0) {
			avrgTotalTimeSuccessfulTransformations = totalSuccessfulTransformationTime / totalTransformationAttempts;
		}
		
		result += "\n\nTIME STATISTICS:\n";
		result += "Total transformations: " + totalTransformationAttempts +
				"\nTotal time used on transformations: " + totalTransformationTime + " ms" +
				"\nAverage time per transformation: " + avrgTransformationTime + " ms" +
				"\nLongest transformation time: " + longestTransformationTime + " ms" +
				
				"\n\nTotal successfull transformations: " + totalSuccessfulTransformations +
				"\nTotal time used on successfull transformations: " + totalSuccessfulTransformationTime + " ms" +
				"\nAverage time per successfull transformation: " + avrgSuccessfullTransformationTime +  " ms" +
				"\nLongest successfull transformation time: " + longestSuccessfulTransformationTime + " ms" +
				
				"\n\nFailed transformations: " + totalUnsuccessfulTransformations +
				"\nTime used on unsuccessful transformations: " + totalUnsuccessfulTransformationTime +  " ms" +
				"\nAverage time per unsuccessfull transformation: " + avrgUnsuccessfulTransformationTime +  " ms" +
				"\nLongest unsuccessfull transformation time: " + longestUnsuccessfulTransformationTime + " ms" +
				
				"\n\nAverage total time per successful transformation: " + avrgTotalTimeSuccessfulTransformations + " ms";
		
		result += "\n\nSWEEPLINE STATISTICS:";
		result += "\nMax in memory: " + maxStatesInMemory;
		result += "\nMax in layer: " + maxStatesInLayer;
		result += "\nMax in roots: " + maxStatesInRoots;
		
		return result;
	}
	
	public String toShortString() {
		return "Max in memory: " + maxStatesInMemory;
	}
	
	private List<SweeplineState> getSuccessors(SweeplineState current) {
		Specification currentSpecification = current.getDerfInterpretation();
		
		List<Specification> successorSpecifications = new LinkedList<Specification>();
		
		for(Rule interpretationRule : interpretationRules) {
			long transformationStart = System.currentTimeMillis();
			int successorSpecificationSizeBefore = successorSpecifications.size();
			
			successorSpecifications.addAll(
					TransformationUtil.transformAllMatches(currentSpecification, interpretationRule));
			
			long transformationTime = System.currentTimeMillis() - transformationStart;
			totalTransformationTime += transformationTime;
			
			totalTransformationAttempts++;
			if(successorSpecifications.size() > successorSpecificationSizeBefore) {
				totalSuccessfulTransformations++;
				totalSuccessfulTransformationTime += transformationTime;
				
				if(longestSuccessfulTransformationTime < transformationTime) {
					longestSuccessfulTransformationTime = transformationTime;
				}
			}
			else {
				if(longestUnsuccessfulTransformationTime < transformationTime) {
					longestUnsuccessfulTransformationTime = transformationTime;
				}
			}
		}
		
		List<SweeplineState> successorStates = new LinkedList<SweeplineState>();
		
		for(Specification successorSpecification : successorSpecifications) {
			SweeplineState newState = new SweeplineState(successorSpecification, "NA", calculateProgressMeasure(successorSpecification), false);
			if(!newState.equals(current)) {
				successorStates.add(newState);
			}
		}
		
		return successorStates;
	}

	private int calculateProgressMeasure(Specification derfInterpretation) {
		return this.progressMapper.getProgressMeasure(derfInterpretation);
	}
	
	private void initialize(Specification derfImplementation, Specification initialState) {		
		progressMapper.initializeProgressMapper(derfImplementation);
		
		SweeplineState s0 = new SweeplineState(
				initialState, "s" + stateCounter++, calculateProgressMeasure(initialState), false);
		roots.add(s0);
		nodes.add(s0);
		
		if(DERFUtil.debug) {
			statespaceGraph = new StatespaceGraph();
			statespaceGraph.addState(new State("s0", initialState));
		}
	}
}
