package statespace.sweepline;

import statespace.normal.State;

import java.util.Map;

import no.hib.dpf.core.Specification;

public class SweeplineState extends State implements Comparable<SweeplineState> {
	
	private int progressvalue;
	private boolean persistent;

	public SweeplineState(Specification state, String stateName, int progressValue, boolean persistent) {
		super(stateName, state);
		
		this.progressvalue = progressValue;
		this.persistent = persistent;
	}
	
	public boolean isPersistent() {
		return persistent;
	}
	
	public int getProgressValue() {
		return progressvalue;
	}

	@Override
	public int compareTo(SweeplineState state) {
		return this.getProgressValue() - state.getProgressValue();
	}

	public void setPersistent(boolean persistent) {
		this.persistent = persistent;
		
	}
	
}
