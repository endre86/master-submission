package statespace.sweepline;

import statespace.normal.State;

import java.util.ArrayList;
import java.util.List;

import util.Edge;

public class SweeplineGraph<T extends SweeplineState> {
	private class Edge {
		SweeplineState source;
		SweeplineState target;
		
		protected Edge(SweeplineState source, SweeplineState target) {
			this.source = source;
			this.target = target;
		}
	}
	
	private int size;
	
	private List<SweeplineState> states;
	private List<Edge> edges;
	
	public void addState(SweeplineState state) {
		this.states.add(state);
	}
	
	public void addEdge(SweeplineState source, SweeplineState target) {
		this.edges.add(new Edge(source, target));
	}
	
	public boolean containsState(SweeplineState state) {
		if(getState(state) == null) {
			return false;
		}
		
		return true;
	}
	
	public SweeplineState getState(SweeplineState state) {
		for(SweeplineState s : states) {
			if(s.equals(state)) {
				return s;
			}
		}
		
		return null;
	}
	
	public int size() {
		return states.size();
	}
	
	public List<SweeplineState> getOutgoingFrom(SweeplineState state) {
		List<SweeplineState> result = new ArrayList<SweeplineState>();
		
		for(Edge e : edges) {
			if(e.source.equals(state)) {
				result.add(e.target);
			}
		}
		
		return result;
	}
	
	public List<SweeplineState> getIncomingFrom(SweeplineState state) {
		List<SweeplineState> result = new ArrayList<SweeplineState>();
		
		for(Edge e : edges) {
			if(e.target.equals(state)) {
				result.add(e.source);
			}
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		String states = "States:\n";
		for(SweeplineState s : this.states) {
			states += "\t" + s.toString() + "\n";
		}
		
		String edges = "Edges:\n";
		int count = 0;
		for(Edge e : this.edges) {
			edges += "\te" + count++ + ": {" + e.source.getName();
			edges += " => " + e.target.getName() + "}\n";
		}
		
		String other = "";
		other += "States: " + this.states.size();
		other += "\nEdges: " + this.edges.size();
		
		return other + "\n\n" + states + "\n\n" + edges;
	}
}
