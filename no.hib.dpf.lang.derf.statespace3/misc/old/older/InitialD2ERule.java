package rules.old.older;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.DELETE;
import static org.eclipse.emf.henshin.model.Action.Type.FORBID;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_RUNNING;
import static util.DERFConstants.TASK;
import static util.DERFConstants.FLOW;

import java.util.LinkedList;
import java.util.List;

import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.IRule;
import rules.RuleUtil;

public class InitialD2ERule implements IRule {

	private static CorePackage cp = CorePackage.eINSTANCE;
	
	public Rule createRule() {
		Rule rule = RuleUtil.createRule("Initial Rule");
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		// BASICS
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE);
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE);
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, "graph");
		
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);
		
		// WORKING ON x:y:Task
		Node task = createNode(lhs, cp.getNode(), PRESERVE, "task");
		Node taskType = createNode(lhs, cp.getNode(), PRESERVE);
		Node taskTypeType = createNode(lhs, cp.getNode(), PRESERVE);
		createAttr(taskTypeType, cp.getNode_Name(), TASK, PRESERVE);
		
		createEdge(graph, task, cp.getGraph_Nodes(), PRESERVE);
		createEdge(task, taskType, cp.getNode_TypeNode(), PRESERVE);
		createEdge(taskType, taskTypeType, cp.getNode_TypeNode(), PRESERVE);
		
		
		// FORBID INCOMMING ARROW
//		Node flow = createNode(lhs, cp.getArrow(), FORBID);
//		Node forbidGraph = flow.getGraph().getNode("graph");
//		Node forbidFlowTypeType = flow.getGraph().getNode("flowTypeType");
//		Node forbidTask = flow.getGraph().getNode("task");
//		
//		createEdge(forbidGraph, flow, cp.getGraph_Arrows(), FORBID);
//		createEdge(flow, forbidTask, cp.getArrow_Target(), FORBID);
		
		// DELETE D PREDICATE
		Node oldPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(oldPredicate, cp.getPredicate_Symbol(), PREDICATE_DISABLED, PRESERVE);
		Node oldPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node oldPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(oldPredicate, oldPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(oldPredicateGraph, oldPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		Node oldConstraint = createNode(lhs, cp.getConstraint(), DELETE);
		Node oldGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), DELETE);
		Node oldNodeToNodeMap = createNode(lhs, cp.getNodeToNodeMap(), DELETE);
		
		createEdge(specification, oldConstraint, cp.getSpecification_Constraints(), DELETE);
		createEdge(task, oldConstraint, cp.getNode_Constraints(), DELETE);
//		createEdge(oldConstraint, oldPredicate, cp.getConstraint_Predicate(), DELETE);
		createEdge(oldConstraint, oldPredicate, cp.getConstraint_Predicate(), PRESERVE);
		
		createEdge(oldConstraint, oldGraphHomom, cp.getConstraint_Mappings(), DELETE);
		createEdge(oldGraphHomom, oldNodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), DELETE);
		createEdge(oldNodeToNodeMap, oldPredicateNode, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(oldNodeToNodeMap, task, cp.getNodeToNodeMap_Value(), DELETE);
		
		// ADD R PREDICATE
		Node newPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(newPredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, PRESERVE);
		Node newPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node newPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(newPredicate, newPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(newPredicateGraph, newPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		Node newConstraint = createNode(rhs, cp.getConstraint(), CREATE);
		Node newGraphHomom = createNode(rhs, cp.getGraphHomomorphism(), CREATE);
		Node newNodeToNodeMap = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
		
		createEdge(rhs(specification, rule), newConstraint, cp.getSpecification_Constraints(), CREATE);
		createEdge(rhs(task, rule), newConstraint, cp.getNode_Constraints(), CREATE);
		createEdge(newConstraint, rhs(newPredicate, rule), cp.getConstraint_Predicate(), CREATE);
		
		createEdge(newConstraint, newGraphHomom, cp.getConstraint_Mappings(), CREATE);
		createEdge(newGraphHomom, newNodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(newNodeToNodeMap, rhs(newPredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(newNodeToNodeMap, rhs(task, rule), cp.getNodeToNodeMap_Value(), CREATE);
		
		return rule;
	}
}
