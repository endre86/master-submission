package rules.old.older;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.DELETE;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static org.eclipse.emf.henshin.model.Action.Type.REQUIRE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.FLOW;
import static util.DERFConstants.PREDICATE_AND_JOIN;
import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FINISHED;
import static util.DERFConstants.TASK;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.IRule;
import rules.RuleUtil;

public class AndJoinRule implements IRule {
	
	private static CorePackage cp = CorePackage.eINSTANCE;

	@Override
	public Rule createRule() {
		Rule rule = RuleUtil.createRule("And Join");
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		// BASICS
		
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE, "spec");
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE, "sign");
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, "graph");
		Node typeGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);
		createEdge(graph, typeGraph, cp.getGraph_Type(), PRESERVE);
		
		Node taskMeta = createNode(lhs, cp.getNode(), PRESERVE);
		createAttr(taskMeta, cp.getNode_Name(), TASK, PRESERVE);
		
		Node flowMeta = createNode(lhs, cp.getArrow(), PRESERVE);
		createAttr(flowMeta, cp.getArrow_Name(), FLOW, PRESERVE);
		
		
		// SEACH STUFF
		Node sourceA = createNode(lhs, cp.getNode(), PRESERVE);
		Node sourceAType = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(graph, sourceA, cp.getGraph_Nodes(), PRESERVE);
		createEdge(sourceA, sourceAType, cp.getNode_TypeNode(), PRESERVE);
		createEdge(sourceAType, taskMeta, cp.getNode_TypeNode(), PRESERVE);

		Node sourceB = createNode(lhs, cp.getNode(), PRESERVE);
		Node sourceBType = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(graph, sourceB, cp.getGraph_Nodes(), PRESERVE);
		createEdge(sourceB, sourceBType, cp.getNode_TypeNode(), PRESERVE);
		createEdge(sourceBType, taskMeta, cp.getNode_TypeNode(), PRESERVE);
		
		// F predicate
		Node sourcePredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(sourcePredicate, cp.getPredicate_Symbol(), PREDICATE_FINISHED, PRESERVE);
		Node sourcePredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node sourcePredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(sourcePredicate, sourcePredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(sourcePredicateGraph, sourcePredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Require F predicate
		Node sourceAConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node sourceAGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node sourceANodeToNodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, sourceAConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(sourceA, sourceAConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(sourceAConstraint, sourcePredicate, cp.getConstraint_Predicate(), PRESERVE);
		
		createEdge(sourceAConstraint, sourceAGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		createEdge(sourceAGraphHomom, sourceANodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(sourceANodeToNodeMap, sourcePredicateNode, cp.getNodeToNodeMap_Key(), PRESERVE);
		createEdge(sourceANodeToNodeMap, sourceA, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		Node sourceBConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node sourceBGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node sourceBNodeToNodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, sourceBConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(sourceB, sourceBConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(sourceBConstraint, sourcePredicate, cp.getConstraint_Predicate(), PRESERVE);
		
		createEdge(sourceBConstraint, sourceBGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		createEdge(sourceBGraphHomom, sourceBNodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(sourceBNodeToNodeMap, sourcePredicateNode, cp.getNodeToNodeMap_Key(),  PRESERVE);
		
		// NODE TO ALTER ([D] => [E]) FOR BOTH TARGETS (A & B)
		
		Node target = createNode(lhs, cp.getNode(), PRESERVE);
		Node targetType = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(graph, target, cp.getGraph_Nodes(), PRESERVE);
		createEdge(target, targetType, cp.getNode_TypeNode(), PRESERVE);
		createEdge(targetType, taskMeta, cp.getNode_TypeNode(), PRESERVE);
		
		
		// D predicate
		Node oldPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(oldPredicate, cp.getPredicate_Symbol(), PREDICATE_DISABLED, PRESERVE);
		Node oldPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node oldPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(oldPredicate, oldPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(oldPredicateGraph, oldPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// E predicate 
		Node newPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(newPredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, PRESERVE);
		Node newPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node newPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(newPredicate, newPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(newPredicateGraph, newPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Old constraint
		Node oldAConstraint = createNode(lhs, cp.getConstraint(), DELETE);
		Node oldAGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), DELETE);
		Node oldANodeToNodeMap = createNode(lhs, cp.getNodeToNodeMap(), DELETE);
		
		createEdge(specification, oldAConstraint, cp.getSpecification_Constraints(), DELETE);
		createEdge(target, oldAConstraint, cp.getNode_Constraints(), DELETE);
		createEdge(oldAConstraint, oldPredicate, cp.getConstraint_Predicate(), DELETE);
		
		createEdge(oldAConstraint, oldAGraphHomom, cp.getConstraint_Mappings(), DELETE);
		createEdge(oldAGraphHomom, oldANodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), DELETE);
		createEdge(oldANodeToNodeMap, oldPredicateNode, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(oldANodeToNodeMap, target, cp.getNodeToNodeMap_Value(), DELETE);
		
		// New constraint
		Node newAConstraint = createNode(rhs, cp.getConstraint(), CREATE);
		Node newAGraphHomom = createNode(rhs, cp.getGraphHomomorphism(), CREATE);
		Node newANodeToNodeMap = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
		
		createEdge(rhs(specification, rule), newAConstraint, cp.getSpecification_Constraints(), CREATE);
		createEdge(rhs(target, rule), newAConstraint, cp.getNode_Constraints(), CREATE);
		createEdge(newAConstraint, rhs(newPredicate, rule), cp.getConstraint_Predicate(), CREATE);
		
		createEdge(newAConstraint, newAGraphHomom, cp.getConstraint_Mappings(), CREATE);
		createEdge(newAGraphHomom, newANodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(newANodeToNodeMap, rhs(newPredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(newANodeToNodeMap, rhs(target, rule), cp.getNodeToNodeMap_Value(), CREATE);
		
		
		// FLOWS
		Node flowA = createNode(lhs, cp.getArrow(), PRESERVE);
		Node flowAType = createNode(lhs, cp.getArrow(), PRESERVE, "flowAType");
		
		createEdge(graph, flowA, cp.getGraph_Arrows(), PRESERVE);
		createEdge(flowA, flowAType, cp.getArrow_TypeArrow(), PRESERVE);
		createEdge(flowAType, flowMeta, cp.getArrow_TypeArrow(), PRESERVE);
		
		createEdge(flowA, sourceA, cp.getArrow_Source(), PRESERVE);
		createEdge(flowA, target, cp.getArrow_Target(), PRESERVE);
		createEdge(flowAType, sourceAType, cp.getArrow_Source(), PRESERVE);
		createEdge(flowAType, targetType, cp.getArrow_Target(), PRESERVE);
		
		Node flowB = createNode(lhs, cp.getArrow(), PRESERVE);
		Node flowBType = createNode(lhs, cp.getArrow(), PRESERVE, "flowBType");
		
		createEdge(graph, flowB, cp.getGraph_Arrows(), PRESERVE);
		createEdge(flowB, flowBType, cp.getArrow_TypeArrow(), PRESERVE);
		createEdge(flowBType, flowMeta, cp.getArrow_TypeArrow(), PRESERVE);
		
		createEdge(flowB, sourceB, cp.getArrow_Source(), PRESERVE);
		createEdge(flowB, target, cp.getArrow_Target(), PRESERVE);
		createEdge(flowBType, sourceBType, cp.getArrow_Source(), PRESERVE);
		createEdge(flowBType, targetType, cp.getArrow_Target(), PRESERVE);
		
		
		// AND_JOIN CONSTRAINT
		Node andSplitPredicate = createNode(lhs, cp.getPredicate(), PRESERVE, "andJoinPredicate");
		createAttr(andSplitPredicate, cp.getPredicate_Symbol(), PREDICATE_AND_JOIN, PRESERVE);
		
		Node flowConstraint = createNode(lhs, cp.getConstraint(), REQUIRE);
		
		Graph requireGraph = flowConstraint.getGraph();
		Node requireFlowAType = requireGraph.getNode("flowAType");
		Node requireFlowBType = requireGraph.getNode("flowBType");
		Node requireAndSplitPredicate = requireGraph.getNode("andJoinPredicate");
		
		createEdge(requireFlowAType, flowConstraint, cp.getArrow_Constraints(), REQUIRE);
		createEdge(requireFlowBType, flowConstraint, cp.getArrow_Constraints(), REQUIRE);
		createEdge(flowConstraint, requireAndSplitPredicate, cp.getConstraint_Predicate(), REQUIRE);
		
		return rule;
	}
	
}
