package rules.old.older;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.DELETE;
import static org.eclipse.emf.henshin.model.Action.Type.FORBID;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.*;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Edge;
import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.IRule;
import rules.RuleUtil;

public class SimpleFlowRule implements IRule {
	
	private static CorePackage cp = CorePackage.eINSTANCE;

	@Override
	public Rule createRule() {
		Rule rule = RuleUtil.createRule("Simple Flow");
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		// BASICS
		
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE, "spec");
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE, "sign");
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, "graph");
		Node typeGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);
		createEdge(graph, typeGraph, cp.getGraph_Type(), PRESERVE);
		
		Node taskMeta = createNode(lhs, cp.getNode(), PRESERVE);
		createAttr(taskMeta, cp.getNode_Name(), TASK, PRESERVE);
		
		Node flowMeta = createNode(lhs, cp.getArrow(), PRESERVE);
		createAttr(flowMeta, cp.getArrow_Name(), FLOW, PRESERVE);
		
		
		// SEACH STUFF
		Node source = createNode(lhs, cp.getNode(), PRESERVE);
		Node sourceType = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(graph, source, cp.getGraph_Nodes(), PRESERVE);
		createEdge(source, sourceType, cp.getNode_TypeNode(), PRESERVE);
		createEdge(sourceType, taskMeta, cp.getNode_TypeNode(), PRESERVE);
		
		// require F predicate
		Node sourcePredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(sourcePredicate, cp.getPredicate_Symbol(), PREDICATE_FINISHED, PRESERVE);
		Node sourcePredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node sourcePredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(sourcePredicate, sourcePredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(sourcePredicateGraph, sourcePredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		Node sourceConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node sourceGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node sourceNodeToNodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, sourceConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(source, sourceConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(sourceConstraint, sourcePredicate, cp.getConstraint_Predicate(), PRESERVE);
		
		createEdge(sourceConstraint, sourceGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		createEdge(sourceGraphHomom, sourceNodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(sourceNodeToNodeMap, sourcePredicateNode, cp.getNodeToNodeMap_Key(), PRESERVE);
		createEdge(sourceNodeToNodeMap, source, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		
		// NODE TO ALTER ([D] => [E])
		
		Node target = createNode(lhs, cp.getNode(), PRESERVE);
		Node targetType = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(graph, target, cp.getGraph_Nodes(), PRESERVE);
		createEdge(target, targetType, cp.getNode_TypeNode(), PRESERVE);
		createEdge(targetType, taskMeta, cp.getNode_TypeNode(), PRESERVE);
		
		// delete D predicate
		Node oldPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(oldPredicate, cp.getPredicate_Symbol(), PREDICATE_DISABLED, PRESERVE);
		Node oldPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node oldPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(oldPredicate, oldPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(oldPredicateGraph, oldPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		Node oldConstraint = createNode(lhs, cp.getConstraint(), DELETE);
		Node oldGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), DELETE);
		Node oldNodeToNodeMap = createNode(lhs, cp.getNodeToNodeMap(), DELETE);
		
		createEdge(specification, oldConstraint, cp.getSpecification_Constraints(), DELETE);
		createEdge(target, oldConstraint, cp.getNode_Constraints(), DELETE);
		createEdge(oldConstraint, oldPredicate, cp.getConstraint_Predicate(), DELETE);
		
		createEdge(oldConstraint, oldGraphHomom, cp.getConstraint_Mappings(), DELETE);
		createEdge(oldGraphHomom, oldNodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), DELETE);
		createEdge(oldNodeToNodeMap, oldPredicateNode, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(oldNodeToNodeMap, target, cp.getNodeToNodeMap_Value(), DELETE);
		
		// add E predicate
		Node newPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(newPredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, PRESERVE);
		Node newPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node newPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(newPredicate, newPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(newPredicateGraph, newPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		Node newConstraint = createNode(rhs, cp.getConstraint(), CREATE);
		Node newGraphHomom = createNode(rhs, cp.getGraphHomomorphism(), CREATE);
		Node newNodeToNodeMap = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
		
		createEdge(rhs(specification, rule), newConstraint, cp.getSpecification_Constraints(), CREATE);
		createEdge(rhs(target, rule), newConstraint, cp.getNode_Constraints(), CREATE);
		createEdge(newConstraint, rhs(newPredicate, rule), cp.getConstraint_Predicate(), CREATE);
		
		createEdge(newConstraint, newGraphHomom, cp.getConstraint_Mappings(), CREATE);
		createEdge(newGraphHomom, newNodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(newNodeToNodeMap, rhs(newPredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(newNodeToNodeMap, rhs(target, rule), cp.getNodeToNodeMap_Value(), CREATE);
		
		// FLOW
		Node flow = createNode(lhs, cp.getArrow(), PRESERVE, "flow");
		Node flowType = createNode(lhs, cp.getArrow(), PRESERVE, "flowType");
		
		createEdge(graph, flow, cp.getGraph_Arrows(), PRESERVE);
		createEdge(flow, flowType, cp.getArrow_TypeArrow(), PRESERVE);
		createEdge(flowType, flowMeta, cp.getArrow_TypeArrow(), PRESERVE);
		
		createEdge(flow, source, cp.getArrow_Source(), PRESERVE);
		createEdge(flow, target, cp.getArrow_Target(), PRESERVE);
		
		Node flowTypeConstraint = createNode(lhs, cp.getConstraint(), FORBID);
		
		Graph forbidGraph = flowTypeConstraint.getGraph();
		
		Node forbidFlowType = forbidGraph.getNode("flowType");
		createEdge(forbidFlowType, flowTypeConstraint, cp.getArrow_Constraints(), FORBID);
		
		// Remove [f] predicate
		Node oldFlowPredicate = createNode(lhs, cp.getPredicate(), PRESERVE, "oldFlowPredicate");
		createAttr(oldFlowPredicate, cp.getPredicate_Symbol(), PREDICATE_FLOW_FALSE, PRESERVE);
		Node oldFlowPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE, "pg");
		Node oldFlowPredicateSource = createNode(lhs, cp.getNode(), PRESERVE, "oldFlowPredicateSource");
		Node oldFlowPredicateTarget = createNode(lhs, cp.getNode(), PRESERVE, "oldFlowPredicateTarget");
		Node oldFlowPredicateArrow = createNode(lhs, cp.getArrow(), PRESERVE, "oldFlowPredicateArrow");
		
		createEdge(signature, oldFlowPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(oldFlowPredicate, oldFlowPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(oldFlowPredicateGraph, oldFlowPredicateSource, cp.getGraph_Nodes(), PRESERVE);
		createEdge(oldFlowPredicateGraph, oldFlowPredicateTarget, cp.getGraph_Nodes(), PRESERVE);
		createEdge(oldFlowPredicateGraph, oldFlowPredicateArrow, cp.getGraph_Arrows(), PRESERVE);
		createEdge(oldFlowPredicateArrow, oldFlowPredicateSource, cp.getArrow_Source(), PRESERVE);
		createEdge(oldFlowPredicateArrow, oldFlowPredicateTarget, cp.getArrow_Target(), PRESERVE);
		
		
		Node oldFlowConstraint = createNode(lhs, cp.getConstraint(), DELETE, "c");
		Node oldFlowGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), DELETE, "gh");
		Node oldFlowNodeToNodeMapSource = createNode(lhs, cp.getNodeToNodeMap(), DELETE);
		Node oldFlowNodeToNodeMapTarget = createNode(lhs, cp.getNodeToNodeMap(), DELETE);
		Node oldFlowArrowToArrowMap = createNode(lhs, cp.getArrowToArrowMap(), DELETE, "a2a");

		createEdge(specification, oldFlowConstraint, cp.getSpecification_Constraints(), DELETE);
		createEdge(oldFlowConstraint, oldFlowPredicate, cp.getConstraint_Predicate(), DELETE);
		createEdge(source, oldFlowConstraint, cp.getNode_Constraints(), DELETE);
		createEdge(target, oldFlowConstraint, cp.getNode_Constraints(), DELETE);
		createEdge(flow, oldFlowConstraint, cp.getArrow_Constraints(), DELETE);
		
		createEdge(oldFlowConstraint, oldFlowGraphHomom, cp.getConstraint_Mappings(), DELETE);
		createEdge(oldFlowGraphHomom, oldFlowNodeToNodeMapSource, cp.getGraphHomomorphism_NodeMapping(), DELETE);
		createEdge(oldFlowGraphHomom, oldFlowNodeToNodeMapTarget, cp.getGraphHomomorphism_NodeMapping(), DELETE);
		createEdge(oldFlowGraphHomom, oldFlowArrowToArrowMap, cp.getGraphHomomorphism_ArrowMapping(), DELETE);
		
		createEdge(oldFlowNodeToNodeMapSource, oldFlowPredicateSource, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(oldFlowNodeToNodeMapSource, source, cp.getNodeToNodeMap_Value(), DELETE);
		createEdge(oldFlowNodeToNodeMapTarget, oldFlowPredicateTarget, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(oldFlowNodeToNodeMapTarget, target, cp.getNodeToNodeMap_Value(), DELETE);
		createEdge(oldFlowArrowToArrowMap, oldFlowPredicateArrow, cp.getArrowToArrowMap_Key(), DELETE);
		createEdge(oldFlowArrowToArrowMap, flow, cp.getArrowToArrowMap_Value(), DELETE);
		
		
		
		
//		createEdge(oldFlowConstraint, oldFlowGraphHomom, cp.getConstraint_Mappings(), DELETE);
//		createEdge(oldFlowGraphHomom, oldFlowArrowToArrowMap, cp.getGraphHomomorphism_ArrowMapping(), DELETE);
//		createEdge(oldFlowArrowToArrowMap, oldFlowPredicateArrow, cp.getArrowToArrowMap_Key(), DELETE);
//		createEdge(oldFlowArrowToArrowMap, flow, cp.getArrowToArrowMap_Value(), DELETE);
//		
//		Node pn1 = createNode(lhs, cp.getNode(), DELETE);
//		Node pn2 = createNode(lhs, cp.getNode(), DELETE);
//		
//		createEdge(oldFlowPredicateGraph, pn1, cp.getGraph_Nodes(), PRESERVE);
//		createEdge(oldFlowPredicateGraph, pn2, cp.getGraph_Nodes(), PRESERVE);
//		
//		Node pn1map = createNode(lhs, cp.getNodeToNodeMap(), DELETE);
//		Node pn2map = createNode(lhs, cp.getNodeToNodeMap(), DELETE);
//
//		createEdge(oldFlowPredicateArrow, pn1, cp.getArrow_Source(), PRESERVE);
//		createEdge(oldFlowPredicateArrow, pn2, cp.getArrow_Target(), PRESERVE);
//		
//		createEdge(oldFlowGraphHomom, pn1map, cp.getGraphHomomorphism_NodeMapping(), DELETE);
//		createEdge(oldFlowGraphHomom, pn2map, cp.getGraphHomomorphism_NodeMapping(), DELETE);
//		createEdge(pn1map, pn1, cp.getNodeToNodeMap_Key(), DELETE);
//		createEdge(pn2map, pn2, cp.getNodeToNodeMap_Key(), DELETE);
//		createEdge(pn1map, source, cp.getNodeToNodeMap_Value(), DELETE);
//		createEdge(pn2map, target, cp.getNodeToNodeMap_Value(), DELETE);
//		
//		createEdge(source, oldFlowConstraint, cp.getNode_Constraints(), DELETE);
//		createEdge(target, oldFlowConstraint, cp.getNode_Constraints(), DELETE);
		
		// add [t] predicate
//		Node newFlowPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
//		createAttr(newFlowPredicate, cp.getPredicate_Symbol(), PREDICATE_FLOW_TRUE, PRESERVE);
//		Node newFlowPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
//		Node newFlowPredicateArrow = createNode(lhs, cp.getArrow(), PRESERVE);
//		
//		createEdge(newFlowPredicate,newFlowPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
//		createEdge(newFlowPredicateGraph, newFlowPredicateArrow, cp.getGraph_Arrows(), PRESERVE);
//		
//		Node newFlowConstraint = createNode(rhs, cp.getConstraint(), CREATE);
//		Node newFlowGraphHomom = createNode(rhs, cp.getGraphHomomorphism(), CREATE);
//		Node newFlowArrowToArrowMap = createNode(rhs, cp.getArrowToArrowMap(), CREATE);
//		
//		createEdge(rhs(specification, rule), newFlowConstraint, cp.getSpecification_Constraints(), CREATE);
//		createEdge(rhs(flow, rule), newFlowConstraint, cp.getArrow_Constraints(), CREATE);
//		createEdge(newFlowConstraint, rhs(newFlowPredicate, rule), cp.getConstraint_Predicate(), CREATE);
//		
//		createEdge(newFlowConstraint, newFlowGraphHomom, cp.getConstraint_Mappings(), CREATE);
//		createEdge(newFlowGraphHomom, newFlowArrowToArrowMap, cp.getGraphHomomorphism_ArrowMapping(), CREATE);
//		createEdge(newFlowArrowToArrowMap, newFlowPredicateArrow, cp.getArrowToArrowMap_Key(), CREATE);
//		createEdge(newFlowArrowToArrowMap, rhs(flow, rule), cp.getArrowToArrowMap_Value(), CREATE);
		
		return rule;
	}
	
}
