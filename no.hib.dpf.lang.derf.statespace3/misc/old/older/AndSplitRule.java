package rules.old.older;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.DELETE;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static org.eclipse.emf.henshin.model.Action.Type.REQUIRE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.FLOW;
import static util.DERFConstants.PREDICATE_AND_SPLIT;
import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.PREDICATE_FINISHED;
import static util.DERFConstants.TASK;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.IRule;
import rules.RuleUtil;

public class AndSplitRule implements IRule {
	
	private static CorePackage cp = CorePackage.eINSTANCE;

	@Override
	public Rule createRule() {
		Rule rule = RuleUtil.createRule("And Split");
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		// BASICS
		
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE, "spec");
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE, "sign");
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, "graph");
		Node typeGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);
		createEdge(graph, typeGraph, cp.getGraph_Type(), PRESERVE);
		
		Node taskMeta = createNode(lhs, cp.getNode(), PRESERVE);
		createAttr(taskMeta, cp.getNode_Name(), TASK, PRESERVE);
		
		Node flowMeta = createNode(lhs, cp.getArrow(), PRESERVE);
		createAttr(flowMeta, cp.getArrow_Name(), FLOW, PRESERVE);
		
		
		// SEACH STUFF
		Node source = createNode(lhs, cp.getNode(), PRESERVE);
		Node sourceType = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(graph, source, cp.getGraph_Nodes(), PRESERVE);
		createEdge(source, sourceType, cp.getNode_TypeNode(), PRESERVE);
		createEdge(sourceType, taskMeta, cp.getNode_TypeNode(), PRESERVE);
		
		// require F predicate
		Node sourcePredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(sourcePredicate, cp.getPredicate_Symbol(), PREDICATE_FINISHED, PRESERVE);
		Node sourcePredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node sourcePredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(sourcePredicate, sourcePredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(sourcePredicateGraph, sourcePredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		Node sourceConstraint = createNode(lhs, cp.getConstraint(), PRESERVE);
		Node sourceGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), PRESERVE);
		Node sourceNodeToNodeMap = createNode(lhs, cp.getNodeToNodeMap(), PRESERVE);
		
		createEdge(specification, sourceConstraint, cp.getSpecification_Constraints(), PRESERVE);
		createEdge(source, sourceConstraint, cp.getNode_Constraints(), PRESERVE);
		createEdge(sourceConstraint, sourcePredicate, cp.getConstraint_Predicate(), PRESERVE);
		
		createEdge(sourceConstraint, sourceGraphHomom, cp.getConstraint_Mappings(), PRESERVE);
		createEdge(sourceGraphHomom, sourceNodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), PRESERVE);
		createEdge(sourceNodeToNodeMap, sourcePredicateNode, cp.getNodeToNodeMap_Key(), PRESERVE);
		createEdge(sourceNodeToNodeMap, source, cp.getNodeToNodeMap_Value(), PRESERVE);
		
		
		// NODE TO ALTER ([D] => [E]) FOR BOTH TARGETS (A & B)
		
		Node targetA = createNode(lhs, cp.getNode(), PRESERVE);
		Node targetAType = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(graph, targetA, cp.getGraph_Nodes(), PRESERVE);
		createEdge(targetA, targetAType, cp.getNode_TypeNode(), PRESERVE);
		createEdge(targetAType, taskMeta, cp.getNode_TypeNode(), PRESERVE);
		
		Node targetB = createNode(lhs, cp.getNode(), PRESERVE);
		Node targetBType = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(graph, targetB, cp.getGraph_Nodes(), PRESERVE);
		createEdge(targetB, targetBType, cp.getNode_TypeNode(), PRESERVE);
		createEdge(targetBType, taskMeta, cp.getNode_TypeNode(), PRESERVE);
		
		
		// D predicate
		Node oldPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(oldPredicate, cp.getPredicate_Symbol(), PREDICATE_DISABLED, PRESERVE);
		Node oldPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node oldPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(oldPredicate, oldPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(oldPredicateGraph, oldPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// E predicate 
		Node newPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(newPredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, PRESERVE);
		Node newPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node newPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(newPredicate, newPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(newPredicateGraph, newPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Old constraint A
		Node oldAConstraint = createNode(lhs, cp.getConstraint(), DELETE);
		Node oldAGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), DELETE);
		Node oldANodeToNodeMap = createNode(lhs, cp.getNodeToNodeMap(), DELETE);
		
		createEdge(specification, oldAConstraint, cp.getSpecification_Constraints(), DELETE);
		createEdge(targetA, oldAConstraint, cp.getNode_Constraints(), DELETE);
		createEdge(oldAConstraint, oldPredicate, cp.getConstraint_Predicate(), DELETE);
		
		createEdge(oldAConstraint, oldAGraphHomom, cp.getConstraint_Mappings(), DELETE);
		createEdge(oldAGraphHomom, oldANodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), DELETE);
		createEdge(oldANodeToNodeMap, oldPredicateNode, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(oldANodeToNodeMap, targetA, cp.getNodeToNodeMap_Value(), DELETE);
		
		// Old constraint B
		Node oldBConstraint = createNode(lhs, cp.getConstraint(), DELETE);
		Node oldBGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), DELETE);
		Node oldBNodeToNodeMap = createNode(lhs, cp.getNodeToNodeMap(), DELETE);
		
		createEdge(specification, oldBConstraint, cp.getSpecification_Constraints(), DELETE);
		createEdge(targetB, oldBConstraint, cp.getNode_Constraints(), DELETE);
		createEdge(oldBConstraint, oldPredicate, cp.getConstraint_Predicate(), DELETE);
		
		createEdge(oldBConstraint, oldBGraphHomom, cp.getConstraint_Mappings(), DELETE);
		createEdge(oldBGraphHomom, oldBNodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), DELETE);
		createEdge(oldBNodeToNodeMap, oldPredicateNode, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(oldBNodeToNodeMap, targetB, cp.getNodeToNodeMap_Value(), DELETE);
		
		// New constraint A
		Node newAConstraint = createNode(rhs, cp.getConstraint(), CREATE);
		Node newAGraphHomom = createNode(rhs, cp.getGraphHomomorphism(), CREATE);
		Node newANodeToNodeMap = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
		
		createEdge(rhs(specification, rule), newAConstraint, cp.getSpecification_Constraints(), CREATE);
		createEdge(rhs(targetA, rule), newAConstraint, cp.getNode_Constraints(), CREATE);
		createEdge(newAConstraint, rhs(newPredicate, rule), cp.getConstraint_Predicate(), CREATE);
		
		createEdge(newAConstraint, newAGraphHomom, cp.getConstraint_Mappings(), CREATE);
		createEdge(newAGraphHomom, newANodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(newANodeToNodeMap, rhs(newPredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(newANodeToNodeMap, rhs(targetA, rule), cp.getNodeToNodeMap_Value(), CREATE);
		
		// New constraint B
		Node newBConstraint = createNode(rhs, cp.getConstraint(), CREATE);
		Node newBGraphHomom = createNode(rhs, cp.getGraphHomomorphism(), CREATE);
		Node newBNodeToNodeMap = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
		
		createEdge(rhs(specification, rule), newBConstraint, cp.getSpecification_Constraints(), CREATE);
		createEdge(rhs(targetB, rule), newBConstraint, cp.getNode_Constraints(), CREATE);
		createEdge(newBConstraint, rhs(newPredicate, rule), cp.getConstraint_Predicate(), CREATE);
		
		createEdge(newBConstraint, newBGraphHomom, cp.getConstraint_Mappings(), CREATE);
		createEdge(newBGraphHomom, newBNodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(newBNodeToNodeMap, rhs(newPredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(newBNodeToNodeMap, rhs(targetB, rule), cp.getNodeToNodeMap_Value(), CREATE);
		
		
		// FLOWS
		Node flowA = createNode(lhs, cp.getArrow(), PRESERVE);
		Node flowAType = createNode(lhs, cp.getArrow(), PRESERVE, "flowAType");
		
		createEdge(graph, flowA, cp.getGraph_Arrows(), PRESERVE);
		createEdge(flowA, flowAType, cp.getArrow_TypeArrow(), PRESERVE);
		createEdge(flowAType, flowMeta, cp.getArrow_TypeArrow(), PRESERVE);
		
		createEdge(flowA, source, cp.getArrow_Source(), PRESERVE);
		createEdge(flowA, targetA, cp.getArrow_Target(), PRESERVE);
		createEdge(flowAType, sourceType, cp.getArrow_Source(), PRESERVE);
		createEdge(flowAType, targetAType, cp.getArrow_Target(), PRESERVE);
		
		Node flowB = createNode(lhs, cp.getArrow(), PRESERVE);
		Node flowBType = createNode(lhs, cp.getArrow(), PRESERVE, "flowBType");
		
		createEdge(graph, flowB, cp.getGraph_Arrows(), PRESERVE);
		createEdge(flowB, flowBType, cp.getArrow_TypeArrow(), PRESERVE);
		createEdge(flowBType, flowMeta, cp.getArrow_TypeArrow(), PRESERVE);
		
		createEdge(flowB, source, cp.getArrow_Source(), PRESERVE);
		createEdge(flowB, targetB, cp.getArrow_Target(), PRESERVE);
		createEdge(flowBType, sourceType, cp.getArrow_Source(), PRESERVE);
		createEdge(flowBType, targetBType, cp.getArrow_Target(), PRESERVE);
		
		
		// AND_SPLIT CONSTRAINT
		Node andSplitPredicate = createNode(lhs, cp.getPredicate(), PRESERVE, "andSplitPredicate");
		createAttr(andSplitPredicate, cp.getPredicate_Symbol(), PREDICATE_AND_SPLIT, PRESERVE);
		
		Node flowConstraint = createNode(lhs, cp.getConstraint(), REQUIRE);
		
		Graph requireGraph = flowConstraint.getGraph();
		Node requireFlowAType = requireGraph.getNode("flowAType");
		Node requireFlowBType = requireGraph.getNode("flowBType");
		Node requireAndSplitPredicate = requireGraph.getNode("andSplitPredicate");
		
		createEdge(requireFlowAType, flowConstraint, cp.getArrow_Constraints(), REQUIRE);
		createEdge(requireFlowBType, flowConstraint, cp.getArrow_Constraints(), REQUIRE);
		createEdge(flowConstraint, requireAndSplitPredicate, cp.getConstraint_Predicate(), REQUIRE);
		
		return rule;
	}
	
}
