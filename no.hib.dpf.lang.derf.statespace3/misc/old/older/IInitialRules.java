package rules.old.older;

import java.util.List;

import no.hib.dpf.core.Node;

import org.eclipse.emf.henshin.model.Rule;

public interface IInitialRules {
	/**
	 * Generates a henshin transformation rule.
	 * @param arrows
	 * 		A list of arrows that are needed to generate the rule
	 * 
	 * @return
	 * 		The generated rule
	 */
	List<Rule> createInitialRules(List<Node> initialNodes);

}
