package rules.old.older;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.DELETE;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;

import java.util.LinkedList;
import java.util.List;

import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.RuleUtil;
import static util.DERFConstants.*;
public class InitialD2ERule_BACKUP implements IInitialRules {

	private static CorePackage cp = CorePackage.eINSTANCE;
	
	public List<Rule> createInitialRules(List<no.hib.dpf.core.Node> initialNodes) {
		List<Rule> rules = new LinkedList<Rule>();
		
		for(no.hib.dpf.core.Node initialTask : initialNodes) {
			rules.add(createInitialRule(initialTask.getName()));
		}
		
		return rules;
	}
	
	private static Rule createInitialRule(String taskName) {
		Rule rule = RuleUtil.createRule(taskName + " [D] => [E]");
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		// BASICS + DERF METAMODEL
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE);
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE);
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node typeGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);
		
		// FIND INITIAL TASK BY NAME
		Node task = createNode(lhs, cp.getNode(), PRESERVE);
		createAttr(task, cp.getNode_Name(), taskName, PRESERVE);
		createEdge(graph, task, cp.getGraph_Nodes(), PRESERVE);
		
		// DELETE D CONSTRAINT	
		Node oldPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(oldPredicate, cp.getPredicate_Symbol(), PREDICATE_DISABLED, PRESERVE);
		Node oldPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node oldPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(oldPredicate, oldPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(oldPredicateGraph, oldPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		Node oldConstraint = createNode(lhs, cp.getConstraint(), DELETE);
		Node oldGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), DELETE);
		Node oldNodeToNodeMap = createNode(lhs, cp.getNodeToNodeMap(), DELETE);
		
		createEdge(specification, oldConstraint, cp.getSpecification_Constraints(), DELETE);
		createEdge(task, oldConstraint, cp.getNode_Constraints(), DELETE);
		createEdge(oldConstraint, oldPredicate, cp.getConstraint_Predicate(), DELETE);
		
		createEdge(oldConstraint, oldGraphHomom, cp.getConstraint_Mappings(), DELETE);
		createEdge(oldGraphHomom, oldNodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), DELETE);
		createEdge(oldNodeToNodeMap, oldPredicateNode, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(oldNodeToNodeMap, task, cp.getNodeToNodeMap_Value(), DELETE);
		
		// CREATE E CONSTRAINT
		Node newPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(newPredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, PRESERVE);
		Node newPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node newPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(newPredicate, newPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(newPredicateGraph, newPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		Node newConstraint = createNode(rhs, cp.getConstraint(), CREATE);
		Node newGraphHomom = createNode(rhs, cp.getGraphHomomorphism(), CREATE);
		Node newNodeToNodeMap = createNode(rhs, cp.getNodeToNodeMap(), CREATE);
		
		createEdge(rhs(specification, rule), newConstraint, cp.getSpecification_Constraints(), CREATE);
		createEdge(rhs(task, rule), newConstraint, cp.getNode_Constraints(), CREATE);
		createEdge(newConstraint, rhs(newPredicate, rule), cp.getConstraint_Predicate(), CREATE);
		
		createEdge(newConstraint, newGraphHomom, cp.getConstraint_Mappings(), CREATE);
		createEdge(newGraphHomom, newNodeToNodeMap, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(newNodeToNodeMap, rhs(newPredicateNode, rule), cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(newNodeToNodeMap, rhs(task, rule), cp.getNodeToNodeMap_Value(), CREATE);
		
		return rule;
	}
}
