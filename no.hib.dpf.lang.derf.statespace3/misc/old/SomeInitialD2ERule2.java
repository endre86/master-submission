package rules.old;

import static org.eclipse.emf.henshin.model.Action.Type.CREATE;
import static org.eclipse.emf.henshin.model.Action.Type.DELETE;
import static org.eclipse.emf.henshin.model.Action.Type.FORBID;
import static org.eclipse.emf.henshin.model.Action.Type.PRESERVE;
import static org.eclipse.emf.henshin.model.Action.Type.REQUIRE;
import static rules.RuleUtil.createAttr;
import static rules.RuleUtil.createEdge;
import static rules.RuleUtil.createNode;
import static rules.RuleUtil.rhs;
import static util.DERFConstants.PREDICATE_DISABLED;
import static util.DERFConstants.PREDICATE_ENABLED;
import static util.DERFConstants.TASK;
import no.hib.dpf.core.CorePackage;

import org.eclipse.emf.henshin.model.Graph;
import org.eclipse.emf.henshin.model.Node;
import org.eclipse.emf.henshin.model.Rule;

import rules.IRule;
import rules.RuleUtil;

public class SomeInitialD2ERule2 implements IRule{

	private static CorePackage cp = CorePackage.eINSTANCE;
	
	@Override
	public Rule createRule() {
		Rule rule = RuleUtil.createRule("[D] => [E]");
		
		Graph lhs = rule.getLhs();
		Graph rhs = rule.getRhs();
		
		// Core
		Node specification = createNode(lhs, cp.getSpecification(), PRESERVE, "spec");
		Node signature = createNode(lhs, cp.getSignature(), PRESERVE, "sign");
		Node graph = createNode(lhs, cp.getGraph(), PRESERVE, "graph");
		createEdge(specification, signature, cp.getSpecification_Signature(), PRESERVE);
		createEdge(specification, graph, cp.getSpecification_Graph(), PRESERVE);		
		
		// Predicate D
		Node dPredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(dPredicate, cp.getPredicate_Symbol(), PREDICATE_DISABLED, REQUIRE);
		Node dPredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node dPredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, dPredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(dPredicate, dPredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(dPredicateGraph, dPredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Predicate E
		Node ePredicate = createNode(lhs, cp.getPredicate(), PRESERVE);
		createAttr(ePredicate, cp.getPredicate_Symbol(), PREDICATE_ENABLED, REQUIRE);
		Node ePredicateGraph = createNode(lhs, cp.getGraph(), PRESERVE);
		Node ePredicateNode = createNode(lhs, cp.getNode(), PRESERVE);
		
		createEdge(signature, ePredicate, cp.getSignature_Predicates(), PRESERVE);
		createEdge(ePredicate, ePredicateGraph, cp.getPredicate_Shape(), PRESERVE);
		createEdge(ePredicateGraph, ePredicateNode, cp.getGraph_Nodes(), PRESERVE);
		
		// Task
		Node task = createNode(lhs, cp.getNode(), PRESERVE, "task");
		createEdge(graph, task, cp.getGraph_Nodes(), PRESERVE);
		
		// Forbid incomming arrows
		Node flow = createNode(lhs, cp.getArrow(), FORBID);
		Node forbidGraph = flow.getGraph().getNode("graph");
		Node forbidTask = flow.getGraph().getNode("task");
		createEdge(forbidGraph, flow, cp.getGraph_Arrows(), FORBID);
		createEdge(flow, forbidTask, cp.getArrow_Source(), FORBID);
		
		// Remove D predicate
		Node oldConstraint = createNode(lhs, cp.getConstraint(), DELETE, "oldCOnstr");
		Node oldGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), DELETE, "grhom");
		Node oldNode2NodeMap = createNode(lhs, cp.getNodeToNodeMap(), DELETE, "n2n");
		
		createEdge(specification, oldConstraint, cp.getSpecification_Constraints(), DELETE);
		createEdge(task, oldConstraint, cp.getNode_Constraints(), DELETE);
		createEdge(oldConstraint, dPredicate, cp.getConstraint_Predicate(), DELETE);
		createEdge(oldConstraint, task, cp.getConstraint_Nodes(), DELETE);
		createEdge(oldConstraint, oldGraphHomom, cp.getConstraint_Mappings(), DELETE);
		
		createEdge(oldGraphHomom, oldNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), DELETE);
		createEdge(oldNode2NodeMap, dPredicateNode, cp.getNodeToNodeMap_Key(), DELETE);
		createEdge(oldNode2NodeMap, task, cp.getNodeToNodeMap_Value(), DELETE);

		// Remove D predicate
		Node newConstraint = createNode(lhs, cp.getConstraint(), CREATE);
		Node newGraphHomom = createNode(lhs, cp.getGraphHomomorphism(), CREATE);
		Node newNode2NodeMap = createNode(lhs, cp.getNodeToNodeMap(), CREATE);
		
		createEdge(specification, newConstraint, cp.getSpecification_Constraints(), CREATE);
		createEdge(task, newConstraint, cp.getNode_Constraints(), CREATE);
		createEdge(newConstraint, dPredicate, cp.getConstraint_Predicate(), CREATE);
		createEdge(newConstraint, task, cp.getConstraint_Nodes(), CREATE);
		createEdge(newConstraint, newGraphHomom, cp.getConstraint_Mappings(), CREATE);
		
		createEdge(newGraphHomom, newNode2NodeMap, cp.getGraphHomomorphism_NodeMapping(), CREATE);
		createEdge(newNode2NodeMap, dPredicateNode, cp.getNodeToNodeMap_Key(), CREATE);
		createEdge(newNode2NodeMap, task, cp.getNodeToNodeMap_Value(), CREATE);
		
		
		
		
		return rule;
	}
	
}
