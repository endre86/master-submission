This is my submission code for my master thesis.

### How do I get set up? ###

* Download the entire repository.
* Add all projects to Eclipse.
* Run the main/Program.java file

### How do I create my own DERF models? ###

* First you need to install the DPF Framework which can be found here:
http://dpf.hib.no/downloads/ (In Eclipse, choose install new software from the 'Help' menu, add the url and install the package.)

* Then you want to create a new DPF Specification. File > New > DPF Specification Diagram.
  
* Enter the file name and click next.
  
* Choose no.hib.dpf.lang.derf3/resources/derf_meta.xmi as the metamodel
  
* Choose no.hib.dpf.lang.derf3/resources/derf_meta.sig as the signature 

### Who do I talk to? ###

endre.vestbo@gmail.com